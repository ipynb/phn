﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Web;




/// <summary>
/// Summary description for PageBuilder
/// </summary>
public class myPageBuilder
{
    private string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
    public string TagBuildTagText(string cssClass, string style, string text)
    {   
        if (String.IsNullOrEmpty(cssClass)){
            return "<span style=\"" + style + "\">" + text + "</span>";
        }
        else
        {
        return "<span class=\"" + cssClass + "\" style=\"" + style + "\">" + text + "</span>";
        }
    }
    public string TagBuildTagVisitorIP(string cssClass, string style, string text)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<span style=\"" + style + "\">" + text + "</span>";
        }
        else
        {
            return "<span class=\"" + cssClass + "\" style=\"" + style + "\">" + text + "</span>";
        }
    }
    public string TagBuildTagVisitorCountry(string cssClass, string style, string ip)
    {
        string cc = "-";
        string cn = "Unknown";

        using (CountryLookup cl = new CountryLookup(HttpContext.Current.Server.MapPath("~/App_Data/GeoIP.dat")))
        {
            try
            {
                cc = cl.lookupCountryCode(ip);
                cn = cl.lookupCountryName(ip);
            }
            catch (Exception ex)
            {
                //return (ex.ToString());
                cc = "-";
                cc = "Unknown";
            }
        }


        if (String.IsNullOrEmpty(cssClass))
        {
            return "<span style=\"" + style + "\"><img src=\"/images/flags/" + cc + ".png\" alt=\"" + cc + "\"/> " + cn + "</span>";
        }
        else
        {
            return "<span class=\"" + cssClass + "\" style=\"" + style + "\"><img src=\"/images/flags/" + cc + ".png\" alt=\"" + cc + "\"/> " + cn + "</span>";
        }
    }
    public string TagBuildTagBText(string cssClass, string style, string text)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<div style=\"" + style + "\">" + text + "</div>";
        }
        else
        {
            return "<div class=\"" + cssClass + "\" style=\"" + style + "\">" + text + "</div>";
        }
    }
    public string TagBuildTagDiv(string cssClass, string style)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<div style=\"" + style + "\">";
        }
        else
        {
            return "<div class=\"" + cssClass + "\" style=\"" + style + "\">";
        }
    }
    public string TagBuildTagParagraph(string cssClass, string style)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<p style=\"" + style + "\">";
        }
        else
        {
            return "<p class=\"" + cssClass + "\" style=\"" + style + "\">";
        }
    }
    public string TagBuildTagServerTime(string cssClass, string style, float timezone)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<span style=\"" + style + "\">" + String.Format("{0:HH:mm:ss}", DateTime.UtcNow.AddHours(timezone)) + "</span>";
        }
        else
        {
            return "<span class=\"" + cssClass + "\" style=\"" + style + "\">" + String.Format("{0:HH:mm:ss}", DateTime.UtcNow.AddHours(timezone)) + "</span>";
        }
    }
    public string TagBuildTagServerDate(string cssClass, string style, float timezone)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<span style=\"" + style + "\">" + String.Format("{0:dd MMM yyyy}", DateTime.UtcNow.AddHours(timezone)) + "</span>";
        }
        else
        {
            return "<span class=\"" + cssClass + "\" style=\"" + style + "\">" + String.Format("{0:dd MMM yyyy}", DateTime.UtcNow.AddHours(timezone)) + "</span>";
        }
    }
    public string TagBuildTagTextLink(string cssClass, string style, string url, string title)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<a href=\"" + url + " \" style=\"" + style + "\">" + title + "</a>";
        }
        else
        {
            return "<a href=\"" + url + " class=\"" + cssClass + "\"  \" style=\"" + style + "\">" + title + "</a>";
        }
    }
    public string TagBuildTagImage(string cssClass, string style, string imageurl)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<img src=\"" + imageurl + " \" style=\"" + style + "\" alt=\"\"/>";
        }
        else
        {
            return "<img src=\"" + imageurl + " \" class=\"" + cssClass + "\" style=\"" + style + "\" alt=\"\"/>"; 
        }
    }
    public string TagBuildTagImageLink(string cssClass, string style, string linktitle, string imageurl)
    {
        if (String.IsNullOrEmpty(cssClass))
        {
            return "<a href=\"" + imageurl + " \" style=\"" + style + "\"><img src=\"" + linktitle + " \" alt=\"\"/></a>";
        }
        else
        {
            return "<a href=\"" + imageurl + " class=\"" + cssClass + "\"  \" style=\"" + style + "\"><img src=\"" + linktitle + " \" alt=\"\"/></a>";
        }
    }
    public string TagBuildTagCodeSnippet(string subdomainname, int tagRelatedId)
    {
        using (SqlConnection conn = new SqlConnection())
            {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetAsnipForBuilder", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = tagRelatedId;
                return (string)cmd.ExecuteScalar();
            }
        }
    }
    public string TagBuildTagRssFeedA(string subdomainname, int tagRelatedId)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetFeedUrlForBuilder", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = tagRelatedId;
                return (string)cmd.ExecuteScalar();
            }
        }
    }
    public string TagBuildTagRssFeedB(string RssUrl, string articleCountToPull)
    {
        try
        {

            RssToolkit.Rss.RssDocument rss = RssToolkit.Rss.RssDocument.Load(new System.Uri(RssUrl));
            StringBuilder s = new StringBuilder();

            s.Append("<ul style=\"list-style-type:none; padding:0; margin:0;\">");
            int i = 0;
            foreach (RssToolkit.Rss.RssItem item in rss.Channel.Items)//rss.SelectItems(Convert.ToInt32(articleCountToPull)))
            {
                s.Append("<li style=\"margin-bottom:5px;\">");
                s.Append("<b>" + item.Title + "</b> " + item.PubDate + "<br/>");
                //s.Append(myPageBuilder.Truncate(item.Description, 500));
                s.Append(ReplaceImageLinks(item.Description));
                s.Append("<br/>");
                s.AppendFormat("<a href=\"{0}\">Original</a>", item.Link, item);
                s.Append("</li>");

                i++;
                if (i == Convert.ToInt32(articleCountToPull))
                {
                    break;
                }

            }
            s.Append("</ul>");

            return s.ToString();
        }
        catch
        {
            return "Unable to get the feed. Please try again later";
        }

    }
    public string TagBuildTagLinkRotator(string subdomainname)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("GetRandomLinkFromLinkRotator", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return "<a href=\"" + reader["url"].ToString() + "\">" + reader["title"].ToString() + "</a>";
                    }
                    else
                    {
                        return string.Empty;
                    }
                }

            }
        }
    }
    public string TagBuildTagAdvert(string subdomainname, int tagRelatedId)
    {
        string TagTypeToShow = null;
        string alternatePublisherId = null;
        string publisherId= null;
        bool show=false;

        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetAdIdsForBuilder", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = tagRelatedId;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        show = true;
                        TagTypeToShow = reader["advertCompany"].ToString();
                        publisherId = reader["publisherId"].ToString();
                        alternatePublisherId = reader["alternateId"].ToString();
                    }
                  }
            }
        }

        if (show == true)
        {
            if (TagTypeToShow == "Admob")
            {
                Admob admob = new Admob();
                return admob.ShowAd(false, publisherId);
            }
            else if (TagTypeToShow == "AdmodaBanner")
            {
                Admoda admoda = new Admoda();
                return admoda.ShowAd("AdmodaBanner", publisherId);
            }
            else if (TagTypeToShow == "AdultmodaBanner")
            {
                Admoda admoda = new Admoda();
                return admoda.ShowAd("AdultmodaBanner", publisherId);
            }

            else if (TagTypeToShow == "AdmodaText")
            {
                Admoda admoda = new Admoda();
                return admoda.ShowTextAd("AdmodaText", publisherId);
            }
            else if (TagTypeToShow == "AdultmodaText")
            {
                Admoda admoda = new Admoda();
                return admoda.ShowTextAd("AdultmodaText", publisherId);
            }
            else if (TagTypeToShow == "BuzzcityBanner")
            {
                Buzzcity buzz = new Buzzcity();
                return buzz.ShowAd(publisherId);
            }
            else if (TagTypeToShow == "BuzzcityText")
            {
                Buzzcity buzz = new Buzzcity();
                return buzz.ShowTextAd(publisherId);
            }
            else if (TagTypeToShow == "Decktrade")
            {
                Decktrade dec = new Decktrade();
                return dec.ShowAd(false, publisherId);
            }
            else if (TagTypeToShow == "Google")
            {
                AdSense adsense = new AdSense();
                return adsense.ShowAd(publisherId, alternatePublisherId);
            }
            else if (TagTypeToShow == "Mojiva")
            {
                Mojiva moj = new Mojiva();
                return moj.ShowAd(false, publisherId, alternatePublisherId);
            }
            else if (TagTypeToShow == "Mkhoj")
            {
                Mkhoj mkhoj = new Mkhoj();
                return mkhoj.ShowAd(false, publisherId);
            }
            else if (TagTypeToShow == "ZestADZ")
            {
                zestadz zest = new zestadz();
                return zest.ShowAd(publisherId);
            }
            else
            {
                return string.Empty;
            }

        }
        else{
            return string.Empty;}
        }
    public void TagBuildTagUpdateVisitors(string subdomain, string domain, string location)
    {
        int secondsToKeep = 600; // seconds to keep visitor in the table bwefore delete
        string visitorSessionId =  HttpContext.Current.Session.SessionID.ToString();
        string visitorIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

        string visitorBrowser = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"];
        visitorBrowser = HttpContext.Current.Server.HtmlEncode(visitorBrowser);
        visitorBrowser = Functions.LimitString(visitorBrowser, 255);
        if (String.IsNullOrEmpty(visitorBrowser))
        {
            visitorBrowser = "Unknown browser";
        }
        if (String.IsNullOrEmpty(location))
        {
            location = "Unknown";
        }
        // check & extract ip from operamini
        if (visitorBrowser.ToLower().Contains("mini")) 
        {
            string forwardedip = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (forwardedip!= null){
            string[] forwardedipArray = forwardedip.Split(new char[] { ',' });
            //last ip in the array is the real ip
            //http://dev.opera.com/articles/view/opera-mini-request-headers/
            string extractedIp = forwardedipArray[forwardedipArray.Length - 1].ToString().Trim();
            if (Functions.IsValidIP(extractedIp) == true && Functions.IsIpLocalIP(extractedIp) == false)
                {
                    visitorIp = extractedIp;
                }
            }

        }

        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("UpdateVisitorCount", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@secondsToKeep", SqlDbType.Int).Value = secondsToKeep;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@domain", SqlDbType.VarChar).Value = domain;
                cmd.Parameters.Add("@visitorSessionId", SqlDbType.VarChar).Value = visitorSessionId;
                cmd.Parameters.Add("@visitorIp", SqlDbType.VarChar).Value = visitorIp;
                cmd.Parameters.Add("@visitorBrowser", SqlDbType.VarChar).Value = visitorBrowser;
                cmd.Parameters.Add("@location", SqlDbType.VarChar).Value = location;
                cmd.ExecuteNonQuery();

            }
        }


    }
    public string GetVisitorcount(string subdomain)
    {

        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("CountVisitors", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                return cmd.ExecuteScalar().ToString();

            }
        }


    }
    public string TagBuildTagTotalHits(string subdomain)
    {

        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("TotalHits", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                return cmd.ExecuteScalar().ToString();

            }
        }


    }
    public string TagBuildTagFile(string subdomainname, int tagRelatedId, string linkType, string title)
    {
        if (linkType == "href")
        {
            return "<a href=\"/FileHandler.ashx?id=" + tagRelatedId + "\">" + title + "</a>";
        }
        else 
        {
            return "<img src=\"/FileHandler.ashx?id=" + tagRelatedId + "\" alt=\"" + title + "\"/>";
        }
    }
    public string TagBuildTagSitemap(string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("SiteMap", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    StringBuilder s = new StringBuilder();
                    while (reader.Read())
                    {
                        s.AppendFormat("<a href=\"/Page{0}.aspx\">{1}</a><br/>",reader["id"],reader["title"]);
                    }
                    return s.ToString();
                }
            }
        }

    }
    public string TagBuildTagPoll(int id, string viewVotes, int pageid)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("poll_show_by_id", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    StringBuilder s = new StringBuilder();
                    s.Append("<form action=\"/pollVoteEngine.aspx\" method=\"post\">");
                    if (reader.Read()) // get title of the poll
                    {
                        s.Append("<p><a id=\"poll"+ id +"\"></a><input type=\"hidden\" name=\"returnUrl\" value=\"page" + pageid + ".aspx\"/><input type=\"hidden\" name=\"pollid\" value=\"" + id + "\" /><b>" + reader["pollTitle"].ToString() + "</b></p><ul style=\"list-style-type:none; padding:0; margin:0;\">");
                    }

                    reader.NextResult(); // go to next table and get choices

                    while (reader.Read())
                    {
                        s.AppendFormat("<li><input type=\"radio\" name=\"answer\" value=\"{0}\" />{1}", reader["id"], reader["answer"]);
                        if (viewVotes == "yes")
                        {
                            s.AppendFormat(" ({1}% {0} votes)<br/><img src=\"/images/vote_bar.gif\" width=\"{1}%\" height=\"5px\" alt=\"*\" />", reader["votes"], reader["mypercent"]);
                        }
                        else
                        {
                        }
                        s.Append("</li>");
                    }
                    s.Append("<li><a href=\"/captchaImage.aspx/dummy.jpg\"><img alt=\"Securityimage\" src=\"/captchaImage.aspx/dummy.jpg\"/></a> : <input name=\"captcha\" type=\"text\" size=\"4\"/><br/> <input type=\"submit\" /> <a href=\"Page" + pageid + ".aspx?v=yes#poll"+ id +"\">Results</a></li></ul></form>");
                    return s.ToString();
                }
            }
        }

    }
    public string TagBuildTagContactAdminForm(string subdomain,string submitButtonTitle, string MessageBoxTitle, string ReturnUrl)
    {
        return "<form method=\"post\" action=\"/contactSiteAdmin.aspx\"><p><input type=\"hidden\" name=\"returnUrl\" value=\"" + ReturnUrl + "\" />Email:<br/><input type=\"text\" name=\"fromEmail\"/><br/>" + MessageBoxTitle + ":<br/><textarea name=\"message\" rows=\"3\" cols=\"20\"></textarea><br/><img alt=\"Securityimage\" src=\"/captchaImage.aspx/dummy.jpg\"/> : <input name=\"captcha\" type=\"text\" size=\"4\"/><br/><input type=\"submit\" value=\"" + submitButtonTitle + "\"/></p></form>";

    }
    public DataSet TagBuildGuestbook(string subdomain, int page, int perPage)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("ListGuestBookMessages", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = page;
                cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = perPage;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    public DataSet TagBuildTagVisitorsList(string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("OnlineUserList", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }

    }
    public DataSet TagBuildTagChatMessageList(int id)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("chat_list", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@pageid", SqlDbType.Int).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;
            }
        }

    }
    public void TagBuildTagChatMessageInsert(int pageid,string u,string m,string Ip, string unBrowser)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("chat_insert", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@pageid", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@u", SqlDbType.VarChar).Value = u;
                cmd.Parameters.Add("@m", SqlDbType.VarChar).Value = m;
                cmd.Parameters.Add("@uIp", SqlDbType.VarChar).Value = Ip;
                cmd.Parameters.Add("@uBrowser", SqlDbType.VarChar).Value = unBrowser;
                cmd.ExecuteNonQuery();
            }
        }

    }
    

    public string UnderCunstruction()
    {
        StringBuilder s = new StringBuilder();
        s.Append (@"<div style=""text-align:center;"">");
        s.Append(DefaultAdverts.BuzzNonAdult());
        s.Append("<br/><br/>");
        s.Append(@"<img src=""/images/underConstruction.gif"" alt="""" /> Page is under construction. If you are the owner of this site, go to site name in your management tools and start designing it.");
        s.Append("<br/><a href=\"http://" + SiteConfiguration.GetConfig().defaultSiteDomain+"\">"+SiteConfiguration.GetConfig().siteName+"</a>");
        s.Append("</div>");
        return s.ToString();
    }
    public DataSet BuildPage(string subdomain, int pageid)
    {
            try
            {
                using (SqlConnection conn = new SqlConnection(connString))
                {
                    conn.Open();

                    using (SqlCommand cmd = new SqlCommand("BuildPage", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                        cmd.Parameters.Add("@pageid", SqlDbType.VarChar).Value = pageid;

                        using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                        {
                            using (DataSet ds = new DataSet())
                            {
                                da.Fill(ds);
                                // has 3 tables. 
                                // first status of the page with PageStatusinHTTP column. if page exists PageStatusinHTTP=200
                                // second is selecting from pages table with columns title,description,keywords
                                // third selects from pageBlocks table with columns 
                                //                    tagType,tagRelatedId,tagText,tagStyleClass,tagFrontColor,tagFontDecoration,
                                //                    tagFontWeight,tagFontSize, tagBgColor,tagBorderSize,tagBorderColor,tagHorizantalAlign,positionInPage
                                return ds;
                            }
                        }
                    }
                }
            }
            catch
            {
                            using (DataSet ds = new DataSet())
                            {
                                DataTable dt = new DataTable();
                                DataColumn PageStatusinHTTP = new DataColumn("PageStatusinHTTP");
                                 dt.Columns.Add(PageStatusinHTTP);
                                ds.Tables.Add(dt);
                                return ds;
                            }
            }
      }
    private string CssExtractHelper(string ExtractedClass)
    {
        if (ExtractedClass.StartsWith("."))
        {
            //return ExtractedClass.TrimStart(".");
            return ExtractedClass.Remove(0, 1);
        }
        else
        {
            return ExtractedClass;
        }
    }
    public DataSet CssExtract (string subdomain)
 {
     // \.[-]?[_a-zA-Z][_a-zA-Z0-9-]*|[^\0-\177]*\\[0-9a-f]{1,6}(\r\n[ \n\r\t\f])?|\\[^\n\r\f0-9a-f]*
     // (?<!url\s*\(.*)(\.[-]?[_a-zA-Z][_a-zA-Z0-9-]*|[^\0-\177]*\\[0-9a-f]{1,6}(\r\n[ \n\r\t\f])?|\\[^\n\r\f0-9a-f]*)

     Functions f = new Functions();
     DataSet ds = new DataSet();
     DataTable dt = new DataTable("ExtractedCss");
     ds.Tables.Add(dt);
     dt.Columns.Add("Cssclass", typeof(string));

     string css = f.GetCurrentStyle(subdomain);

     if (String.IsNullOrEmpty(css) == false)
     {
         MatchCollection matches = Regex.Matches(css, @"(?<!url\s*\(.*)(\.[-]?[_a-zA-Z][_a-zA-Z0-9-]*|[^\0-\177]*\\[0-9a-f]{1,6}(\r\n[ \n\r\t\f])?|\\[^\n\r\f0-9a-f]*)");

         
         foreach (Match match in matches)
         {
             dt.Rows.Add(CssExtractHelper(match.ToString()));
             
         }

     }
    
     return ds;

 }
    public int GetDefaultPage(string subdomainname)
    {
        using (SqlConnection conn = new SqlConnection())
            {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetDefaultPage", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                return (int)cmd.ExecuteScalar();
            }
        }
    }
    public bool CheckSubdomain(string subdomainname, string domainname)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("CheckSubdomain", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@domain", SqlDbType.VarChar).Value = domainname;
                SqlParameter isUserExists = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                isUserExists.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(isUserExists);
                cmd.ExecuteScalar();
                return Convert.ToBoolean(isUserExists.Value);
            }
        }
    }
    public static string Truncate(string input, int limit)
    {
        string output = input;

        if (String.IsNullOrEmpty(input)) return null;

        // Check if the string is longer than the allowed amount
        // otherwise do nothing
        if (output.Length > limit && limit > 0)
        {
            // cut the string down to the maximum number of characters
            output = output.Substring(0, limit);

            // Check if the space right after the truncate point 
            // was a space. if not, we are in the middle of a word and 
            // need to cut out the rest of it
            //if (input.Substring(output.Length, 1) != " ")
           // {
           //     int LastSpace = output.LastIndexOf(" ");

                // if we found a space then, cut back to that space
             //   if (LastSpace != -1)
               // {
                 //   output = output.Substring(0, LastSpace);
                //}
            //}
            // Finally, add the "..."
            output += "...";
        }
        return output;
    }
    public DataSet colourList()
    {
        KnownColor enumColor = new KnownColor();
        Array Colors = Enum.GetValues(enumColor.GetType());
        DataSet ds = new DataSet();
        DataTable dt = new DataTable("ColourTable");
        ds.Tables.Add(dt);
        dt.Columns.Add("ColorName");
        dt.Columns.Add("ColorHex");
        // This is to get rid of System Colors
        foreach (object clr in Colors)
        {
            if (!Color.FromKnownColor((KnownColor)clr).IsSystemColor && clr.ToString() != "Transparent")
            {
                Color c2 = Color.FromKnownColor((KnownColor)clr);
                ds.Tables["ColourTable"].Rows.Add(clr.ToString(), String.Format("#{0:X2}{1:X2}{2:X2}", c2.R, c2.G, c2.B));
            }
        }

        return ds;

    }
    public bool DeleteItem(string un, string subdomain, int pid, int tid)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteItem", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@pageid", SqlDbType.VarChar).Value = pid;
        cmd.Parameters.Add("@tagid", SqlDbType.VarChar).Value = tid;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    public bool DeleteItems(string un, string subdomain, int pid, string tidcoll)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteItems", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@pageid", SqlDbType.VarChar).Value = pid;
        cmd.Parameters.Add("@tagids", SqlDbType.VarChar).Value = tidcoll;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    public bool AddTag(int limit, string un, string subdomainname, int pageid, string tagtype, int tagRelatedId, string tagText, string tagLinkTitle, string tagLinkUrl, string tagStyleClass, string tagFontColor, string tagFontDecoration, string tagFontWeight, string tagFontSize, string tagBgColor, int tagBorderSize, string tagBorderColor, int prevPositiontagid, string AlignmentToSave)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("AddTag", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@pageWidelimit", SqlDbType.VarChar).Value = Convert.ToInt32(SiteConfiguration.GetConfig().TotalItemsLimitPerPage);
                cmd.Parameters.Add("@limit", SqlDbType.VarChar).Value = limit;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@pageId", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@tagType", SqlDbType.VarChar).Value = tagtype;
                cmd.Parameters.Add("@tagRelatedId", SqlDbType.VarChar).Value = tagRelatedId;
                cmd.Parameters.Add("@tagText", SqlDbType.VarChar).Value = tagText;
                cmd.Parameters.Add("@tagLinkTitle", SqlDbType.VarChar).Value = tagLinkTitle;
                cmd.Parameters.Add("@tagLinkUrl", SqlDbType.VarChar).Value = tagLinkUrl;
                cmd.Parameters.Add("@tagStyleClass", SqlDbType.VarChar).Value = tagStyleClass;
                cmd.Parameters.Add("@tagFontColor", SqlDbType.VarChar).Value = tagFontColor;
                cmd.Parameters.Add("@tagFontDecoration", SqlDbType.VarChar).Value = tagFontDecoration;
                cmd.Parameters.Add("@tagFontWeight", SqlDbType.VarChar).Value = tagFontWeight;
                cmd.Parameters.Add("@tagFontSize", SqlDbType.VarChar).Value = tagFontSize;
                cmd.Parameters.Add("@tagBgColor", SqlDbType.VarChar).Value = tagBgColor;
                cmd.Parameters.Add("@tagBorderSize", SqlDbType.Int).Value = tagBorderSize;
                cmd.Parameters.Add("@tagBorderColor", SqlDbType.VarChar).Value = tagBorderColor;
                cmd.Parameters.Add("@prevPositiontagid", SqlDbType.Int).Value = prevPositiontagid;
                cmd.Parameters.Add("@tagAlignment", SqlDbType.VarChar).Value = AlignmentToSave;
        
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    public bool MoveTag(string un, string subdomainname, int pageid,int tagid, int cp, string way)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("MoveTag", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomainname;
                cmd.Parameters.Add("@pageId", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@tagId", SqlDbType.Int).Value = tagid;
                cmd.Parameters.Add("@currentPosition", SqlDbType.Int).Value = cp;
                cmd.Parameters.Add("@way", SqlDbType.VarChar).Value = way;

                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    public DataSet ListPages(string subdomain, string un)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("listJustSubdomainPages", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.SelectCommand = cmd;

                    using (DataSet ds = new DataSet())
                    {
                        
                        da.Fill(ds);
                        foreach( DataRow dataRow in ds.Tables[0].Rows)
                        {
                            dataRow["title"] = dataRow["pageFolder"].ToString() + ">Page" + dataRow["id"].ToString() + ".aspx - " + HttpContext.Current.Server.HtmlEncode(Truncate(dataRow["title"].ToString(), 10));
                        }

                        return ds;
                    }
                }
            }
        }

    }
    public DataSet ListAdverts(string subdomain, string un)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("ListAdverts", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.SelectCommand = cmd;

                    using (DataSet ds = new DataSet())
                    {

                        da.Fill(ds);
                        

                        return ds;
                    }
                }
            }
        }

    }
    public DataSet ListAdvertsOnAddNewItemPage(string subdomain, string un)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("ListAdverts", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                using (SqlDataAdapter da = new SqlDataAdapter())
                {
                    da.SelectCommand = cmd;

                    using (DataSet ds = new DataSet())
                    {

                        da.Fill(ds);
                        foreach (DataRow dataRow in ds.Tables[0].Rows)
                        {
                            dataRow["advertCompany"] = dataRow["advertCompany"].ToString() + "-" + dataRow["publisherId"].ToString();
                        }

                        return ds;
                    }
                }
            }
        }

    }
    public bool UpdateTag(string subdomain, int pageid, int tagid, string TextToSave, string CssClassToSave, string FontSizeToSave, string FontColourToSave, string FontDecorationToSave, string FontWeightToSave, string TagBgColorToSave, int TagtagBorderSizeToSave, string TagBorderColourToSave, string AlignmentToSave)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("UpdateTag", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pageId", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@tagId", SqlDbType.Int).Value = tagid;
                cmd.Parameters.Add("@tagText", SqlDbType.VarChar).Value = TextToSave;
                cmd.Parameters.Add("@tagStyleClass", SqlDbType.VarChar).Value = CssClassToSave;
                cmd.Parameters.Add("@tagFontSize", SqlDbType.VarChar).Value = FontSizeToSave;
                cmd.Parameters.Add("@tagFontColor", SqlDbType.VarChar).Value = FontColourToSave;
                cmd.Parameters.Add("@tagFontDecoration", SqlDbType.VarChar).Value = FontDecorationToSave;
                cmd.Parameters.Add("@tagFontWeight", SqlDbType.VarChar).Value = FontWeightToSave;
                cmd.Parameters.Add("@tagBgColor", SqlDbType.VarChar).Value = TagBgColorToSave;
                cmd.Parameters.Add("@tagBorderSize", SqlDbType.Int).Value = TagtagBorderSizeToSave;
                cmd.Parameters.Add("@tagBorderColor", SqlDbType.VarChar).Value = TagBorderColourToSave;
                cmd.Parameters.Add("@tagAlignment", SqlDbType.VarChar).Value = AlignmentToSave;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteScalar();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    private string ReplaceImageLinks(string pageResult)
    {
        Regex regex = new Regex("(?<first><img[\\s\\w]+?.*?src=[\"|'])(?<src>.*?)(?<last>[\"|'].*?[/]?>)",
                     RegexOptions.IgnoreCase
                     | RegexOptions.CultureInvariant
                     | RegexOptions.IgnorePatternWhitespace
                     | RegexOptions.Compiled);

        // This is the replacement string
        string regexReplace = "<a href=\"${src}\">${first}http://i.tinysrc.mobi/${src}${last}</a>";


        // Replace the matched text in the InputText using the replacement pattern
        return regex.Replace(pageResult, regexReplace);

    }
    public bool IsMobile()
    {
        try //try , because sometimes user agent is empty
        {
            string ua = HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToLower();
            string[] uaArray = { "danger", "novarra", "android", "maui", "ppc", "opera mobi", "symbian", "series", "nokia", "mot-", "motorola", "lg-", "lge", "nec-", "lg/", "samsung", "sie-", "sec-", "sgh-", "sonyericsson", "sharp", "windows ce", "portalmmm", "o2-", "docomo", "philips", "panasonic", "sagem", "smartphone", "up.browser", "up.link", "googlebot-mobile", "googlebot-image", "slurp", "spring", "alcatel", "sendo", "blackberry", "opera mini", "opera 2", "netfront", "mobilephone mm", "vodafone", "avantgo", "palmsource", "siemens", "toshiba", "i-mobile", "asus", "ice", "kwc", "htc", "softbank", "playstation", "nitro", "iphone", "ipod", "google wireless transcoder", "t-mobile", "obigo", "brew" };
            bool foundInArray = false;
            foreach (string s in uaArray)
            {
                if (ua.IndexOf(s) > -1)
                {
                    foundInArray = true;
                    break;
                }
            }

            if (foundInArray==true || HttpContext.Current.Request.Browser.IsMobileDevice || HttpContext.Current.Request.ServerVariables["HTTP_ACCEPT"].Contains("application/vnd.wap.xhtml+xml"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        catch (Exception e)
        {
            return false;
        }
      }
    public string Smile(string input)
    {
        input = input.Replace(":)", "<img src=\"/images/smile/smiley.gif\" alt=\":)\"/>");
        input = input.Replace(":D", "<img src=\"/images/smile/bigsmile.gif\" alt=\":D\"/>");
        input = input.Replace(":@", "<img src=\"/images/smile/angry.gif\" alt=\":@\"/>");
        input = input.Replace(":H", "<img src=\"/images/smile/shades.gif\" alt=\":H\"/>");
        input = input.Replace(":(", "<img src=\"/images/smile/misery.gif\" alt=\":(\"/>");
        input = input.Replace(":c", "<img src=\"/images/smile/crying.gif\" alt=\":c\"/>");
        input = input.Replace(":o", "<img src=\"/images/smile/shock.gif\" alt=\":o\"/>");
        input = input.Replace(";)", "<img src=\"/images/smile/wink.gif\" alt=\";)\"/>");
        input = input.Replace(":p", "<img src=\"/images/smile/tongue.gif\" alt=\":p\"/>");
        return input;
    }
    public void AddAutoBrTag(int pageid, string tagtype, string tagText)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("AddAutoBrTag", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@pageId", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@tagType", SqlDbType.VarChar).Value = tagtype;
                cmd.Parameters.Add("@tagText", SqlDbType.VarChar).Value = tagText;
                cmd.ExecuteNonQuery();
            }
        }
    }
   }
