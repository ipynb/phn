﻿using System;
using System.Text;
using System.Net;
using System.IO;
using System.Web;
/// <summary>
/// Summary description for Admoda
/// </summary>
public class Decktrade
{
    public string ShowAd(bool isTest, string publisherId)
    {

        string dt_ua = HttpContext.Current.Request.UserAgent.ToString();
        string dt_ip = HttpContext.Current.Request.UserHostAddress.ToString();
        string dt_mode;

        string dt_asid = publisherId;

        if (isTest == true)
        {
            dt_mode = "test";
        }
        else
        {
            dt_mode = "live";
        }

        string dt_id = null;
        if (!String.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_X_UP_SUBNO"]))
        {
            dt_id = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["HTTP_X_UP_SUBNO"]);
        }
        else if (!String.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_XID"]))
        {
            dt_id = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["HTTP_XID"]);
        }
        else if (!String.IsNullOrEmpty(HttpContext.Current.Request.ServerVariables["HTTP_CLIENTID"]))
        {
            dt_id = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["HTTP_CLIENTID"]);
        }
        else
        {
            dt_id = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]);
        }

        string url = String.Format("http://ads.dt.mydas.mobi/getAd.php5?asid={0}&auid={1}&ua={2}&uip={3}&mode={4}", dt_asid, dt_id, dt_ua, dt_ip, dt_mode);
        Uri uri = new Uri(url);
        string res;

        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = "GET";
            request.ContentType = "text/html";
            request.Timeout = 1000;

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);

             res = reader.ReadToEnd().ToString();

            response.Close();
            reader.Close();
        }
        catch (Exception e)
        {
            //res = "Decktrade error";
            res = string.Empty;
        }

        return res;

    }

}
