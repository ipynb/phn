﻿/// <summary>
/// Summary description for Buzzcity
/// </summary>
using System.Web;
using System;
public class Buzzcity
{
	public string ShowAd(string publisherId)
	{
        return "<a href=\"http://click.buzzcity.net/click.php?partnerid=" + publisherId + "\"><img src=\"http://ads.buzzcity.net/show.php?partnerid=" + publisherId + "\" alt=\"\" /></a>";

	}
    public string ShowTextAd(string bz_partnerid)
        {
            // code by nolovelust http://mobile.web.tr
            if (bz_partnerid == null)
            {
                bz_partnerid = "14635"; //default partnerid
            }
            string alternate_link = "http://click.buzzcity.net/click.php?partnerid=14635";
            string alternate_link_text = "Click here";
            string bz_ads_domain = "ads.buzzcity.net";
            string bz_click_domain = "click.buzzcity.net";

            // extract IP info
            string ip = string.Empty;
            if (HttpContext.Current.Request.ServerVariables["HTTP_X_OPERAMINI_PHONE"] != null && HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                // Opera mini or proxy client
                string ip_str = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
                System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"%(\d{1,3}(?:[.]\d{1,3}){3})%", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                System.Text.RegularExpressions.Match m = r.Match(ip_str);
                if (m.Success)
                {
                    ip = m.Groups[1].ToString();
                }
            }
            if (String.IsNullOrEmpty(ip))
            {
                ip = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }

            // extract UA info
            string ua = string.Empty;
            string[] keyname_ua_arr = new string[] { "HTTP_X_DEVICE_USER_AGENT", "HTTP_X_OPERAMINI_PHONE_UA", "HTTP_USER_AGENT" };
            foreach (string keyname_ua in keyname_ua_arr)
            {
                if (HttpContext.Current.Request.ServerVariables[keyname_ua] != null)
                {
                    ua = HttpContext.Current.Server.UrlEncode(HttpContext.Current.Request.ServerVariables[keyname_ua].ToString());
                    break;
                }
            }
            if (String.IsNullOrEmpty(ua))
            {
                ua = "No browser found";
            }

            //request ad
            string url = "http://" + bz_ads_domain + "/show.php?get=1&partnerid=" + bz_partnerid + "&a=" + ua + "&i=" + ip;
            System.Text.StringBuilder bz_request_result = new System.Text.StringBuilder();
            try
            {
                System.Net.HttpWebRequest request = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                request.Method = "GET";
                request.Accept = "*/*";
                request.ContentType = "text/html";
                request.Timeout = 2000;
                using (System.Net.HttpWebResponse response = request.GetResponse() as System.Net.HttpWebResponse)
                {
                    System.IO.StreamReader reader = new System.IO.StreamReader(response.GetResponseStream());
                    bz_request_result.Append(reader.ReadToEnd());
                }
            }
            catch (Exception e)
            {
                //error
                //return e.ToString();
                return "<a href=\"http://" + alternate_link + "\">" + alternate_link_text + "</a>";
            }

            try
            {
                string[] advert = bz_request_result.ToString().Split('\n'); ;
                if (advert != null)
                {
                    string text = advert[0].ToString();
                    string cid = advert[1].ToString();
                    if (cid != "241") // if default buzzcity add, do not show!
                    {
                        // display BuzzCity TextAd
                        return "<a href=\"http://" + bz_click_domain + "/click.php?partnerid=" + bz_partnerid + "&amp;cid=" + cid + "\">" + text + "</a>";
                    }
                    else
                    {
                        return string.Empty;
                    }
                }
                else
                {
                    //no advert
                    return "<a href=\"http://" + alternate_link + "\">" + alternate_link_text + "</a>";

                }
            }
            catch
            {
                return "<a href=\"http://" + alternate_link + "\">" + alternate_link_text + "</a>";

            }


        }

}
