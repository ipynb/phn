﻿using System;
using System.Web;
using System.Text;


public class Mojiva
{
	public string ShowAd(bool isTest, string publisherId, string zoneId)
	{
		
      string MOJIVA_MODE ;

        if (isTest == true){
        MOJIVA_MODE = "test";
        }
        else{
            MOJIVA_MODE = "live";
        }

    StringBuilder mojiva_get = new StringBuilder();
    mojiva_get.Append("site=").Append(publisherId);
    mojiva_get.Append("&ua=").Append(HttpUtility.UrlEncode(HttpContext.Current.Request.UserAgent, Encoding.UTF8));
    mojiva_get.Append("&ip=").Append(HttpUtility.UrlEncode(HttpContext.Current.Request.UserHostAddress, Encoding.UTF8));
    mojiva_get.Append("&url=").Append(HttpUtility.UrlEncode(HttpContext.Current.Request.Url.ToString(), Encoding.UTF8));
    mojiva_get.Append("&zone=").Append(HttpUtility.UrlEncode(zoneId, Encoding.UTF8));
    mojiva_get.Append("&adstype=3"); // type of ads (1 - text only, 2 - images only, 3 - text + images)
    mojiva_get.Append("&key=1");
    mojiva_get.Append("&keywords="); // keywords to search ad delimited by commas (not necessary)
    mojiva_get.Append("&whitelabel=0"); // filter by whitelabel(0 - all, 1 - only whitelabel, 2 - only non-whitelabel)
    mojiva_get.Append("&premium=0"); // filter by premium status (0 - non-premium, 1 - premium only, 2 - both)
    mojiva_get.Append("&over_18=0"); // filter by ad over 18 content (0 - allow over 18 content, 1 - deny over 18 content, 2 - only over 18 content)
    mojiva_get.Append("&count=1"); // quantity of ads
    //mojiva_get.Append("&paramBORDER=").Append(HttpUtility.UrlEncode("#ffffff", Encoding.UTF8)); // ads border color
    //mojiva_get.Append("&paramHEADER=").Append(HttpUtility.UrlEncode("#ffffff", Encoding.UTF8)); // header color
    //mojiva_get.Append("&paramBG=").Append(HttpUtility.UrlEncode("#eeeeee", Encoding.UTF8)); // background color
    //mojiva_get.Append("&paramTEXT=").Append(HttpUtility.UrlEncode("#000000", Encoding.UTF8)); // text color
    //mojiva_get.Append("&paramLINK=").Append(HttpUtility.UrlEncode("#ff0000", Encoding.UTF8)); // url color
    
        
    if (MOJIVA_MODE == "test") mojiva_get.Append("&test=1");

    // send request
    StringBuilder mojiva_contents = new StringBuilder();
    try {
        byte[] mojiva_get_bytes = System.Text.Encoding.UTF8.GetBytes(mojiva_get.ToString());
        System.Net.WebRequest mojiva_request  = System.Net.WebRequest.Create("http://ads.mojiva.com/ad");
        mojiva_request.Method = "POST";
        mojiva_request.ContentType = "application/x-www-form-urlencoded";
        mojiva_request.ContentLength = mojiva_get_bytes.Length;
        mojiva_request.Timeout = 2000;
        System.IO.Stream mojiva_os = mojiva_request.GetRequestStream();
        mojiva_os.Write(mojiva_get_bytes, 0, mojiva_get_bytes.Length);
        mojiva_os.Close();
        System.Net.HttpWebResponse mojiva_response = (System.Net.HttpWebResponse)mojiva_request.GetResponse();
        System.IO.StreamReader mojiva_reader = new System.IO.StreamReader(mojiva_response.GetResponseStream());
        mojiva_contents.Append(mojiva_reader.ReadToEnd().Trim());
    } catch (Exception) {
        //mojiva_contents.Append("Mojiva Error!");
    }
    return mojiva_contents.ToString();
}

}