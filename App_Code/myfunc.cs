﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;


public class Functions
{

    private string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
    // Function to Check for AlphaNumeric.
    public static bool IsAlphaNumeric(string strToCheck)
    {
        if (strToCheck == null || strToCheck == "")
        {
            return false;
        }
        else
        {
            Regex objAlphaNumericPattern = new Regex(@"[^a-zA-Z0-9\.\-]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }
    }
    public static bool IsAlphaNumericForSubdomain(string strToCheck)
    {
        if (strToCheck == null || strToCheck == "")
        {
            return false;
        }
        else
        {
            Regex objAlphaNumericPattern = new Regex(@"[^a-zA-Z0-9\-]");
            return !objAlphaNumericPattern.IsMatch(strToCheck);
        }
    }
    public static bool IsMimeTypeImage(string mimetype)
    {
        if (mimetype.ToLower().IndexOf("image")>-1)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static int ReturnNumeric(string input)
    {
        try
        {
            return Convert.ToInt32(input);
        }
        catch
        {
            return 0;
        }
    }
    public static float ReturnFloat(string input)
    {
        try
        {
            return float.Parse(input);
        }
        catch
        {
            return 0;
        }
    }
    public static string ReturnAlphaNumeric(string input)
    {
        try
        {
            return Regex.Replace(input,@"[^a-zA-Z0-9\.\-]", "").ToString();
        }
        catch
        {
            return string.Empty;
        }
    }
    // check subdomain name if its reserved
    public bool isReservedSub(string subdomain)
    {
        if (subdomain == null || subdomain.Length == 0)
        {
            return false;
        }
        else
        {
            Regex regex = new Regex(@"\b(www|preteen|incest|snuff|rape|childsex|ncestsex|preteens|pedofilia|animalsex|animalfuck)\b",RegexOptions.IgnoreCase);
            return regex.IsMatch(subdomain);
        }
    }
    // check subdomain name if its reserved
    public bool isAdultKeyword(string input)
    {
        if (input == null || input.Length == 0)
        {
            return false;
        }
        else
        {
            Regex regex = new Regex(@"\b(mporn.me|ass|cock|cunt|piss|porn|sex|fuck|hardcore|slut|whore|bitch|pussy|dick|anal|anus)\b", RegexOptions.IgnoreCase);
            return regex.IsMatch(input);
        }
    }
    // Function to Check for email.
    public static bool IsValidEmail(string inputEmail)
    {
        if (inputEmail == null || inputEmail.Length == 0)
        {
            return false;
        }
        else
        {
            Regex regex = new Regex(@"^[\w-\.]{1,}\@([\da-zA-Z-]{1,}\.){1,}[\da-zA-Z-]{2,4}$");
            return regex.IsMatch(inputEmail);
        }

    }
    // cssUpdateSanitize
    public string cssUpdateSanitize(string input)
    {
        string pattern = @"[^A-Za-z0-9\(\)\/\{\}\#\@\,\:\~\s;\.\-\r\n]";
        Regex regex = new Regex(pattern, RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.Compiled);
        return regex.Replace(input, string.Empty);
    }
    // limit strings
    public static string LimitString(string strToLimit, int limitTo)
    {
        if (strToLimit != null && strToLimit.Length > 0)
        {
            strToLimit = strToLimit.Trim();

            if (strToLimit.Length > limitTo)
            {
                return strToLimit.Substring(0, limitTo);
            }
            else
            {
                return strToLimit;
            }
        }
        return strToLimit;
    }
    // get username from query
    public static string un
    {
        get
        {
            return Functions.LimitString(HttpContext.Current.Request["un"], 50);
        }
    }
    // get pw from query
    public static string pw
    {
        get
        {
            return Functions.LimitString(HttpContext.Current.Request["pw"], 50);
        }
    }
    // login check
    public bool LoginCheck()
    {
        //no need to use  LimitString(myun, 50) already limiting at functions.un
        string myun = Functions.un;
        string mypw = Functions.pw;

        if (Functions.IsAlphaNumeric(myun) == true && Functions.IsAlphaNumeric(mypw) == true)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("CheckLoginonSession", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
            cmd.Parameters.Add("@pw", SqlDbType.VarChar).Value = mypw;



            SqlParameter isUserExists = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
            isUserExists.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(isUserExists);

            cmd.ExecuteReader();

            bool isUserExistsResult = Convert.ToBoolean(isUserExists.Value);

            if (conn != null)
            {
                conn.Close();
            }

            return isUserExistsResult;
        }
        else
        {
            return false;
        }
    }
    public void Logout()
    {
        //no need to use  LimitString(myun, 50) already limiting at functions.un
        string myun = Functions.un;
        string mypw = Functions.pw;

        
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("Logout", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
            cmd.Parameters.Add("@pw", SqlDbType.VarChar).Value = mypw;

            cmd.ExecuteNonQuery();

    }
    // login check on login
    public bool LoginCheckOnLogin()
    {
        //no need to use  LimitString(myun, 50) already limiting at functions.un
        string myun = Functions.un;
        string mypw = Functions.pw;

        if (Functions.IsAlphaNumeric(myun) == true && Functions.IsAlphaNumeric(mypw) == true)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("CheckLogin", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
            cmd.Parameters.Add("@pw", SqlDbType.VarChar).Value = mypw;



            SqlParameter isUserExists = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
            isUserExists.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(isUserExists);

            cmd.ExecuteReader();

            bool isUserExistsResult = Convert.ToBoolean(isUserExists.Value);

            if (conn != null)
            {
                conn.Close();
            }

            return isUserExistsResult;
        }
        else
        {
            return false;
        }
    }
    // insert session
    public void InsertSession(string un, string sessionid)
    {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();
            SqlCommand cmd = new SqlCommand("InsertSession", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@sessionid", SqlDbType.VarChar).Value = sessionid;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
            cmd.ExecuteNonQuery();
    }
    // get subdomain id
    public string GetSubdomainNameById(int id)
    {
        try
        {

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("GetSubdomainNameById", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
        string myReturn = (string)cmd.ExecuteScalar();


        if (conn != null)
        {
            conn.Close();
        }

        return myReturn;

        }
        catch (Exception e)
        {   
            return e.ToString();
        }
    }
    //get domain name
    public string GetDomainNameById(int id)
    {
        try
        {

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("GetDomainNameById", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
            string myReturn = (string)cmd.ExecuteScalar();


            if (conn != null)
            {
                conn.Close();
            }

            return myReturn;

        }
        catch (Exception e)
        {
            return e.ToString();
        }
    }
    // get full subdomain id
    public string GetFullSubdomainNameById(int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("GetFullSubdomainNameById", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
        string myReturn = (string)cmd.ExecuteScalar();


        if (conn != null)
        {
            conn.Close();
        }

        return myReturn;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // get code Snippets
    public string getCodeSnippet(string subdomain, string un)
    {


        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("listCodeSnippets", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                    cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        if (reader.HasRows)
                        {

                            StringBuilder s = new StringBuilder();
                            s.Append("<ul>");
                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append(" <a href=\"manageSiteSnippetsEdit.aspx?id=" + reader["id"] + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + reader["subdomainid"] + "\"><img src=\"/images/edit.gif\" alt=\"edit\" /> " + reader["snippetName"] + "</a>");
                                s.Append(" <a onclick=\"return confirm('DELETE this code? It will be deleted from all pages too!')\" href=\"manageSiteSnippets.aspx?job=delete&amp;id=" + reader["id"] + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + reader["subdomainid"] + "\"><img src=\"/images/delete.png\" alt=\"Del\" /></a>");
                                s.Append("</li>");

                            }
                            s.Append("</ul>");
                            return (s.ToString());
                             }
                        else
                        {
                            return ("There are no code snippets");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            return ("Something is not right. Can't find subdomain!");
        }



    }
    // set index page
    public void setIndexPage(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("setIndexPage", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@pageid", SqlDbType.VarChar).Value = id;
        cmd.ExecuteNonQuery();


        if (conn != null)
        {
            conn.Close();
        }


        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    public bool checkUrl(string url)
    {
        try
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            
            if (response.StatusCode == HttpStatusCode.OK)
            {
                //if (response.ContentType == "application/rss+xml" || response.ContentType == "application/atom+xml" || response.ContentType == "text/xml")
                if (response.ContentType.ToString().IndexOf("application/xml") > -1 || response.ContentType.ToString().IndexOf("application/rss+xml") > -1 || response.ContentType.ToString().IndexOf("application/atom+xml") > -1 || response.ContentType.ToString().IndexOf("text/xml") > -1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else{
                return false;
            }


        }
        catch (Exception e)
        {   
        return false;
        }
    }
    // delete a page
    public bool DeleteAPage(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAPage", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@pageid", SqlDbType.VarChar).Value = id;
        
        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete a rss
    public bool DeleteRSSfeed(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteRSSfeed", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@feedid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete a file
    public bool DeleteAFile(string un, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAfile", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete an advert
    public bool DeleteAdvert(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAdvert", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //delete a link
    public bool DeleteALink(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteALink", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@linkid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //delete a gb post
    public bool DeleteAGbPost(string un, string subdomain, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAGbPost", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@postid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete a gb post owner
    public bool DeleteAGbPostOwner(int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAGbPostOwner", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@postid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //add a link
    public bool AddALink(string un, string subdomain, string title, string url)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddALink", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = title;
        cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = url;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //add a rss
    public bool AddRSSLink(string un, string subdomain, string title, string url)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddRSSLink", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = title;
        cmd.Parameters.Add("@url", SqlDbType.VarChar).Value = url;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete a snip
    public bool DeleteAsnip(string un, string subdomain, int id)
    {
      
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAsnip", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // delete user anounce
    public bool DeleteUserAnounce(string un, int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAnouncement", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
        cmd.Parameters.Add("@anounceid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // delete user anounce for global admin
    public bool DeleteAnounce(int id)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteAnouncementAdmin", conn);
        cmd.CommandType = CommandType.StoredProcedure;
         cmd.Parameters.Add("@anounceid", SqlDbType.VarChar).Value = id;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // add anounce for global admin
    public bool AddAnnounce(string toun, string announce, bool isPublic)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddAnnounce", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@toun", SqlDbType.VarChar).Value = toun;
        cmd.Parameters.Add("@anounce", SqlDbType.VarChar).Value = announce;
        cmd.Parameters.Add("@isPublic", SqlDbType.Bit).Value = isPublic;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // register user
    public bool RegisterUser(string myun, string mypw, string myemail, string myip, string myverificationCode)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("RegisterUser", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@pw", SqlDbType.VarChar).Value = mypw;
        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = myemail;
        cmd.Parameters.Add("@ip", SqlDbType.VarChar).Value = myip;
        cmd.Parameters.Add("@verificationCode", SqlDbType.VarChar).Value = myverificationCode;



        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);

        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    // create unique verification code
    public string GetUniqueKey(int maxSize)
    {
        char[] chars = new char[62];
        string a = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ234567890"; // dont have l and 1 to prevent mixup
        chars = a.ToCharArray();
        int size = maxSize;
        byte[] data = new byte[1];
        RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
        crypto.GetNonZeroBytes(data);
        size = maxSize;
        data = new byte[size];
        crypto.GetNonZeroBytes(data);
        StringBuilder result = new StringBuilder(size);
        foreach (byte b in data)
        { result.Append(chars[b % (chars.Length - 1)]); }
        return result.ToString();

    }
    // e-mail sender
    public bool sendVerificationMail(string toEmail, string myverificationCode, string username, string password)
    {

        try
        {
            string CurrentDomain = SiteConfiguration.GetConfig().defaultSiteDomain;
            StringBuilder mailBody = new System.Text.StringBuilder();
            mailBody.Append("Welcome to  " + CurrentDomain + ". Best mobile web site designer!");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("All you need to do is verify your e-mail by using below verification code.");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("---------------");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Your verification code is: " + myverificationCode);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Please go to http://" + CurrentDomain + " to verify your account.");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("---------------");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Your details as follows");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Username: " + username);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Password: " + password);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("---------------");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("http://" + CurrentDomain);

            MailMessage oMsg = new MailMessage();
            oMsg.From = new MailAddress(SiteConfiguration.GetConfig().emailServerDefaultFromMail);
            oMsg.To.Add(new MailAddress(toEmail));
            oMsg.Subject = CurrentDomain + " verification code!";
            oMsg.IsBodyHtml = false;
            oMsg.Body = mailBody.ToString();
            // ADD AN ATTACHMENT.
            // TODO: Replace with path to attachment.
            //String sFile = @"C:\temp\Hello.txt";
            //MailAttachment oAttch = new MailAttachment(sFile, MailEncoding.Base64);
            //oMsg.Attachments.Add(oAttch);
            // TODO: Replace with the name of your remote SMTP server.
            SmtpClient SmtpMail = new SmtpClient(SiteConfiguration.GetConfig().emailServer);
            SmtpMail.Port = Convert.ToInt32(SiteConfiguration.GetConfig().emailServerPort);
            SmtpMail.Send(oMsg);
            oMsg = null;
            //oAttch = null;
            return true;
        }
        catch (Exception e)
        {
            return false;

            //HttpContext.Current.Response.Output.Write("{0} Exception caught.", e);

        }
    }
    // remind password function
    public string RemindPassword(string myun, string myemail)
    {
        //try
        //{

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("GetUserPassword", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = myemail;
        string myReturn = (string)cmd.ExecuteScalar();


        if (conn != null)
        {
            conn.Close();
        }

        return myReturn;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //remind password mail sender
    public bool sendPasswordReminderMail(string toEmail, string username, string password)
    {

        try
        {
            string CurrentDomain = HttpContext.Current.Request.Url.Host.ToString();
            StringBuilder mailBody = new System.Text.StringBuilder();
            mailBody.Append("Thank you for requesting your password. Below are your current username and password details.");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("---------------");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Username: " + username);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("Password: " + password);
            mailBody.Append(Environment.NewLine);
            mailBody.Append("---------------");
            mailBody.Append(Environment.NewLine);
            mailBody.Append("http://" + CurrentDomain);

            MailMessage oMsg = new MailMessage();
            oMsg.From = new MailAddress(SiteConfiguration.GetConfig().emailServerDefaultFromMail);
            oMsg.To.Add(new MailAddress(toEmail));
            oMsg.Subject = CurrentDomain + " password reminder.";
            oMsg.IsBodyHtml = false;
            oMsg.Body = mailBody.ToString();
            oMsg.Body = mailBody.ToString();
            // ADD AN ATTACHMENT.
            // TODO: Replace with path to attachment.
            //String sFile = @"C:\temp\Hello.txt";
            //MailAttachment oAttch = new MailAttachment(sFile, MailEncoding.Base64);
            //oMsg.Attachments.Add(oAttch);
            // TODO: Replace with the name of your remote SMTP server.
            SmtpClient SmtpMail = new SmtpClient(SiteConfiguration.GetConfig().emailServer);
            SmtpMail.Port = Convert.ToInt32(SiteConfiguration.GetConfig().emailServerPort);
            SmtpMail.Send(oMsg);
            oMsg = null;
            //oAttch = null;
            return true;
        }
        catch (Exception e)
        {
            return false;

            //HttpContext.Current.Response.Output.Write("{0} Exception caught.", e);

        }
    }
    // list subdomains
    public void listUsersSubdomains(string myun, int showDeleteMode)
    {
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            // instantiate and open connection
            conn = new
            SqlConnection(connString);
            conn.Open();

            // don't ever do this!
            //			SqlCommand cmd = new SqlCommand(
            //				"select * from Customers where city = '" + inputCity + "'";

            // 1. declare command object with parameter
            SqlCommand cmd = new SqlCommand("listUsersSubdomains", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;


            // get data stream
            reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                StringBuilder s = new StringBuilder();
                s.Append("<ul>");
                // write each record
                while (reader.Read())
                {
                    s.Append("<li>");
                    s.AppendFormat("<a href=\"manageSite.aspx?subdomainid={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">{1}.{2}</a> ({3} hits)", reader["id"], reader["subdomain"], reader["domain"], reader["totalhits"]);
                    // adult?? not
                    if ((bool)reader["isR"] == true)
                    {
                        s.Append(" <img src=\"/images/adult.png\" alt=\"R\" /> ");
                    }
                    //is active inactive
                    if ((bool)reader["isActive"] == true)
                    {
                        s.Append("<img src=\"/images/flag_green.gif\" alt=\"Active\" /> ");
                    }
                    else
                    {
                        s.Append("<img src=\"/images/flag_red.gif\" alt=\"Inctive\" /> ");
                    }
                    //delete mode
                    if (showDeleteMode == 1)
                    {
                        s.Append("<a onclick=\"return confirm('DELETE this site? All pages will be deleted too!')\" href=\"manageSiteDeleteSite.aspx?un="+Functions.un +"&amp;pw="+Functions.pw+"&amp;subdomain=" + reader["subdomain"] + "\"><img src=\"/images/delete.png\" alt=\"Del\" /></a>");
                    }
                    s.Append("</li>");
                }
                s.Append("</ul>");
                HttpContext.Current.Response.Write(s);
            }
            else
            {
                HttpContext.Current.Response.Write("You haven't got any sites yet. Start with creating a site. Click link below to start.<br/>");
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
        finally
        {
            // close reader
            if (reader != null)
            {
                reader.Close();
            }

            // close connection
            if (conn != null)
            {
                conn.Close();
            }
        }

    }
    public DataSet listUsersSubdomains2(string myun)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("listUsersSubdomains", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    // list available domains
    public void listAvailableDoaminsFromWEbConfig()
    {

        List<string> myDomains = new List<string>();

        string domains = string.Empty;

        if (SiteConfiguration.GetConfig().AvailableDomains.Count == 0)
            domains = "";
        else
        {
            HttpContext.Current.Response.Write("<select name=\"domain\">");
            foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AvailableDomains)
                myDomains.Add(string.Format("<option value=\"{0}\">{0}</option>", domain.Name));
            HttpContext.Current.Response.Write(string.Join("", myDomains.ToArray()));
            HttpContext.Current.Response.Write("</select>");
        }




    }
    public void listAvailableDoaminsFromWEbConfig2()
    {

        List<string> myDomains = new List<string>();

        string domains = string.Empty;

        if (SiteConfiguration.GetConfig().AvailableDomains.Count == 0)
            domains = "";
        else
        {

            foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AvailableDomains)
                myDomains.Add(string.Format("<b>subdomain.{0}</b><br/>", domain.Name));
            HttpContext.Current.Response.Write(string.Join("", myDomains.ToArray()));

        }




    }
    // update style
    public bool UpdateStyleSheet(string myun, string subdomain, string newStyle)
    {

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("UpdateStyle", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
            cmd.Parameters.Add("@style", SqlDbType.VarChar).Value = newStyle;


            SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
            success.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(success);

            cmd.ExecuteReader();

            bool successResult = Convert.ToBoolean(success.Value);

            if (conn != null)
            {
                conn.Close();
            }

            return successResult;

    }
    // update rating
    public string GetCurrentStyle(string subdomain)
    {
       
        SqlConnection conn = null;
        SqlDataReader reader = null;

        try
        {
            // instantiate and open connection
            conn = new SqlConnection(connString);
            conn.Open();


            SqlCommand cmd = new SqlCommand("GetCurrentStyle", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;




            // get data stream
            reader = cmd.ExecuteReader();


            if (reader.Read())
            {

                return reader["style"].ToString();


            }
            else
            {
              return string.Empty;
            }


        }
        finally
        {
            if (conn != null)
            {
                conn.Close();
            }
        }
    }
    //change password
    public bool ChangePassword(string myun, string mynewpw)
    {

        if (Functions.IsAlphaNumeric(mynewpw) == true)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = connString;
            conn.Open();

            SqlCommand cmd = new SqlCommand("ChangePassword", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
            cmd.Parameters.Add("@pw", SqlDbType.VarChar).Value = mynewpw;



            SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
            success.Direction = ParameterDirection.ReturnValue;
            cmd.Parameters.Add(success);

            cmd.ExecuteReader();

            bool successResult = Convert.ToBoolean(success.Value);

            if (conn != null)
            {
                conn.Close();
            }

            return successResult;
        }
        else
        {
            return false;
        }
    }
    // add new site
    public bool RegisterSubdomain(string myun, string subdomain, string domain, int isR)
    {
        //try
        //{
        //if (domain == "mporn.me")
        //{
        //    isR = 1;
        //}
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("RegisterSubdomain", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@domain", SqlDbType.VarChar).Value = domain;
        cmd.Parameters.Add("@limit", SqlDbType.Int).Value = Convert.ToInt32(SiteConfiguration.GetConfig().subdomainLimitPerUser);
        cmd.Parameters.Add("@isR", SqlDbType.Bit).Value = isR;


        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);

        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

        //}
        //catch (Exception e)
        //{   
        //    return false;
        //}
    }
    //check if domain is in webconfig
    public int isDomaininWebConfig(string inputdomain)
    {
        List<string> myDomains = new List<string>();

        if (SiteConfiguration.GetConfig().AvailableDomains.Count == 0)
        { return -1; }

        else
        {
            //populate list
            foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AvailableDomains)
                myDomains.Add(domain.Name);

            //serach list
            return myDomains.IndexOf(inputdomain);

        }
    }
    //change email
    public bool ChangeEmail(string myun, string newemail)
    {

        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("ChangeEmail", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = newemail;


        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);

        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);

        if (conn != null)
        {
            conn.Close();
        }

        return successResult;
    }
    // de validate account
    public void deValidateAccount(string myun, string newVerificationCode)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DevalidateAccount", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@newVerificationCode", SqlDbType.VarChar).Value = newVerificationCode;



        cmd.ExecuteReader();

        if (conn != null)
        {
            conn.Close();
        }

    }
    // validate account
    public bool ValidateAccount(string myemail, string verificationCode)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("validateAccount", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@email", SqlDbType.VarChar).Value = myemail;
        cmd.Parameters.Add("@verif", SqlDbType.VarChar).Value = verificationCode;


        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);


        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // is glogal admin
    public bool IsGlobalAdmin(string myun)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("IsGlobalAdmin", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);


        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // add forum topic
    public bool AddForumTopic(string myun, string topic, string replyBody, bool sticky)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddForumTopic", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@topic", SqlDbType.VarChar).Value = topic;
        cmd.Parameters.Add("@replyBody", SqlDbType.VarChar).Value = replyBody;
        cmd.Parameters.Add("@isSticky", SqlDbType.Bit).Value = sticky;


        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);


        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // add reply forum topic
    public bool AddReplyToForumTopic(string myun, int id, string replyBody)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddReplyToForumTopic", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = id;
        cmd.Parameters.Add("@replyBody", SqlDbType.VarChar).Value = replyBody;



        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);


        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // list forum topics
    public void listForumTopics(int pageNumber, int pageSize)
    {
      

        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ListForumTopics", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {
                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);
                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }
                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append("<b>");
                                s.Append(reader["un"]);
                                s.Append(" <a href=\"forumView.aspx?id=" + reader["id"] + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">" + reader["topic"] + "</a>");
                                s.Append("</b>");
                                if ((bool)reader["isSticky"] == true)
                                {
                                    s.Append(" <img src=\"/images/icon_sticky.gif\" alt=\"\" />");

                                }
                                if (HttpContext.Current.Session["IsGlobalAdmin"].ToString() == "yes")
                                {
                                    s.Append(" <a onclick=\"return confirm('Are you sure?')\" href=\"forumManage.aspx?id=" + reader["id"] + "&amp;del=topic&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\"><img src=\"/images/delete.png\" alt=\"\" /></a>");
                                }
                                s.Append("<br/>");
                                s.AppendFormat("Last reply by <b>{1}</b> on {0:ddd, dd MMM yy HH:mm}", reader["updateDate"], reader["lastReplyBy"]);
                                s.Append("</li>");



                            }
                            s.Append("<li>Page " + currentPage + " of " + totalPages + "</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"forums.aspx?p={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"forums.aspx?p={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no topics.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
      }
    // list forum topics
    public void ViewForumTopic(int id, int pageNumber, int pageSize)
    {


        try
        {

            using (
            SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ViewForumTopic", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;



                    // get data stream
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {

                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);

                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }




                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            StringBuilder s = new StringBuilder();
                            s.Append("<ul>");
                            // write each record
                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.AppendFormat("<b>{0} on {1:ddd, dd MMM yy HH:mm}</b>", reader["un"], reader["postDate"]);
                                if (HttpContext.Current.Session["IsGlobalAdmin"].ToString() == "yes")
                                {
                                    s.Append(" <a onclick=\"return confirm('Are you sure?')\" href=\"forumManage.aspx?id=" + reader["id"] + "&amp;del=reply&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\"><img src=\"/images/delete.png\" alt=\"\" /></a>");
                                }
                                s.Append("<br/>" + reader["replyBody"]);
                                s.Append("</li>");




                            }
                            s.Append("<li>Page " + currentPage + " of " + totalPages + "</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"forumView.aspx?p={0}&amp;id=" + id + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"forumView.aspx?p={0}&amp;id=" + id + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no posts.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }

    }
    // replace line breaks for forum posts
    public string GetPostTopic(int id)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();
        SqlCommand cmd = new SqlCommand("GetPostTopic", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

        string _return = cmd.ExecuteScalar().ToString();

        if (conn != null)
        {
            conn.Close();
        }
        return _return;
    }
    //delete topic
    public void DeleteTopic(int id, int what)
    {
        //what
        //1 topic
        //2 post
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();
        SqlCommand cmd = new SqlCommand("DeleteTopic", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
        cmd.Parameters.Add("@what", SqlDbType.Int).Value = what;

        cmd.ExecuteScalar();

        if (conn != null)
        {
            conn.Close();
        }
    }
    //delete a page for owner
    public void DeleteAPageOwner(int id)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("DeleteAPageOwner", conn))
            {
            cmd.CommandType = CommandType.StoredProcedure;

            cmd.Parameters.Add("@pageid", SqlDbType.Int).Value = id;

            cmd.ExecuteScalar();
            }

        }

    }
    // replace br etc
    public string ReplaceLineBreak(string inputStr)
    {
        StringBuilder s = new StringBuilder(inputStr);
        s.Replace("'", "`");
        //s.Replace("<", "&lt");
        //s.Replace("&", "&amp;");
        //s.Replace(">", "&gt");
        s.Replace("\r\n", "<br />\r\n");
         return s.ToString();
    }
    public string SqlProtect(string inputStr)
    {
        StringBuilder s = new StringBuilder(inputStr);
        s.Replace("'", "`");
        return s.ToString();
    }
    public string NewLineBreak(string inputStr)
    {
        StringBuilder s = new StringBuilder(inputStr);
        s.Replace("\r\n", "<br />\r\n");
        return s.ToString();
    }
    // clean html
    public string CleanHtml(string inputStr)
    {
        StringBuilder s = new StringBuilder(inputStr);
        s.Replace("'", "`");
        //s.Replace("&", "&amp;");
        s.Replace("\r\n", " ");
        return s.ToString();
    }
    // replace br for titles
    public string ReplaceLineBreakForTitles(string inputStr)
    {
        StringBuilder s = new StringBuilder(inputStr);
        s.Replace("'", "`");
        //s.Replace("<", "&lt");
        //s.Replace(">", "&gt");
        //s.Replace("&", "&amp;");
        s.Replace("\r\n", " ");
        return s.ToString();
    }
    // ad site page
    public bool AddSitePage(string myun, string subdomain, string title, string description, string keywords, string fontSize, string pageBgColor, string pageFontColor, string pageFolder, string pageOtherMetaTags)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("AddSitePage", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = title;
        cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = description;
        cmd.Parameters.Add("@keywords", SqlDbType.NVarChar).Value = keywords;
        cmd.Parameters.Add("@other_meta", SqlDbType.VarChar).Value = pageOtherMetaTags;
        cmd.Parameters.Add("@fontSize", SqlDbType.VarChar).Value = fontSize;
        cmd.Parameters.Add("@pageBgColor", SqlDbType.VarChar).Value = pageBgColor;
        cmd.Parameters.Add("@pageFontColor", SqlDbType.VarChar).Value = pageFontColor;
        cmd.Parameters.Add("@pageFolder", SqlDbType.VarChar).Value = pageFolder;
        cmd.Parameters.Add("@limit", SqlDbType.Int).Value = Convert.ToInt32(SiteConfiguration.GetConfig().pageLimitPerSubdomain);



        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);
        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    //copy 
     public bool CopySitePage(string un, string subdomain, int pageToCopy)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("CopyAPage", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pageid", SqlDbType.Int).Value = pageToCopy;
                cmd.Parameters.Add("@limit", SqlDbType.Int).Value = Convert.ToInt32(SiteConfiguration.GetConfig().pageLimitPerSubdomain);

                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);
                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    // update site page
     public bool UpdateSitePage(string myun, string subdomain, int pageid, string title, string description, string keywords, string fontSize, string pageBgColor, string pageFontColor, string pageFolder, string pageOtherMetaTags)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("UpdateSitePage", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
        cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = pageid;
        cmd.Parameters.Add("@title", SqlDbType.NVarChar).Value = title;
        cmd.Parameters.Add("@description", SqlDbType.NVarChar).Value = description;
        cmd.Parameters.Add("@keywords", SqlDbType.NVarChar).Value = keywords;
        cmd.Parameters.Add("@fontSize", SqlDbType.VarChar).Value = fontSize;
        cmd.Parameters.Add("@pageBgColor", SqlDbType.VarChar).Value = pageBgColor;
        cmd.Parameters.Add("@pageFontColor", SqlDbType.VarChar).Value = pageFontColor;
        cmd.Parameters.Add("@pageFolder", SqlDbType.VarChar).Value = pageFolder;
        cmd.Parameters.Add("@other_meta", SqlDbType.VarChar).Value = pageOtherMetaTags;


        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);
        cmd.ExecuteReader();

        bool successResult = Convert.ToBoolean(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // change site status
    public int ChangeSiteStatus(string myun, string subdomain)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("ChangeSiteStatus", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
 
        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);
        cmd.ExecuteReader();

       int successResult = Convert.ToInt32(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // change site rating
    public int ChangeSiteRating(string myun, string subdomain)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("ChangeSiteRating", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);
        cmd.ExecuteReader();

        int successResult = Convert.ToInt32(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // change deelte site
    public int DeleteSite(string myun, string subdomain)
    {
        SqlConnection conn = new SqlConnection();
        conn.ConnectionString = connString;
        conn.Open();

        SqlCommand cmd = new SqlCommand("DeleteSite", conn);
        cmd.CommandType = CommandType.StoredProcedure;

        cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
        cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

        SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
        success.Direction = ParameterDirection.ReturnValue;
        cmd.Parameters.Add(success);
        cmd.ExecuteReader();

        int successResult = Convert.ToInt32(success.Value);


        if (conn != null)
        {
            conn.Close();
        }

        return successResult;

    }
    // get Announcements
    public string getAnnouncements(string myun)
    {
        //caching this caches username and password in more link! do not cache
        //if (HttpContext.Current.Cache["anounces"] == null)
        //{

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();
                using (SqlCommand cmd = new SqlCommand("getAnnouncements", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = myun;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {



                        if (reader.HasRows)
                        {

                            StringBuilder s = new StringBuilder();
                            s.Append("<div class=\"title\"><img src=\"/images/icon_sticky.gif\" alt=\"\"/> Announcements</div>");
                            s.Append("<ul>");

                            int i = 0;

                            while (reader.Read())
                            {

                                s.Append("");
                                s.AppendFormat("<li><b>{0:ddd, dd MMM yy}</b> ", reader["msgDate"]);
                                if ((bool)reader["isPublic"] == false)
                                {
                                    s.Append(" <a onclick=\"return confirm('Delete this message?')\" href=\"manageSiteDeleteMessage.aspx?un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;id=" + reader["id"] + "\"><img src=\"../images/delete.png\" alt=\"-\"/></a> - <b>PRIVATE</b><br/> ");
                                }
                                else
                                {
                                    s.Append("- <b>PUBLIC</b><br/> ");
                                }
                                s.Append(reader["anounce"] + "</li>");

                                i++;
                                if (i == 2) { break; }

                            }
                            s.Append("<li><a href=\"manageSiteReadMessages.aspx?un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">More..</a></li>");

                            s.Append("</ul>");
                            return s.ToString();
                        }

                            //HttpContext.Current.Cache.Insert(
                        //"anounces",
                        //s.ToString(),
                        //null,
                        //DateTime.Now.AddMinutes(30), Cache.NoSlidingExpiration);

                        else
                        {
                            return string.Empty;
                        }
                        
                    }
                }
            }
        //}
        //else
        //{
        //    return HttpContext.Current.Cache["anounces"].ToString();
        //}
       
    
    }
    public bool addAdvert(string un, string subdomain, string advertCompany, string advertPublisherId, int limit)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("AddAdvert", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@advertCompany", SqlDbType.VarChar).Value = advertCompany;
                cmd.Parameters.Add("@advertPublisherId", SqlDbType.VarChar).Value = advertPublisherId;
                cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    public bool addAdvert(string un, string subdomain, string advertCompany, string advertPublisherId, string alternateId, int limit)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("AddAdvertTwo", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@advertCompany", SqlDbType.VarChar).Value = advertCompany;
                cmd.Parameters.Add("@advertPublisherId", SqlDbType.VarChar).Value = advertPublisherId;
                cmd.Parameters.Add("@alternatePublisherId", SqlDbType.VarChar).Value = alternateId;
                cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
    // add codesnippest
    public bool AddCodeSnippet(string un, string subdomain, string snipName, string code, int limit)
    {


        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("AddCodeSnippet", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = snipName;
                cmd.Parameters.Add("@code", SqlDbType.NVarChar).Value = code;
                cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;

                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);
                cmd.ExecuteReader();

                int successResult = Convert.ToInt32(success.Value);
                return Convert.ToBoolean(success.Value);
            }
        }

    }
    // update snip
    public bool UpdateSnip(string un, string subdomain, string snippetName, string snippetCode, int snipid)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("UpdateCodeSnippet", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@name", SqlDbType.NVarChar).Value = snippetName;
                cmd.Parameters.Add("@code", SqlDbType.NVarChar).Value = snippetCode;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = snipid;

                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);

                cmd.ExecuteReader();

                return Convert.ToBoolean(success.Value);

            }
        }

    }
    // listCodeSnippets
    public DataSet ListCodeSnippets(string un, string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("listCodeSnippets", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    //list polls
    public DataSet ListPolls(string un, string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_list", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    // list folders
    public DataSet ListFolders(string un, string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("listFolders", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow dataRow in ds.Tables[0].Rows)
                {
                    dataRow["folderName2"] = dataRow["folderName2"].ToString() + " (" + dataRow["pagesInFolder"].ToString() + ")";
                }

                return ds;

            }
        }

    }
    //list illegal sites
    public DataSet ListIllegalSites(string keyword)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("ADMIN_Illegal_keyword_check", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@q", SqlDbType.VarChar).Value = keyword;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    //list illegal guestbookposts
    public DataSet ListIllegalGuestBooks(string keyword)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("ADMIN_Illegal_keyword_check_in_gb", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@q", SqlDbType.VarChar).Value = keyword;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    // list rotator links
    public DataSet ListRotatorLinks(string un, string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("listRotatorLinks", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    // list rotator links
    public DataSet ListRSSLinks(string un, string subdomain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("listRSSLinks", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    public static string ReturnCodeSnippetName(string un, string subdomain, int id)
    {
        try
        {
            Functions f = new Functions();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = f.connString;
            conn.Open();
            SqlCommand cmd = new SqlCommand("ReturnCodeSnippetName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            string _return = cmd.ExecuteScalar().ToString();

            if (conn != null)
            {
                conn.Close();
            }
            return _return;
        }
        catch
        {
            return "Snippet not found";
        }
    }
    public static string ReturnFeedName(string un, string subdomain, int id)
    {
        try
        {
            Functions f = new Functions();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = f.connString;
            conn.Open();
            SqlCommand cmd = new SqlCommand("ReturnFeedName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            string _return = cmd.ExecuteScalar().ToString();

            if (conn != null)
            {
                conn.Close();
            }
            return _return;
        }
        catch
        {
            return "Feed not found";
        }
    }
    public static string ReturnFileMimeType(int id)
    {

        Functions f = new Functions();
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = f.connString;
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("ReturnFileMimeType", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader["fileType"].ToString();
                    }
                    else
                    {
                        return "text/plain";
                    }
                }
            }

        }

    }
    public static string ReturnAdvertName(string un, string subdomain, int id)
    {
        try
        {
            Functions f = new Functions();
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = f.connString;
            conn.Open();
            SqlCommand cmd = new SqlCommand("ReturnAdvertName", conn);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
            cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
            cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

            string _return = cmd.ExecuteScalar().ToString();

            if (conn != null)
            {
                conn.Close();
            }
            return _return;
        }
        catch
        {
            return "Advert not found";
        }
    }
    public void listAllSitesNormal(int pageNumber, int pageSize)
    {


        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ListSites", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {
                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);
                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }
                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append(" <a href=\"http://" + reader["subdomain"] + "." + reader["domain"] + "\">" + reader["subdomain"] + "." + reader["domain"] + "</a>");
                                s.Append("</li>");

                            }
                            s.Append("<li>Total " + recordCount + " sites in " + totalPages + " pages</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sites.aspx?p={0}\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sites.aspx?p={0}\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no sites.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
    }
    public void listGlobalOnlines()
    {
        try
        {
            using (SqlConnection conn = new SqlConnection(connString))
            {
                conn.Open();
                using (SqlCommand cmd = new SqlCommand("OnlineUserList", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = string.Empty;
                     using (SqlDataReader reader = cmd.ExecuteReader())
                    {

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.AppendFormat(Functions.GetCountryFlag(reader["visitorIp"].ToString()) + " <b><a href=\"http://{3}.{1}/page{4}.aspx\">Visitor-{0}</a></b> on {5: HH:mm UTC}<br/><small>{2}</small><br/>", reader["id"], reader["domain"], reader["visitorBrowser"], reader["subdomain"], reader["location"], reader["lastseen"]);
                                s.Append("</li>");

                            }
                            HttpContext.Current.Response.Write(s.ToString());

                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no visitors.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
    }
    public void listAllSitesAdult(int pageNumber, int pageSize)
    {


        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ListSitesAdults", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {
                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);
                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }
                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append(" <a href=\"http://" + reader["subdomain"] + "." + reader["domain"] + "\">" + reader["subdomain"] + "." + reader["domain"] + "</a>");
                                s.Append("</li>");

                            }
                            s.Append("<li>Total " + recordCount + " sites in " + totalPages + " pages</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesAdult.aspx?p={0}\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesAdult.aspx?p={0}\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no sites.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
    }
    public bool UploadFile(string un, string fileName , int fileSize, string fileType, byte[] fileData)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("UploadFile", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@fileName", SqlDbType.NVarChar).Value = fileName;
                cmd.Parameters.Add("@fileSize", SqlDbType.Int).Value = fileSize;
                cmd.Parameters.Add("@fileType", SqlDbType.VarChar).Value = fileType;
                cmd.Parameters.Add("@fileData", SqlDbType.VarBinary).Value = fileData;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);
                cmd.ExecuteScalar();
                return Convert.ToBoolean(success.Value);

            }
        }

    }
    public int GetCurrentTotalUploads(string un)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetCurrentTotalUploads", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return Convert.ToInt32(reader["totalFileSize"]);
                    }
                    else
                    {
                        return 0;
                    }
                }


            }
        }
    }
    // list uploaded files
    public DataSet ListUploadedFiles(string un)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("ListUploadedfiles", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    public void listPopularSitesNormal(int pageNumber, int pageSize)
    {


        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ListSitesPopular", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {
                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);
                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }
                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append(" <a href=\"http://" + reader["subdomain"] + "." + reader["domain"] + "\">" + reader["subdomain"] + "." + reader["domain"] + " ("+ reader["hits"] +" hits)</a>");
                                s.Append("</li>");

                            }
                            s.Append("<li>Total " + recordCount + " sites in " + totalPages + " pages</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesPopular.aspx?p={0}\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesPopular.aspx?p={0}\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no sites.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
    }
    public void listPopularSitesAdult(int pageNumber, int pageSize)
    {


        try
        {

            using (SqlConnection conn = new SqlConnection(connString))
            {

                conn.Open();


                using (SqlCommand cmd = new SqlCommand("ListSitesPopularAdult", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@PageNumber", SqlDbType.Int).Value = pageNumber;
                    cmd.Parameters.Add("@PageSize", SqlDbType.Int).Value = pageSize;

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {


                        int currentPage;
                        int totalPages;
                        int recordCount;

                        if (reader.Read())
                        {
                            currentPage = Convert.ToInt32(reader["CurrentPage"]);
                            totalPages = Convert.ToInt32(reader["TotalPages"]);
                            recordCount = Convert.ToInt32(reader["TotalRows"]);
                        }
                        else
                        {
                            currentPage = 1;
                            totalPages = 1;
                            recordCount = 0;
                        }
                        reader.NextResult();

                        if (reader.HasRows)
                        {
                            HttpContext.Current.Response.Write("<ul>");
                            // write each record
                            StringBuilder s = new StringBuilder();

                            while (reader.Read())
                            {
                                s.Append("<li>");
                                s.Append(" <a href=\"http://" + reader["subdomain"] + "." + reader["domain"] + "\">" + reader["subdomain"] + "." + reader["domain"]  +" (" + reader["hits"] + " hits)</a>");
                                s.Append("</li>");

                            }
                            s.Append("<li>Total " + recordCount + " sites in " + totalPages + " pages</li>");
                            HttpContext.Current.Response.Write(s.ToString());

                            //paging begin
                            if (currentPage < totalPages)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesPopularAdult.aspx?p={0}\">Next page</a></li>", currentPage + 1); }
                            if (currentPage > 1)
                            { HttpContext.Current.Response.Output.Write("<li><a class=\"faint\" href=\"sitesPopularAdult.aspx?p={0}\">Previous page</a></li>", currentPage - 1); }

                            //paging end
                            HttpContext.Current.Response.Write("</ul>");
                        }
                        else
                        {
                            HttpContext.Current.Response.Write("There are no sites.");
                        }
                    }
                }
            }
        }
        catch (SqlException e)
        {
            HttpContext.Current.Response.Output.Write(String.Format("An exception occurred" +
                    " : {0}. Please contact your system administrator.",
                    e.Message));
        }
    }
    public string SitemapInXml(string subdomain, string domain)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();
            using (SqlCommand cmd = new SqlCommand("SiteMap", conn))
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    StringBuilder s = new StringBuilder();
                    while (reader.Read())
                    {
                        s.AppendFormat("<url><loc>http://"+subdomain+"." + domain + "/Page{0}.aspx</loc> <mobile:mobile/><changefreq>monthly</changefreq></url>", reader["id"]);
                    }
                    return s.ToString();
                }
            }
        }

    }
    public static string GetCountryFlag(string ip)
    {
        string cc = "-";
        //string cn = "Unknown";

        using (CountryLookup cl = new CountryLookup(HttpContext.Current.Server.MapPath("~/App_Data/GeoIP.dat")))
        {
            try
            {
                cc = cl.lookupCountryCode(ip);
            }
            catch (Exception ex)
            {
                cc = "-";
            }
        }

         return "<img src=\"/images/flags/" + cc + ".png\" alt=\"" + ip + "\"/>";

    }
    //add poll 
    public int addPoll(string un, string subdomain, string pollTitle, int limit)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_add", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pollTitle", SqlDbType.VarChar).Value = pollTitle;
                cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;
                SqlParameter pollId = new SqlParameter("@RETURN_VALUE", SqlDbType.Int);
                pollId.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(pollId);
                cmd.ExecuteScalar();
                return ReturnNumeric(pollId.Value.ToString());
            }
        }
    }
    //add poll 
    public void addPollChoice(string un, string subdomain, string answer, int pollid)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_add_answer", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@answer", SqlDbType.VarChar).Value = answer;
                cmd.Parameters.Add("@pollid", SqlDbType.Int).Value = pollid;
                cmd.ExecuteNonQuery();
            }
        }
    }
    // reset a poll
    public bool ResetPoll(string un, string subdomain, int pollid)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_reset", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pollid", SqlDbType.Int).Value = pollid;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);
                cmd.ExecuteScalar();
                return Convert.ToBoolean(success.Value);

            }
        }
    }
    // delete a poll
    public bool DeletePoll(string un, string subdomain,  int pollid)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_delete", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pollid", SqlDbType.Int).Value = pollid;
                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);
                cmd.ExecuteScalar();
                return Convert.ToBoolean(success.Value);
            }
        }
    }
    //show poll
    public DataSet ShowPoll(string subdomain, int id)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_show", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    //show poll title
    public string GetPollTitleById(int id)
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_get_title_by_id", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                return cmd.ExecuteScalar().ToString();

            }
        }

    }
    public static string GetPollChoices(int id)
    {

        StringBuilder sb = new StringBuilder();
        Functions f = new Functions();
        using (SqlConnection conn = new SqlConnection(f.connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("poll_show_by_id", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;

                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    reader.NextResult();

                    while (reader.Read())
                    {
                        sb.AppendFormat("- {0} ({1}% {2} votes)<br/>", reader["answer"].ToString(), reader["mypercent"].ToString(), reader["votes"].ToString());

                    }
                }
            }
        }
        return sb.ToString();
    }
    public static bool IsValidIP(string addr)
    {
        string pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
        Regex check = new Regex(pattern);
        bool valid = false;
        if (addr == "")
        {
            valid = false;
        }
        else
        {
            valid = check.IsMatch(addr, 0);
        }
        return valid;
    }
    //for opera mini ip check some times opera mini doesn't send real ip but lan ip of user
    public static bool IsIpLocalIP(string ip)
    {
        if (    ip.StartsWith("10.") || 
                ip.StartsWith("174.") || 
                ip.StartsWith("192.168") ||
                ip.StartsWith("172.") ||
                ip.StartsWith("127.") ||
                ip.StartsWith("97.")
            )
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    // is mimetype allowed?
    public int IsFileExtensionsAllowed(string mime)
    {
        List<string> mimetypes = new List<string>();

        if (SiteConfiguration.GetConfig().AllowedFileExtensions.Count == 0)
        { return -1; }

        else
        {
            //populate list
            foreach (SiteConfigurationDomain mimetype in SiteConfiguration.GetConfig().AllowedFileExtensions)
                mimetypes.Add(mimetype.Name);

            //serach list
            return mimetypes.IndexOf(mime);

        }
    }
    public static string ListFileExtensionsAllowed()
    {

        List<string> filetypes = new List<string>();


        if (SiteConfiguration.GetConfig().AllowedFileExtensions.Count == 0)
            return "";
        else
        {

            foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AllowedFileExtensions)
                filetypes.Add(string.Format("{0}", domain.Name));

            return string.Join(", ", filetypes.ToArray());

        }
    }


    public int IsFileExtensionsAllowedForPaidUser(string mime)
    {
        List<string> mimetypes = new List<string>();

        if (SiteConfiguration.GetConfig().AllowedFileExtensionsForPaidUser.Count == 0)
        { return -1; }

        else
        {
            //populate list
            foreach (SiteConfigurationDomain mimetype in SiteConfiguration.GetConfig().AllowedFileExtensionsForPaidUser)
                mimetypes.Add(mimetype.Name);

            //serach list
            return mimetypes.IndexOf(mime);

        }
    }
    public static string ListFileExtensionsAllowedForPaidUser(string un)
    {
        Functions f = new Functions();

        if (f.isUserPaid(un) == true)
        {
            List<string> filetypes = new List<string>();


            if (SiteConfiguration.GetConfig().AllowedFileExtensionsForPaidUser.Count == 0)
                return "";
            else
            {

                foreach (SiteConfigurationDomain domain in SiteConfiguration.GetConfig().AllowedFileExtensionsForPaidUser)
                    filetypes.Add(string.Format(", {0}", domain.Name));

                return string.Join("", filetypes.ToArray());

            }
        }
        else
        {
            return string.Empty;
        }
    }
    public static bool IsFileImage(string input)
    {
        if (
             Path.GetExtension(input) == ".jpg" ||
             Path.GetExtension(input) == ".gif" ||
             Path.GetExtension(input) == ".png" ||
             Path.GetExtension(input) == ".pjpeg" ||
             Path.GetExtension(input) == ".bmp" ||
            Path.GetExtension(input) == ".jpeg"
            )
            return true;
        else
        {
            return false;
        }
    }
    public DataSet ListPaidUsers()
    {
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("ListPaidUsers", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                return ds;

            }
        }

    }
    public void MakeUserPaid(string un)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("make_user_paid", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.ExecuteNonQuery();
            }
        }
    }
    public void MakeUserUnPaid(string un)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("make_user_unpaid", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;
                cmd.ExecuteNonQuery();
            }
        }
    }
    public bool isUserPaid(string un)
    {
        using (SqlConnection conn = new SqlConnection())
        {
            conn.ConnectionString = connString;
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("isuserpaid", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = un;

                SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                success.Direction = ParameterDirection.ReturnValue;
                cmd.Parameters.Add(success);


                cmd.ExecuteReader();

                bool successResult = Convert.ToBoolean(success.Value);
                return successResult;
            }
        }
    }
}