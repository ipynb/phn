﻿using System;
using System.Web;
using System.Collections;
using System.Text;
using System.Web.SessionState;

public class Admob 
{
  
  public string ShowAd(bool isTest, string publisherId)
	{ 
    Hashtable admobRequiredParams = new Hashtable();
    Hashtable admobOptionalParams = new Hashtable();
    admobRequiredParams.Add("PUBLISHER_ID", publisherId);
    admobRequiredParams.Add("ANALYTICS_ID", "");
    admobRequiredParams.Add("COOKIE_DOMAIN", "");
    admobRequiredParams.Add("AD_REQUEST", true);
    admobRequiredParams.Add("ANALYTICS_REQUEST", false);
    admobRequiredParams.Add("TEST_MODE", isTest);
    return AdmobRequest(admobRequiredParams, admobOptionalParams).ToString();
    }

  //string sess = System.Web.HttpContext.Current.Session.SessionID;

  private string AdmobRequest(Hashtable requiredParams, Hashtable optionalParams)
  {
    if (requiredParams == null) return "";
    
    bool admobPixelSent = false;
    bool adMode = false;
    if (requiredParams.ContainsKey("PUBLISHER_ID") && requiredParams.ContainsKey("AD_REQUEST"))
      adMode = (bool)requiredParams["AD_REQUEST"];

    bool analyticsMode = false;
    if (requiredParams.ContainsKey("ANALYTICS_ID") && requiredParams.ContainsKey("ANALYTICS_REQUEST") && !admobPixelSent)
      analyticsMode = (bool)requiredParams["ANALYTICS_REQUEST"];

    String rt = adMode ? (analyticsMode ? "2" : "0") : (analyticsMode ? "1" : "-1");
    if (rt == "-1") return "";

    String protocol = HttpContext.Current.Request.IsSecureConnection ? "https" : "http";
    String domain = requiredParams.ContainsKey("COOKIE_DOMAIN") ? (string)requiredParams["COOKIE_DOMAIN"] : "";
    String z = (DateTime.UtcNow - new DateTime(1970, 1, 1)).TotalSeconds.ToString();
    String o = AdmobGetCookieValue(domain);
    StringBuilder admobContents = new StringBuilder();
    AdmobAppendParams(admobContents, "rt", rt);
    AdmobAppendParams(admobContents, "z", z);
    AdmobAppendParams(admobContents, "u", HttpContext.Current.Request.UserAgent);
    AdmobAppendParams(admobContents, "i", HttpContext.Current.Request.UserHostAddress);
    AdmobAppendParams(admobContents, "p", HttpContext.Current.Request.Url.ToString());
    AdmobAppendParams(admobContents, "t", AdmobMd5(HttpContext.Current.Session.SessionID));
    AdmobAppendParams(admobContents, "v", "20081105-CSHARP-daf93db369888d5f");
    AdmobAppendParams(admobContents, "o", o);
    if (adMode) AdmobAppendParams(admobContents, "s", (string)requiredParams["PUBLISHER_ID"]);
    if (analyticsMode) AdmobAppendParams(admobContents, "a", (string)requiredParams["ANALYTICS_ID"]);
    
    if (optionalParams != null)
      foreach (string optkey in optionalParams.Keys)
        AdmobAppendParams(admobContents, optkey, (string)optionalParams[optkey]);

    ArrayList ignoreHeaders = ArrayList.Adapter(new String[] { "PRAGMA", "CACHE-CONTROL", "CONNECTION", "USER-AGENT", "COOKIE" });
    foreach (string name in HttpContext.Current.Request.Headers)
      if (!ignoreHeaders.Contains(name.ToUpper()))
        AdmobAppendParams(admobContents, "h[" + name + "]", string.Join(",", HttpContext.Current.Request.Headers.GetValues(name)));

    if (requiredParams.ContainsKey("TEST_MODE") && (bool)requiredParams["TEST_MODE"]) admobContents.Append("&m=test");
    
    StringBuilder admobReturn = new StringBuilder();
    const int ADMOB_TIMEOUT = 1000; // 1 second timeout
    System.Net.HttpWebResponse admobWebResponse = null;
    System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
    try
    {
      byte[] postBytes = System.Text.Encoding.UTF8.GetBytes(admobContents.ToString());
      System.Net.HttpWebRequest admobWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create("http://r.admob.com/ad_source.php");
      admobWebRequest.Method = "POST";
      admobWebRequest.ContentType = "application/x-www-form-urlencoded";
      admobWebRequest.ContentLength = postBytes.Length;
      admobWebRequest.Timeout = ADMOB_TIMEOUT;
      admobWebRequest.ServicePoint.Expect100Continue = false;
      sw = System.Diagnostics.Stopwatch.StartNew();
      System.IO.Stream admobWriter = admobWebRequest.GetRequestStream();
      admobWriter.Write(postBytes, 0, postBytes.Length);
      admobWriter.Close();
      admobWebResponse = (System.Net.HttpWebResponse)admobWebRequest.GetResponse();
      sw.Stop();
      System.IO.StreamReader admobReader = new System.IO.StreamReader(admobWebResponse.GetResponseStream());
      admobReturn.Append(admobReader.ReadToEnd().Trim());
      admobReader.Close();
    }
    catch (Exception e) { }
    finally { if (admobWebResponse != null) admobWebResponse.Close(); }
    sw.Stop();
    
    if (!admobPixelSent)
    {
      admobPixelSent = true;
      admobReturn.Append("<img src=\"" + protocol + "://p.admob.com/e0?")
        .Append("rt=").Append(rt)
        .Append("&amp;z=").Append(z)
        .Append("&amp;a=").Append(analyticsMode ? requiredParams["ANALYTICS_ID"] : "")
        .Append("&amp;s=").Append(adMode ? requiredParams["PUBLISHER_ID"] : "")
        .Append("&amp;o=").Append(o)
        .Append("&amp;lt=").Append(sw.Elapsed.TotalSeconds.ToString())
        .Append("&amp;to=").Append(ADMOB_TIMEOUT / 1000.0)
        .Append("\" alt=\"\" width=\"1\" height=\"1\"/>");
    }
    
    return admobReturn.ToString();
  }
  
  private string AdmobGetCookieValue(String domain)
  { 
    if (HttpContext.Current.Request.Cookies["admobuu"] != null)
      return HttpContext.Current.Request.Cookies["admobuu"].Value;

    HttpContext.Current.Response.Cookies["admobuu"].Value = System.Guid.NewGuid().ToString().Replace("-", "");
    HttpContext.Current.Response.Cookies["admobuu"].Expires = DateTime.Now.AddYears(20);
    HttpContext.Current.Response.Cookies["admobuu"].Path = "/";
    if (domain.Length > 0)
    {
      if (domain[0] != '.') domain = "." + domain;
      HttpContext.Current.Response.Cookies["admobuu"].Domain = domain;
    }
    return HttpContext.Current.Request.Cookies["admobuu"].Value; // the new cookie is immediately available in Request.Cookies
  }
  
  private void AdmobAppendParams(StringBuilder contents, String key, String val)
  {
    if (!string.IsNullOrEmpty(val))
    {
      if (contents.Length > 0) contents.Append("&");
      contents.Append(HttpUtility.UrlEncode(key, Encoding.UTF8)).Append("=").Append(HttpUtility.UrlEncode(val, Encoding.UTF8));
    }
  }

  private string AdmobMd5(string val)
  {
    System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
    System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
    byte[] hashBytes = md5.ComputeHash(encoding.GetBytes(val));
    StringBuilder md5Val = new StringBuilder();
    foreach (byte b in hashBytes)
    {
      md5Val.Append(String.Format("{0:x2}", b));
    }
    return md5Val.ToString();
  }

}
