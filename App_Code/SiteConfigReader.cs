﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;

// for domain list 
public class SiteConfigurationDomain : ConfigurationElement
{
   [ConfigurationProperty("name", IsRequired=true)]
   public string Name
   {
      get
      {
         return this["name"] as string;
      }
   }
}
// for domain list 
public class SiteConfigurationDomainCollection : ConfigurationElementCollection
{
    public SiteConfigurationDomain this[int index]
    {
        get
        {
            return base.BaseGet(index) as SiteConfigurationDomain;
        }
        set
        {
            if (base.BaseGet(index) != null)
            {
                base.BaseRemoveAt(index);
            }
            this.BaseAdd(index, value);
        }
    }

    protected override ConfigurationElement CreateNewElement()
    {
        return new SiteConfigurationDomain();
    }

    protected override object GetElementKey(ConfigurationElement element)
    {
        return ((SiteConfigurationDomain)element).Name;
    }
}
// for scalar configuration values
public class SiteConfiguration : ConfigurationSection
{
    public static SiteConfiguration GetConfig()
    {
        return ConfigurationManager.GetSection("SiteConfigSection/siteConfigVariables") as SiteConfiguration;
    }


    [ConfigurationProperty("emailServer", DefaultValue = "localhost", IsRequired = false)]
    public string emailServer
    {
        get
        {
            return this["emailServer"] as string;
        }
    }

    [ConfigurationProperty("emailServerDefaultFromMail", DefaultValue = "admin@localhost", IsRequired = false)]
    public string emailServerDefaultFromMail
    {
        get
        {
            return this["emailServerDefaultFromMail"] as string;
        }
    }

    [ConfigurationProperty("siteName", DefaultValue = "", IsRequired = false)]
    public string siteName
    {
        get
        {
            return this["siteName"] as string;
        }
    }

    [ConfigurationProperty("defaultSiteDomain", DefaultValue = "", IsRequired = false)]
    public string defaultSiteDomain
    {
        get
        {
            return this["defaultSiteDomain"] as string;
        }
    }

    [ConfigurationProperty("uploadedfileSizeLimitInBytes", DefaultValue = "", IsRequired = false)]
    public string uploadedfileSizeLimitInBytes
    {
        get
        {
            return this["uploadedfileSizeLimitInBytes"] as string;
        }
    }

    [ConfigurationProperty("uploadedfileSizeLimitPaidUserInBytes", DefaultValue = "", IsRequired = false)]
    public string uploadedfileSizeLimitPaidUserInBytes
    {
        get
        {
            return this["uploadedfileSizeLimitPaidUserInBytes"] as string;
        }
    }


    [ConfigurationProperty("emailServerPort", DefaultValue = "25", IsRequired = false)]
    public string emailServerPort
    {
        get
        {
            return this["emailServerPort"] as string;
        }
    }

    [ConfigurationProperty("domains")]
    public SiteConfigurationDomainCollection AvailableDomains
    {
        get
        {
            return this["domains"] as SiteConfigurationDomainCollection;
        }
    }

    [ConfigurationProperty("AllowedFileExtensions")]
    public SiteConfigurationDomainCollection AllowedFileExtensions
    {
        get
        {
            return this["AllowedFileExtensions"] as SiteConfigurationDomainCollection;
        }
    }

    [ConfigurationProperty("AllowedFileExtensionsForPaidUser")]
    public SiteConfigurationDomainCollection AllowedFileExtensionsForPaidUser
    {
        get
        {
            return this["AllowedFileExtensionsForPaidUser"] as SiteConfigurationDomainCollection;
        }
    }

    [ConfigurationProperty("subdomainLimitPerUser", DefaultValue = "10", IsRequired = false)]
    public string subdomainLimitPerUser
    {
        get
        {
            return this["subdomainLimitPerUser"] as string;
        }
    }

    [ConfigurationProperty("pageLimitPerSubdomain", DefaultValue = "50", IsRequired = false)]
    public string pageLimitPerSubdomain
    {
        get
        {
            return this["pageLimitPerSubdomain"] as string;
        }
    }

    [ConfigurationProperty("codeSnippetLimit", DefaultValue = "20", IsRequired = false)]
    public string codeSnippetLimit
    {
        get
        {
            return this["codeSnippetLimit"] as string;
        }
    }

    [ConfigurationProperty("pollLimitPerSite", DefaultValue = "10", IsRequired = false)]
    public string pollLimitPerSite
    {
        get
        {
            return this["pollLimitPerSite"] as string;
        }
    }

    [ConfigurationProperty("uploadedTotalfileSizeLimitInBytes", DefaultValue = "", IsRequired = false)]
    public string uploadedTotalfileSizeLimitInBytes
    {
        get
        {
            return this["uploadedTotalfileSizeLimitInBytes"] as string;
        }
    }



    [ConfigurationProperty("uploadedTotalfileSizeLimitPaidUserInBytes", DefaultValue = "", IsRequired = false)]
    public string uploadedTotalfileSizeLimitPaidUserInBytes
    {
        get
        {
            return this["uploadedTotalfileSizeLimitPaidUserInBytes"] as string;
        }
    }


    [ConfigurationProperty("advertiserLimit", DefaultValue = "20", IsRequired = false)]
    public string advertiserLimit
    {
        get
        {
            return this["advertiserLimit"] as string;
        }
    }

    [ConfigurationProperty("RssFeedLimitPerPage", DefaultValue = "1", IsRequired = false)]
    public string RssFeedLimitPerPage
    {
        get
        {
            return this["RssFeedLimitPerPage"] as string;
        }
    }

    [ConfigurationProperty("OnlyOneItemPerPage", DefaultValue = "1", IsRequired = false)]
    public string OnlyOneItemPerPage
    {
        get
        {
            return this["OnlyOneItemPerPage"] as string;
        }
    }

    [ConfigurationProperty("SiteMapLimitPerPage", DefaultValue = "1", IsRequired = false)]
    public string SiteMapLimitPerPage
    {
        get
        {
            return this["SiteMapLimitPerPage"] as string;
        }
    }

    [ConfigurationProperty("linkRotatorLimit", DefaultValue = "10", IsRequired = false)]
    public string linkRotatorLimit
    {
        get
        {
            return this["linkRotatorLimit"] as string;
        }
    }

    [ConfigurationProperty("TotalItemsLimitPerPage", DefaultValue = "1000", IsRequired = false)]
    public string TotalItemsLimitPerPage
    {
        get
        {
            return this["TotalItemsLimitPerPage"] as string;
        }
    }

    [ConfigurationProperty("DefaultItemLimitPerPage", DefaultValue = "100", IsRequired = false)]
    public string DefaultItemLimitPerPage
    {
        get
        {
            return this["DefaultItemLimitPerPage"] as string;
        }
    }

    [ConfigurationProperty("TextInputLimitPerPage", DefaultValue = "100", IsRequired = false)]
    public string TextInputLimitPerPage
    {
        get
        {
            return this["TextInputLimitPerPage"] as string;
        }
    }

    
    [ConfigurationProperty("AdvertiserLimitPerPage", DefaultValue = "2", IsRequired = false)]
    public string AdvertiserLimitPerPage
    {
        get
        {
            return this["AdvertiserLimitPerPage"] as string;
        }
    }
}
