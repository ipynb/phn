﻿using System;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

/// <summary>
/// Summary description for ResizeImage
/// </summary>
public class ResizeImage
{

   public static byte[] Resize(Image img, int maxWidth, int maxHeight)
    {
        ImageFormat format = img.RawFormat;

        int newWidth = img.Width;
        int newHeight = img.Height;

        double aspectRatio = (double)img.Width / (double)img.Height;

        if (aspectRatio <= 1 && img.Width > maxWidth)
        {
            newWidth = maxWidth;
            newHeight = (int)Math.Round(newWidth / aspectRatio);
        }
        else if (aspectRatio > 1 && img.Height > maxHeight)
        {
            newHeight = maxHeight;
            newWidth = (int)Math.Round(newHeight * aspectRatio);
        }


        Bitmap b = new Bitmap(newWidth, newHeight);
        Graphics g = Graphics.FromImage((Image)b);


        g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBilinear;
        g.DrawImage(img, 0, 0, newWidth, newHeight);
        g.Dispose();
        return ConvertImageToByteArray((Image)b, format);
    }
    private static byte[] ConvertImageToByteArray(Image imageToConvert, ImageFormat formatOfImage)
    {
        byte[] Ret;

        try
        {

            using (MemoryStream ms = new MemoryStream())
            {
                imageToConvert.Save(ms, formatOfImage);
                Ret = ms.ToArray();
            }
        }
        catch (Exception) { throw; }

        return Ret;
    }
}
