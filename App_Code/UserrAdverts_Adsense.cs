﻿/**********************************************************************
 * AdSenseHelper Class
 * Version: 1.0
 * Release date: 19/09/2007
 * Copyright (c) 2007 by Alberto Falossi
 * 
 * Web & Blog: http://www.albertofalossi.com
 * 
 * This component is 100% free to use, also in commercial applications.
 * This software is provided "AS IS," without a warranty of any kind.
 ***********************************************************************************/

using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Web;

public class AdSense
{

    public string ShowAd(string publisherId, string channel)
    {
        Dictionary<string, string> parameters = new Dictionary<string, string>();
        parameters["channel"] = channel;
        parameters["client"] = publisherId;
        parameters["format"] = "mobile_single";
        parameters["ad_type"] = "text_image";
        //parameters["color_border"] = "000000";
        //parameters["color_bg"] = "000000";
        //parameters["color_link"] = "FFFFFF";
        //parameters["color_text"] = "CCCCCC";
        //parameters["color_url"] = "999999";
        parameters["markup"] = "xhtml";
        parameters["output"] = "xhtml";

        return GetUrl(BuildAdSenseQueryUrl(parameters));

    }
    public static string GetAdMarkup(Dictionary<string, string> parameters)
    {
        // build the URL
        string url = BuildAdSenseQueryUrl(parameters);

        // get and return the response markup
        return GetUrl(url);
    }
    public static string BuildAdSenseQueryUrl(Dictionary<string, string> parameters)
    {
        HttpRequest request = HttpContext.Current.Request;

        const string URL_ADSERVER = "http://pagead2.googlesyndication.com/pagead/ads?&";

        // prepare some parameters
        long googleDt = (long)DateTime.Now.Subtract(new DateTime(1970, 1, 1)).TotalMilliseconds;
        string googleScheme = request.IsSecureConnection ? "https://" : "http://";
        string googleHost = googleScheme + request.ServerVariables["HTTP_HOST"];

        StringBuilder url = new StringBuilder(URL_ADSERVER);

        // set "automatic" parameters
        AddDefaultParameter(parameters, "dt", googleDt.ToString());
        AddDefaultParameter(parameters, "host", HttpUtility.UrlEncode(googleHost));
        AddDefaultParameter(parameters, "ip", request.UserHostAddress);
        AddDefaultParameter(parameters, "ref", request.UrlReferrer == null ? "" : HttpUtility.UrlEncode(request.UrlReferrer.ToString()));
        AddDefaultParameter(parameters, "url", HttpUtility.UrlEncode(request.RawUrl));
        AddDefaultParameter(parameters, "useragent", HttpUtility.UrlEncode(request.UserAgent));

        // add the parameters to the url
        foreach (System.Collections.Generic.KeyValuePair<string, string> pair in parameters)
        {
            string parameterName = pair.Key;
            string parameterValue = pair.Value;

            // encode the color values
            if (parameterName.Contains("color"))
                parameterValue = GetGoogleColor(parameterValue, googleDt);

            // add the parameter
            url.AppendFormat("{0}={1}&", parameterName, parameterValue);
        }

        return url.ToString();
    }
    private static void AddDefaultParameter(Dictionary<string, string> parameters, string parameterName, string parameterDefaultValue)
    {
        // set the value only if the key is missing
        if (!parameters.ContainsKey(parameterName))
            parameters[parameterName] = parameterDefaultValue;
    }
    private static string GetGoogleColor(string value, long random)
    {
        string[] colorArray = value.Split(',');
        return colorArray[(int)(random % colorArray.Length)];
    }
    private static string GetUrl(string url)
    {
        Stream sream = null;
        StreamReader reader = null;

        try
        {
            // build the request
            System.Net.WebRequest request = WebRequest.Create(url);
            request.Timeout = 1000;
            // get the response stream
            sream = request.GetResponse().GetResponseStream();

            // read the stream and copy it to a string; return the string
            reader = new StreamReader(sream);
            return reader.ReadToEnd();
        }
        catch
        {
            // generic error, return an empty string
            return "";
        }
        finally
        {
            if (reader != null)
                reader.Close();
            if (sream != null)
                sream.Close();
        }
    }
}
