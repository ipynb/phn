﻿using System;
using System.IO;
using System.Net;
using System.Web;

/// <summary>
/// Summary description for Adverts_Mkhoj
/// </summary>
public class Mkhoj
{
    public string ShowAd(bool istest, string publisherId)
    {

        try
        {
            HttpContext context = HttpContext.Current;
            string mkhojPlc = "page";
            bool mkhojTest = istest;
            string mkhojSessionId = "";
            string mkhojMkIds = "";
            string mkhojVersion = "el-COPE-CTATD-20090320";
            string mkhojHs = context.Request.ServerVariables["HTTP_USER_AGENT"].ToString();
            if (String.IsNullOrEmpty(mkhojHs) == false)
            {
                mkhojHs = context.Server.UrlEncode(mkhojHs);
            }
            string refer = context.Request.ServerVariables["HTTP_REFERER"];
            if (String.IsNullOrEmpty(refer) == false)
            {
                refer = context.Server.UrlEncode(refer);
            }
            string curl = context.Request.ServerVariables["URL"];
            if (String.IsNullOrEmpty(curl) == false)
            {
                curl = context.Server.UrlEncode(curl);
            }
            string acc = context.Request.ServerVariables["HTTP_ACCEPT"];
            if (String.IsNullOrEmpty(acc) == false)
            {
                acc = context.Server.UrlEncode(acc);
            }


            string mkhojCarrier = context.Request.ServerVariables["REMOTE_ADDR"].ToString();
            string mkhojSiteId = publisherId;
            string mkhojPostdata = "mk-siteid=" + mkhojSiteId;
            mkhojPostdata += "&mk-carrier=" + mkhojCarrier;
            mkhojPostdata += "&mk-version=" + context.Server.UrlEncode(mkhojVersion);
            mkhojPostdata += "&mk-sessionid=" + mkhojSessionId;
            mkhojPostdata += "&h-user-agent=" + mkhojHs;
            mkhojPostdata += "&h-referer=" + refer;
            mkhojPostdata += "&h-page-url=" + curl;
            mkhojPostdata += "&h-accept=" + acc;
            mkhojPostdata += "&mk-placement=" + context.Server.UrlEncode(mkhojPlc);
            mkhojPostdata += "&mk-mkids=" + mkhojMkIds;

            foreach (string mkhojKey in context.Request.ServerVariables)
            {
                int mkhojPosition = -1;
                if (-1 != (mkhojPosition = mkhojKey.IndexOf("HTTP_X_")))
                    mkhojPostdata += "&x-"
                                     + mkhojKey.Substring(mkhojPosition + 7).ToLower()
                                     + "=" + context.Request.ServerVariables[mkhojKey];
            }
            string mkhojUrl = "";
            string mkhojTest_url = "http://w.sandbox.mkhoj.com/showad.asm";
            string mkhojLive_url = "http://w.mkhoj.com/showad.asm";
            mkhojUrl = mkhojTest ? mkhojTest_url : mkhojLive_url;
            try
            {
                WebRequest mkhojReq = WebRequest.Create(mkhojUrl);
                mkhojReq.Timeout = 2000;
                mkhojReq.ContentType = "application/x-www-form-urlencoded";
                mkhojReq.Headers.Set("X-MKHOJ-SITEID", mkhojSiteId);
                mkhojReq.Method = "POST";

                byte[] mkhojBytes = System.Text.Encoding.ASCII.GetBytes(mkhojPostdata);

                mkhojReq.ContentLength = mkhojBytes.Length;
                Stream os = mkhojReq.GetRequestStream();
                os.Write(mkhojBytes, 0, mkhojBytes.Length);
                os.Close();
                WebResponse mkhojResp = mkhojReq.GetResponse();
                StreamReader mkhojRead = new StreamReader(mkhojResp.GetResponseStream());

                if ("" == mkhojMkIds)
                {
                    mkhojMkIds = mkhojResp.Headers.Get("X-MKHOJ-ID");
                }
                else
                {
                    mkhojMkIds = mkhojMkIds + "," + mkhojResp.Headers.Get("X-MKHOJ-ID");
                }

                string mkhojData = mkhojRead.ReadToEnd();
                if (null != mkhojData && "" != mkhojData)
                {
                    return mkhojData;
                }
                else
                {
                    return null;
                }
            }
            catch
            {
                return null;
            }
            ///////////////////////////////////////////////////////////////////////
            //THIS 'IF' CONFIRMS THERE WASNT ANY AD FROM MKHOJ. USE THIS BLOCK FOR
            //BACK-FILLING THIS PLACE OR ENTER SOME OTHER COMPATIBLE WAP HREF TAG
            //HERE TO SHOW TEXT/URL. E.G. <a href="http://www.mkhoj.com">mKhoj</a>
            ///////////////////////////////////////////////////////////////////////
            //if (null != mkhojResp.Headers["x-mkhoj-noad"])
            //{
            //    return null;
            //}
        }
        catch
        {
            return null;
        }

    }
}