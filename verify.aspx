﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/verify.aspx.cs" Inherits="verify" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="Form1" runat="server">
<asp:Panel ID="formpanel" runat="server">
<div class="error">If you have recently registered use this page to verify your account. If you haven't registered yet <a href="manage/register.aspx">click here to register</a></div>

<div class="title">Verify your account</div>
<div class="center">
E-mail address:<br/>
<asp:TextBox ID="emailinput" runat="server"></asp:TextBox>
<br/>
Verification code:<br/>
<asp:TextBox ID="verificationCodeInput" runat="server"></asp:TextBox>
<br/>
You are accepting our <a href="terms.htm">terms and conditions</a> by verifiying your account. Make sure you read our terms!
<br/>
<asp:Button ID="sbmt" runat="server" Text="Verify" onclick="sbmt_Click" />
</div>
</asp:Panel>
<asp:Panel ID="result" runat="server">
<div class="center">
    <asp:Label ID="reslab" runat="server" Text=""></asp:Label></div>
</asp:Panel>
</form>
</asp:Content>

