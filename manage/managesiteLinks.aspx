﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/managesiteLinks.aspx.cs" Inherits="manage_managesiteLinks" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<asp:Panel ID="LoggedInDiv" runat="server">
<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> Link rotator
</div>

<%
    string job = Request.QueryString["job"];
        if (job=="add")
        {
             Functions f = new Functions();
                    int id = Functions.ReturnNumeric(Request["subdomainid"]);
                    int linkid = Functions.ReturnNumeric(Request["id"]);
                    string subdomain = f.GetSubdomainNameById(id);

                    string title = Functions.LimitString(Request["txtTitle"], 100);
                    title = Server.HtmlEncode(title);
                    title = f.SqlProtect(title);
                    string url = Functions.LimitString(Request["txtUrl"], 255);
                    url = Server.HtmlEncode(url);
                    url = f.SqlProtect(url);
            if (
            f.AddALink(Functions.un, subdomain, title, url) == true)
            {%>
            <div class="center">Link has been added<br/>
            <a href="managesiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
            </div>
            <%}
            else
            {%><div class="error">Unable to add the link<br/>
                        <a href="managesiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
            </div><%
            }
                    
                   
        }
        else if (job=="delete")
        {
            Functions f = new Functions();
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            int linkid = Functions.ReturnNumeric(Request["id"]);
            string subdomain = f.GetSubdomainNameById(id);
            if (
            f.DeleteALink(Functions.un, subdomain, linkid) == true)
            {%>
            <div class="center">Link has been deleted<br/>
            <a href="managesiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
            </div>
            <%}
            else
            {%><div class="error">Unable to delete the link<br/>
                        <a href="managesiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
</div><%
            }
        }
        else{ %>

<asp:Repeater ID="Repeater" runat="server">
<HeaderTemplate>
<ul>
</HeaderTemplate>
<ItemTemplate>
<li>
<b>Title:</b> <%#DataBinder.Eval(Container.DataItem, "title")%> <a onclick="return confirm('DELETE this link?')" href="managesiteLinks.aspx?job=delete&amp;id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img
    src="../images/delete.png" alt="del"/></a>
    <br/>
    <b>Url:</b> <%#DataBinder.Eval(Container.DataItem, "url")%>
    </li>
</ItemTemplate>
<FooterTemplate> 
 </ul>
</FooterTemplate>

</asp:Repeater>

<ul>
<li>
<div class="title">Add new</div>
<form action="managesiteLinks.aspx?job=add&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>" method="post">
<p>
    Title: Max 100 chars<br/>
    <input name="txtTitle" type="text" />
    <br/>
    
    Url: Max 255 chars<br/>
    <input name="txtUrl" value="http://" type="text" />
    <br/>
    <input id="Submit1" type="submit" value="Add" />
    </p>
  </form> 
 </li>
<li>
Link rotator is a small advertise engine for your site. 
You can add up to <%=SiteConfiguration.GetConfig().linkRotatorLimit%> links per site. 
Any link you add will be randomly displayed if you use link rotator element on your pages
</li></ul><%
        } %>





</asp:Panel>



    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

</asp:Content>

