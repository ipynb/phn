﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/managePages.aspx.cs" Inherits="manage_managePages" Title="Untitled Page" %>
<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="form1" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> Edit pages
</div>

<ul><li>
<asp:HiddenField ID="subdomainName" runat="server" />
<asp:ObjectDataSource ID="FolderDataSource" runat="server" SelectMethod="ListFolders" TypeName="Functions">
<SelectParameters>
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>


<asp:DropDownList ID="FolderDropDownList" runat="server" DataTextField="folderName2" DataValueField="folderName" 
        DataSourceID="FolderDataSource" AppendDataBoundItems="true">
        <asp:ListItem Value="" Text="All folders"></asp:ListItem>
    </asp:DropDownList> <asp:Button ID="Button1" runat="server" Text="Browse" 
        onclick="Button1_Click" />
        <br />
       Folders for page management purpose only and irrelevant for published site.
</li>

<li>    <a href="manageSiteAddPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img src="/images/newpage.gif" alt=""/> Add new page</a></li>
</ul>


<asp:ListView ID="pageList" runat="server" OnItemDataBound="subDomainPageListEventHandler" onpagepropertieschanging="pageList_PagePropertiesChanging">
    <LayoutTemplate>
    <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </ul>
    </LayoutTemplate>
    <ItemTemplate>
    <li>
      <img src="../images/edit.gif" alt="edit" /> <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;pageid=<%#Eval("id")%>&amp;subdomainid=<%#Eval("subdomainid")%>"><%#Eval("title")%> - page<%#Eval("id")%>.aspx</a>
      <asp:Label ID="pointIndexPageLabel" runat="server" Text=""></asp:Label> <a class="faint" href="manageSiteDeletePage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;id=<%#Eval("id")%>&amp;subdomain=<%#Eval("subdomain")%>&amp;subdomainid=<%#Eval("subdomainid")%>" onclick="return confirm('Delete permenantly?')"><img src="../images/delete.png" alt="delete" /> </a> (<%#Eval("hits")%> hits) 
      <br />
      <a class="faint" href="manageSiteEditPageHeaders.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;id=<%#Eval("id")%>&amp;subdomainid=<%#Eval("subdomainid")%>">Headers &amp; Colours</a>
      <br />
      <asp:Panel CssClass="inlineDiv" ID="setindexpagediv" runat="server">
      <a class="faint" onclick="return confirm('Are you sure?')" href="manageSiteSetIndexPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;id=<%#Eval("id")%>&amp;subdomain=<%#Eval("subdomain")%>&amp;subdomainid=<%#Eval("subdomainid")%>">Set as index</a>
     <br />
    </asp:Panel>  
    </li>
    </ItemTemplate>
    <EmptyDataTemplate>
    <b>Folder is empty! Click "Add new page" link to create one</b>
    </EmptyDataTemplate>
</asp:ListView>

<ul>
<li>
<div class="center"> 
 <asp:DataPager ID="pageListPaging" runat="server" PagedControlID="pageList" PageSize="20">                      
        <Fields>

            
           <asp:NumericPagerField ButtonType="Link" ButtonCount="10" NextPageText="Next" 
                PreviousPageText="Previous" RenderNonBreakingSpacesBetweenControls="true" />
            
           
             
        </Fields>
    </asp:DataPager>
    <br />
    Page numbers are auto generated, you do not have control over them.
    </div>
</li>
</ul>

<div class="error">Important!<br/>We use client side caching (10 minutes per page) in order to provide fast and reliable service. This means although changes you make to your site(s) are applied instantly, you may not see changes on your side until empty your browsers cache or caching timeouts. Keep in mind that caching done on client side so it doesn't affect your site globally.</div>
</asp:Panel>
<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

