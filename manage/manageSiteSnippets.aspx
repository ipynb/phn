﻿<%@ Page Title="" validateRequest="false" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<%   Functions f = new Functions();
     if (f.LoginCheck() == true)
     {
         int id = Functions.ReturnNumeric(Request["subdomainid"]);
         string subdomain = f.GetSubdomainNameById(id);
         fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);%>
<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> Code snippets
</div>
<%
// add job
if (Request.QueryString["job"] == "add")
    {
        string snipName=Request["snippetName"];
        //snipName = f.ReplaceLineBreakForTitles(snipName);
        snipName = Server.HtmlEncode(snipName);
        snipName = f.SqlProtect(snipName);
        snipName= Functions.LimitString(snipName,50);
        if (String.IsNullOrEmpty(snipName) == true)
        {
            snipName = "My new snippet";
        }
        string snippetCode = Request["snippetCode"];
        snippetCode = Server.HtmlEncode(snippetCode);
        snippetCode = f.SqlProtect(snippetCode);
        snippetCode = Functions.LimitString(snippetCode, 1000);
        //snippetCode = f.CleanHtml(snippetCode);
        
        
            if (f.AddCodeSnippet(Functions.un, subdomain, snipName, snippetCode, Convert.ToInt32(SiteConfiguration.GetConfig().codeSnippetLimit)) == true)
            {
                %>
                    <div class="center">
                    Code snippet has been added
                    </div>
                <%
            }
            else
            {
                    %><div class="error">
                    Can't add the code snippet. Try deleting unused snippets
                    </div><%
                
            }
        
       }
         
// delete job      
if (Request.QueryString["job"] == "delete")
{
    int snipid = Functions.ReturnNumeric(Request["id"]);

    if (f.DeleteAsnip(Functions.un, subdomain, snipid) == true)
    {
                %><div class="center">
                    Code snippet has been deleted<br/>
                    <a href="manageSiteSnippets.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=id%>">Ok</a>
                    </div><%
                              }
    else
    {
            
                    %> <div class="error">
                    Can't delete the code snippet
                    </div><%
                              }
}
    // else list
else
{
    Response.Write(f.getCodeSnippet(subdomain, Functions.un));
    
    %>
<ul>
<li>
<div class="title">Add new</div>
<form action="manageSiteSnippets.aspx?job=add&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=id%>" method="post">
<p>
    Snippet's friendly name:
    <br/>
    Max 50 chars<br/>
    <input name="snippetName" type="text" />
    <br/>
    Snippet's code:
    <br/>
    Html only. Max 1000 chars<br/>
    <textarea name="snippetCode" class="fullscreen" rows="5"></textarea>
    <br/>
    <input id="Submit1" type="submit" value="Add" />
    </p>
  </form> 
</li>
<li>
Code snippets are small pieces of code that you use often. 
You can create up to <%=SiteConfiguration.GetConfig().codeSnippetLimit%> code snippets and insert them to your pages like other elements
</li></ul>
        


<%}}else{%>
<div class="error">You need to be logged in in order to use this page!</div>
<%}%>


</asp:Content>
