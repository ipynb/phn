﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteEditPageMoveItem.aspx.cs" Inherits="manage_manageSiteEditPageMoveItem" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<asp:Panel ID="loggedinDiv" runat="server">
<div class="title">
    <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Edit Page<%=Request.QueryString["pageid"] %>.aspx</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Move item
    </div>
    
    
    
    
 <asp:Panel class="center" Visible="false" ID="ResultPanel" runat="server">
     <asp:Label ID="ResultLabel" runat="server" Text=""></asp:Label>
     <br />
     <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Ok</a>
</asp:Panel>    
    
    
    
</asp:Panel>



<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

</asp:Content>

