﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"
    CodeFile="~/codeBehindFiles/register.aspx.cs" Inherits="manage_register" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="body" runat="Server">
    <form id="logon" runat="server">
    <div>
    
        <div class="center">
        <asp:Label ID="ErrorLabel" runat="server" Text="" CssClass="error"></asp:Label>
        </div>
        
        <asp:Panel ID="registerBox" runat="server">
        <%  //get values from posted form to put back in inputs
            string un = Request["un"];
            string pw = Request["pw"];
            string pw2 = Request["pw2"];
            string email = Request["email"];
            string captchaText = Request["captcha"];
            %>
            <ul>
            <li>
            Username:<br />
            <input name="un" type="text" value="<%=un %>"/>
            <br/>
            a-z,0-9. Max 50 chars
            </li>
            <li>
            Password:<br />
            <input name="pw" type="text" value="<%=pw %>"/>
                        <br/>
            a-z,0-9. Max 50 chars
            </li>
            <li>
            Repeat password:<br />
            <input name="pw2" type="text" value="<%=pw2 %>"/>
            </li>
            <li>
            Email:<br />
            <input name="email" type="text" value="<%=email %>"/>
            <br/>Verification code will be sent to your e-mail account
            </li>
            <li>
            Security image:<br />
            <a href="/captchaImage.aspx/dummy.jpg"><img alt="Security image" src="/captchaImage.aspx/dummy.jpg" /></a>
            </li>
            <li>
            Type security image:<br />
            <input name="captcha" type="text" value="<%=captchaText %>" />
            </li>
            </ul>
            <asp:Button ID="logonButton" runat="server" Text="Register" OnClick="regbuttonClick" />
            
            
            
            </asp:Panel>
      </div>
     </form>
     
    <div class="center">
    <asp:Panel ID="registerBoxResult" runat="server">
        You have registered! Please check your e-mail for verification code. Once you
        have verified your e-mail you will be able to start creating your mobile web site.<br />
        <a href="/default.aspx">Ok</a>
    </asp:Panel>
    <asp:Panel ID="emailSenderError" runat="server">
        There was a problem when connecting to mail server. We couldn't send your verification email. Please contact us at <%=SiteConfiguration.GetConfig().emailServerDefaultFromMail%> in order to solve this issue.<br />
        <a href="/default.aspx">Ok</a>
    </asp:Panel>
    </div>
</asp:Content>
