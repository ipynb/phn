﻿<%@ Page Language="C#" validateRequest="false" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteEditPageEditItem.aspx.cs" Inherits="manage_manageSiteEditPageEditItem" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="form1" runat="server">
<div><asp:HiddenField ID="subdomainName" runat="server" /></div>
<asp:ObjectDataSource ID="FontColorDataSource" runat="server" 
        SelectMethod="colourList" TypeName="myPageBuilder"></asp:ObjectDataSource>
        
<asp:ObjectDataSource ID="CssExtractDataSource" runat="server" SelectMethod="CssExtract" TypeName="myPageBuilder">
            <SelectParameters>
                <asp:ControlParameter ControlID="subdomainName" Name="subdomain" 
                    PropertyName="Value" Type="String" />
            </SelectParameters>
</asp:ObjectDataSource>
        
        
<asp:Panel ID="loggedinDiv" runat="server">
<div class="title">
<%string p = Request.QueryString["pageid"];  %>
<a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=p%>">Edit Page<%=p%>.aspx</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Edit item
</div>

<div>
    <asp:HiddenField ID="tagTypeText" runat="server" />
</div>





    <asp:Panel ID="textEditPanel" Visible="false" runat="server">
    <asp:TextBox ID="textEditTextBox" runat="server" TextMode="MultiLine"></asp:TextBox>
    <br />
    <asp:Button ID="updateButton1"
        runat="server" Text="Update" />
    </asp:Panel>



<!-- Tag stylers start -->

<asp:Panel ID="styleOptionsLabel" runat="server">
    <div class="title">Style with css</div>
    <div class="gap"></div>
</asp:Panel>

<asp:Panel ID="Panel_tagStyleClass" runat="server">
    <ul><li>Css class: <asp:DropDownList 
                                        ID="extractedCss" runat="server" 
                                        DataSourceID="CssExtractDataSource" 
                                        DataTextField="Cssclass" 
                                        DataValueField="Cssclass"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Text="Use current setting" Value="-"></asp:ListItem>
                                        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
                                        </asp:DropDownList>  <a href="manageSiteStyleSheet.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>" class="faint">Edit</a>
</li></ul>
</asp:Panel>   

<asp:Panel ID="manualStyleOptionsLabel" runat="server">
    <div class="title">Style manually</div>
    <div class="gap"></div>
</asp:Panel>

<asp:Panel ID="Panel_tagAlignmentList" runat="server">
<ul><li>Alignment: <asp:DropDownList ID="tagAlignmentList" runat="server">
<asp:ListItem Value="-" Text="Use current setting"></asp:ListItem>
<asp:ListItem Value="" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="center" Text="center"></asp:ListItem>
<asp:ListItem Value="right" Text="right"></asp:ListItem>
     </asp:DropDownList>
 </asp:Panel>
     
 <asp:Panel ID="Panel_tagFontSize" runat="server">
<ul><li>Font size: <asp:DropDownList ID="fontSizeList" runat="server">
<asp:ListItem Value="-" Text="Use current setting"></asp:ListItem>
<asp:ListItem Text="Not configured" Value=""></asp:ListItem>
<asp:ListItem Value="xx-small" Text="Xx-small"></asp:ListItem>
<asp:ListItem Value="x-small" Text="X-small"></asp:ListItem>
<asp:ListItem Value="small" Text="Small"></asp:ListItem>
<asp:ListItem Value="medium" Text="Medium"></asp:ListItem>
<asp:ListItem Value="large" Text="Large"></asp:ListItem>
<asp:ListItem Value="x-large" Text="X-large"></asp:ListItem>
<asp:ListItem Value="xx-large" Text="Xx-large"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

 <asp:Panel ID="Panel_tagFontColor" runat="server">
<ul><li>Font color: <asp:DropDownList ID="FontColourList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="FontColourList_setStyle">
        <asp:ListItem Text="Use current setting" Value="-"></asp:ListItem>
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>   

 <asp:Panel ID="Panel_tagFontDecoration" runat="server">
<ul><li>Font decoration: <asp:DropDownList ID="fontDecorationList" runat="server">
<asp:ListItem Value="-" Text="Use current setting"></asp:ListItem>
<asp:ListItem Text="Not configured" Value=""></asp:ListItem>
<asp:ListItem Value="underline" Text="Underline"></asp:ListItem>
<asp:ListItem Value="blink" Text="Blink"></asp:ListItem>
<asp:ListItem Value="overline" Text="Overline"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
        </asp:Panel>  
 
 <asp:Panel ID="Panel_tagFontWeight" runat="server">
<ul><li>Font weight: <asp:DropDownList ID="fontWeightList" runat="server">
<asp:ListItem Value="-" Text="Use current setting"></asp:ListItem>
<asp:ListItem Text="Not configured" Value=""></asp:ListItem>
<asp:ListItem Value="bold" Text="Bold"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<asp:Panel ID="Panel_tagBgColor" runat="server">
<ul><li>Background color: <asp:DropDownList ID="tagBgColorList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="tagBgColorList_setStyle">
        <asp:ListItem Text="Use current setting" Value="-"></asp:ListItem>
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<asp:Panel ID="Panel_tagBorderSize" runat="server">
<ul><li>Border size: <asp:DropDownList ID="tagBorderSizeList" runat="server">
<asp:ListItem Value="-" Text="Use current setting"></asp:ListItem>
<asp:ListItem Text="Not configured" Value=""></asp:ListItem>
<asp:ListItem Value="0" Text="0 px"></asp:ListItem>
<asp:ListItem Value="1" Text="1 px"></asp:ListItem>
<asp:ListItem Value="2" Text="2 px"></asp:ListItem>
<asp:ListItem Value="3" Text="3 px"></asp:ListItem>
<asp:ListItem Value="4" Text="4 px"></asp:ListItem>
<asp:ListItem Value="5" Text="5 px"></asp:ListItem>
</asp:DropDownList>
</li></ul></asp:Panel>

<asp:Panel ID="Panel_tagBorderColor" runat="server">
<ul><li>Border color: <asp:DropDownList ID="tagBorderColourList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="tagBorderColourList_setStyle">
        <asp:ListItem Text="Use current setting" Value="-"></asp:ListItem>
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<!-- Tag stylers end -->


<asp:Button ID="updateButton2" runat="server" Text="Update"/>



 
 <asp:Panel class="center" Visible="false" ID="UpdateButtonResultPanel" runat="server">
     <asp:Label ID="UpdateButtonResultLabel" runat="server" Text=""></asp:Label>
     <br />
     <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Ok</a>
</asp:Panel>


</asp:Panel>

<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>


