﻿<%@ Page validateRequest="false" Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteChangestatus.aspx.cs" Inherits="manage_manageSiteStyleSheet" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="form1" runat="server">
    
    <asp:Panel CssClass="center" ID="loggedinDiv" runat="server">
        <asp:Label ID="changeResult" runat="server" Text=""></asp:Label>
    <br/>
    <a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Ok</a>
    </asp:Panel>
   



    <asp:Panel Visible="false" ID="notLoggedInDiv" CssClass="center" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
    
    </form>

</asp:Content>
