﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteDeleteSite.aspx.cs" Inherits="manage_manageSiteDeleteSite" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="form1" runat="server">
    
    <asp:Panel ID="loggedinDiv" CssClass="center" runat="server">
        <asp:Label ID="changeResult" runat="server" Text=""></asp:Label>
    <br/>
    <a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;view=deletemode">Ok</a>
    </asp:Panel>
   



    <asp:Panel Visible="false" ID="notLoggedInDiv" CssClass="center" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
    
    </form>

</asp:Content>
