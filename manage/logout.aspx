﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"%>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {

            loginResult.Text = "Logout successful!";
            f.Logout();

           
        }
        else
        {
            loginResult.CssClass = "error";
            loginResult.Text = "Check your username and password!";
        }

    }
 </script>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">


    <div class="center">
    <asp:Label ID="loginResult" runat="server" Text=""></asp:Label>
    <br/>
        <a href="/">Continue</a>
    </div>
    
    
    
</asp:Content>
