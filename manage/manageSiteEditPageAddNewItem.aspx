<%@ Page Title="" validateRequest="false" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="manageSiteEditPageAddNewItem.aspx.cs" Inherits="manage_manageSiteEditPageAddNewItem"%>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<form id="addtagsform" runat="server">

<asp:ObjectDataSource ID="FontColorDataSource" runat="server" 
        SelectMethod="colourList" TypeName="myPageBuilder"></asp:ObjectDataSource>

<asp:ObjectDataSource ID="ListPagesDataSource" runat="server" SelectMethod="ListPages" TypeName="myPageBuilder">
<SelectParameters>
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="CodeSnippetDataSource" runat="server" 
    SelectMethod="ListCodeSnippets" TypeName="Functions">
<SelectParameters>
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="RssFeedDataSource" runat="server" 
    SelectMethod="ListRSSLinks" TypeName="Functions">
<SelectParameters>
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>


                
 <asp:ObjectDataSource ID="FileDataSource" runat="server" 
    SelectMethod="ListUploadedFiles" TypeName="Functions">
<SelectParameters>
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>
            
        
<asp:ObjectDataSource ID="CssExtractDataSource" runat="server" SelectMethod="CssExtract" TypeName="myPageBuilder">
            <SelectParameters>
                <asp:ControlParameter ControlID="subdomainName" Name="subdomain" 
                    PropertyName="Value" Type="String" />
            </SelectParameters>
        </asp:ObjectDataSource>
        
 
 <asp:ObjectDataSource ID="ListAdvertsDataSource" runat="server" SelectMethod="ListAdvertsOnAddNewItemPage" TypeName="myPageBuilder">
<SelectParameters>
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>

   
   
       <asp:ObjectDataSource ID="listPollsDatasource" runat="server" 
        SelectMethod="ListPolls" TypeName="Functions">
        <SelectParameters>
            <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
            <asp:ControlParameter ControlID="subdomainName" Name="subdomain" 
                PropertyName="Value" Type="String" />
        </SelectParameters>
    </asp:ObjectDataSource>
    
    
<asp:Panel ID="loggedinDiv" runat="server">
    <div class="title">
    <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Edit Page<%=Request.QueryString["pageid"] %>.aspx</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Add new item <a href="designerHelp.aspx?subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="../images/help.png" alt=""/></a>
    </div>
    
    <asp:Panel ID="tagListPanel" runat="server">
    <ul>
    <li>
    <asp:DropDownList ID="tagList" runat="server">
    <asp:ListItem Value="">Choose item to add</asp:ListItem>
    <asp:ListItem Value="bText">Block Text &lt;div&gt;</asp:ListItem>
    <asp:ListItem Value="DIVo">Div open &lt;div&gt;</asp:ListItem>
    <asp:ListItem Value="DIVc">- Div close &lt;/div&gt;</asp:ListItem>
    <asp:ListItem Value="HtmlCode">Html Code</asp:ListItem>
    <asp:ListItem Value="HR">Horizontal Line &lt;hr/&gt;</asp:ListItem>
    <asp:ListItem Value="Image">Image &lt;img&gt;</asp:ListItem>
    <asp:ListItem Value="ImageLink">Image Link &lt;a&gt;</asp:ListItem>
    <asp:ListItem Value="Text">Inline Text &lt;span&gt;</asp:ListItem>
    <asp:ListItem Value="BR">Line Break &lt;br/&gt;</asp:ListItem>
    <asp:ListItem Value="ULo">List open &lt;ul&gt;</asp:ListItem>
    <asp:ListItem Value="LIo">- List bullet open &lt;li&gt;</asp:ListItem>
    <asp:ListItem Value="LIc">- List bullet close &lt;/li&gt;</asp:ListItem>
    <asp:ListItem Value="ULc">- List close &lt;/ul&gt;</asp:ListItem>
    <asp:ListItem Value="ParagraphO">Paragraph open &lt;p&gt;</asp:ListItem>
    <asp:ListItem Value="ParagraphC">- Paragraph close &lt;/p&gt;</asp:ListItem>
    <asp:ListItem Value="TextLink">Text Link &lt;a&gt;</asp:ListItem>
    <asp:ListItem Value="Advert">PHN: Advert</asp:ListItem>
    <asp:ListItem Value="CodeSnippet">PHN: Code Snippet</asp:ListItem>
    <asp:ListItem Value="Chat">PHN: Chat</asp:ListItem>
    <asp:ListItem Value="ContactAdminForm">PHN: Contact Admin</asp:ListItem>
    <asp:ListItem Value="File">PHN: File</asp:ListItem>
    <asp:ListItem Value="Guestbook">PHN: Guestbook</asp:ListItem>
    <asp:ListItem Value="LinkRotator">PHN: Link Rotator</asp:ListItem>
    <asp:ListItem Value="iTextLink">PHN: Link</asp:ListItem>
    <asp:ListItem Value="OnlineCount">PHN: Online Count</asp:ListItem>
    <asp:ListItem Value="OnlineList">PHN: Online List</asp:ListItem>
    <asp:ListItem Value="Poll">PHN: Poll</asp:ListItem>
    <asp:ListItem Value="RssFeed">PHN: Rss feed</asp:ListItem>
    <asp:ListItem Value="ServerDate">PHN: Server Date</asp:ListItem>
    <asp:ListItem Value="ServerTime">PHN: Server Time</asp:ListItem>
    <asp:ListItem Value="SiteMap">PHN: Sitemap</asp:ListItem>
    <asp:ListItem Value="TotalHits">PHN: Total hits</asp:ListItem>
    <asp:ListItem Value="VisitorIP">PHN: Visitor's IP</asp:ListItem>
    <asp:ListItem Value="VisitorCountry">PHN: Visitor's Country</asp:ListItem>

    </asp:DropDownList> <asp:Button ID="addTagButton" runat="server" Text="Go" 
            onclick="addTagButton_Click" />
  </li>
  </ul>  
     </asp:Panel>
 
 
 
<!-- Tags start -->
   
<asp:HiddenField ID="tagType" runat="server" />
<asp:HiddenField ID="subdomainName" runat="server" />

<asp:Panel ID="Panel_ContactAdminForm" CssClass="center" visible="false" runat="server">
<b>Submit button title (i.e Send):</b><br />
<asp:TextBox ID="submitButtonTextBox" runat="server"></asp:TextBox><br />

<b>Message box title (i.e Your message to admin):</b><br /><asp:TextBox ID="messageBoxTextBox" runat="server"></asp:TextBox> <br />

    <b>Return page. The page that user will be redirected after sending the e-mail:</b><br /><asp:DropDownList ID="ReturnPageDropDownList" runat="server" 
        DataSourceID="ListPagesDataSource" DataTextField="title" 
        DataValueField="id"></asp:DropDownList>
        <br />We advise you to create two pages. One for the contact admin form and another one for to tell user that email was sent.
        
</asp:Panel>

<asp:Panel ID="Panel_File" class="center" visible="false" runat="server">
      Please select file to add then click Save or go to <a href="uploadFile.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">File store</a> to upload files<br />
      <asp:DropDownList ID="FileDropDownList" runat="server" 
        DataSourceID="FileDataSource" DataTextField="fileName" 
        DataValueField="id" AppendDataBoundItems="true">
        <asp:ListItem Text="Select a file" Value="0"></asp:ListItem>
        </asp:DropDownList>
              <asp:DropDownList ID="LinkStyleDropDownList" runat="server">
              <asp:ListItem Text="Direct link" Value="href"></asp:ListItem>
              <asp:ListItem Text="Display on page(images)" Value="img"></asp:ListItem>
              </asp:DropDownList>
              <br />
              Title: <asp:TextBox ID="FileLinkTitle" runat="server"></asp:TextBox>
</asp:Panel>

<asp:Panel ID="Panel_OnlineCount" CssClass="center" visible="false" runat="server">
Click Save button to add online visitor count.
</asp:Panel>
<asp:Panel ID="Panel_SiteMap" CssClass="center" visible="false" runat="server">
Click Save button to add sitemap. Sitemaps are easiest way to linkup your pages together, they also help search engines to discover your content.
</asp:Panel>
<asp:Panel ID="Panel_TotalHits" CssClass="center" visible="false" runat="server">
Click Save button to add total hit count. This item shows total hits of all pages in your site.
</asp:Panel>
<asp:Panel ID="Panel_OnlineList" CssClass="center" visible="false" runat="server">
Click Save button to add online visitor list.
</asp:Panel>
<asp:Panel ID="Panel_RssFeed" CssClass="center" visible="false" runat="server">
Please select RSS feed to add then click Save or go to <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Manage RSS feeds</a> to add new feeds<br />
      <asp:DropDownList ID="RssFeedDropDownList" runat="server" 
        DataSourceID="RssFeedDataSource" DataTextField="title" 
        DataValueField="id"></asp:DropDownList><br />
        Get first <asp:DropDownList ID="FeedPerPage" runat="server">
        <asp:ListItem Text="1" Value="1"></asp:ListItem>
        <asp:ListItem Text="5" Value="5"></asp:ListItem>
        <asp:ListItem Text="10" Value="10"></asp:ListItem>
        <asp:ListItem Text="15" Value="15"></asp:ListItem>
        <asp:ListItem Text="20" Value="20"></asp:ListItem>
        </asp:DropDownList> items
</asp:Panel>

<asp:Panel ID="Panel_Chat" CssClass="center" visible="false" runat="server">
Click Save to add chat. Each page with chat is another chat room. If you have 10 chat rooms create 10 pages and add chat to each page. 
</asp:Panel>
<asp:Panel ID="Panel_Poll" CssClass="center" visible="false" runat="server">
Please select Poll to add then click save or go to <a href="managePoll.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Manage polls</a> to add new poll<br />
<asp:DropDownList ID="pollDropDownList" runat="server" 
        DataSourceID="listPollsDatasource" DataTextField="pollTitle" 
        DataValueField="id"></asp:DropDownList>

</asp:Panel>
<asp:Panel ID="Panel_Guestbook" CssClass="center" visible="false" runat="server">
Click Save button to add guestbook. We advise you to create a page for guestbook only
</asp:Panel>

<asp:Panel ID="Panel_BR" CssClass="center" visible="false" runat="server">
Click Save button to add line break
</asp:Panel>

<asp:Panel ID="Panel_HR" CssClass="center" visible="false" runat="server">
Click Save button to add horizontal line
</asp:Panel>

<asp:Panel ID="Panel_ServerTime" CssClass="center" visible="false" runat="server">
Select time zone<br />
<asp:DropDownList ID="ddlTimeZone" runat="server">
  <asp:ListItem value="-12">(GMT-12:00) International dateline, west</asp:ListItem>
  <asp:ListItem value="-11">(GMT-11:00) Midway Islands, Samoan Islands</asp:ListItem>
  <asp:ListItem value="-10">(GMT-10:00) Hawaii</asp:ListItem>
  <asp:ListItem value="-9">(GMT-09:00) Alaska</asp:ListItem>
  <asp:ListItem value="-8">(GMT-08:00) Pacific Time (USA og Canada); Tijuana</asp:ListItem>
  <asp:ListItem value="-7">(GMT-07:00) Mountain Time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-6">(GMT-06:00) Central time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-5">(GMT-05:00) Eastern time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-4">(GMT-04:00) Atlantic Time (Canada)</asp:ListItem>
  <asp:ListItem value="-3.5">(GMT-03:30) Newfoundland</asp:ListItem>
  <asp:ListItem value="-3">(GMT-03:00) Brasilia</asp:ListItem>
  <asp:ListItem value="-2">(GMT-02:00) Mid-Atlantic</asp:ListItem>
  <asp:ListItem value="-1">(GMT-01:00) Azorerne</asp:ListItem>
  <asp:ListItem value="0" Selected="true">(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lissabon, London</asp:ListItem>
  <asp:ListItem value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien</asp:ListItem>
  <asp:ListItem value="2">(GMT+02:00) Athen, Istanbul, Minsk</asp:ListItem>
  <asp:ListItem value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</asp:ListItem>
  <asp:ListItem value="3.5">(GMT+03:30) Teheran</asp:ListItem>
  <asp:ListItem value="4">(GMT+04:00) Abu Dhabi, Muscat</asp:ListItem>
  <asp:ListItem value="4.5">(GMT+04:30) Kabul</asp:ListItem>
  <asp:ListItem value="5">(GMT+05:00) Islamabad, Karachi, Tasjkent</asp:ListItem>
  <asp:ListItem value="5.5">(GMT+05:30) Kolkata, Chennai, Mumbai, New Delhi</asp:ListItem>
  <asp:ListItem value="5.75">(GMT+05:45) Katmandu</asp:ListItem>
  <asp:ListItem value="6">(GMT+06:00) Astana, Dhaka</asp:ListItem>
  <asp:ListItem value="6.5">(GMT+06:30) Rangoon</asp:ListItem>
  <asp:ListItem value="7">(GMT+07:00) Bangkok, Hanoi, Djakarta</asp:ListItem>
  <asp:ListItem value="8">(GMT+08:00) Beijing, Chongjin, SAR Hongkong, �r�mqi</asp:ListItem>
  <asp:ListItem value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</asp:ListItem>
  <asp:ListItem value="9.5">(GMT+09:30) Adelaide</asp:ListItem>
  <asp:ListItem value="10">(GMT+10:00) Canberra, Melbourne, Sydney</asp:ListItem>
  <asp:ListItem value="11">(GMT+11:00) Magadan, Solomon Islands, New Caledonien</asp:ListItem>
  <asp:ListItem value="12">(GMT+12:00) Fiji, Kamtjatka, Marshall Islands</asp:ListItem>
  <asp:ListItem value="13">(GMT+13:00) Nuku'alofa</asp:ListItem>
</asp:DropDownList>
</asp:Panel>

<asp:Panel ID="Panel_ServerDate" CssClass="center" visible="false" runat="server">
Select time zone<br />
<asp:DropDownList ID="ddlTimeZone2" runat="server">
  <asp:ListItem value="-12">(GMT-12:00) International dateline, west</asp:ListItem>
  <asp:ListItem value="-11">(GMT-11:00) Midway Islands, Samoan Islands</asp:ListItem>
  <asp:ListItem value="-10">(GMT-10:00) Hawaii</asp:ListItem>
  <asp:ListItem value="-9">(GMT-09:00) Alaska</asp:ListItem>
  <asp:ListItem value="-8">(GMT-08:00) Pacific Time (USA og Canada); Tijuana</asp:ListItem>
  <asp:ListItem value="-7">(GMT-07:00) Mountain Time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-6">(GMT-06:00) Central time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-5">(GMT-05:00) Eastern time (USA og Canada)</asp:ListItem>
  <asp:ListItem value="-4">(GMT-04:00) Atlantic Time (Canada)</asp:ListItem>
  <asp:ListItem value="-3.5">(GMT-03:30) Newfoundland</asp:ListItem>
  <asp:ListItem value="-3">(GMT-03:00) Brasilia</asp:ListItem>
  <asp:ListItem value="-2">(GMT-02:00) Mid-Atlantic</asp:ListItem>
  <asp:ListItem value="-1">(GMT-01:00) Azorerne</asp:ListItem>
  <asp:ListItem value="0" Selected="true">(GMT) Greenwich Mean Time: Dublin, Edinburgh, Lissabon, London</asp:ListItem>
  <asp:ListItem value="1">(GMT+01:00) Amsterdam, Berlin, Bern, Rom, Stockholm, Wien</asp:ListItem>
  <asp:ListItem value="2">(GMT+02:00) Athen, Istanbul, Minsk</asp:ListItem>
  <asp:ListItem value="3">(GMT+03:00) Moscow, St. Petersburg, Volgograd</asp:ListItem>
  <asp:ListItem value="3.5">(GMT+03:30) Teheran</asp:ListItem>
  <asp:ListItem value="4">(GMT+04:00) Abu Dhabi, Muscat</asp:ListItem>
  <asp:ListItem value="4.5">(GMT+04:30) Kabul</asp:ListItem>
  <asp:ListItem value="5">(GMT+05:00) Islamabad, Karachi, Tasjkent</asp:ListItem>
  <asp:ListItem value="5.5">(GMT+05:30) Kolkata, Chennai, Mumbai, New Delhi</asp:ListItem>
  <asp:ListItem value="5.75">(GMT+05:45) Katmandu</asp:ListItem>
  <asp:ListItem value="6">(GMT+06:00) Astana, Dhaka</asp:ListItem>
  <asp:ListItem value="6.5">(GMT+06:30) Rangoon</asp:ListItem>
  <asp:ListItem value="7">(GMT+07:00) Bangkok, Hanoi, Djakarta</asp:ListItem>
  <asp:ListItem value="8">(GMT+08:00) Beijing, Chongjin, SAR Hongkong, �r�mqi</asp:ListItem>
  <asp:ListItem value="9">(GMT+09:00) Osaka, Sapporo, Tokyo</asp:ListItem>
  <asp:ListItem value="9.5">(GMT+09:30) Adelaide</asp:ListItem>
  <asp:ListItem value="10">(GMT+10:00) Canberra, Melbourne, Sydney</asp:ListItem>
  <asp:ListItem value="11">(GMT+11:00) Magadan, Solomon Islands, New Caledonien</asp:ListItem>
  <asp:ListItem value="12">(GMT+12:00) Fiji, Kamtjatka, Marshall Islands</asp:ListItem>
  <asp:ListItem value="13">(GMT+13:00) Nuku'alofa</asp:ListItem>
</asp:DropDownList>
</asp:Panel>

<asp:Panel ID="Panel_DIVo" CssClass="center" visible="false" runat="server">
Click Save button to add Div open item.<br />
Keep in mind that you must have structure below for proper div<br />
&lt;div&gt; (Div open)<br />
&lt;/div&gt; (Div close)<br />
</asp:Panel>

<asp:Panel ID="Panel_DIVc" CssClass="center" visible="false" runat="server">
Click Save button to add Div close item
</asp:Panel>

<asp:Panel ID="Panel_ParagraphO" CssClass="center" visible="false" runat="server">
Click Save button to add Paragraph open item.<br />
Keep in mind that you must have structure below for proper paragrap<br />
&lt;p&gt; (Paragraph open)<br />
&lt;/p&gt; (Paragraph close)<br />
</asp:Panel>

<asp:Panel ID="Panel_ParagraphC" CssClass="center" visible="false" runat="server">
Click Save button to add Paragraph close item
</asp:Panel>

<asp:Panel ID="Panel_ULo" CssClass="center" visible="false" runat="server">
Click Save button to add List open item.<br />
Keep in mind that you must have structure below for proper list<br />
&lt;ul&gt; (List open)<br />
&lt;li&gt; (List bullet open)<br />
Your text,link or image<br />
&lt;/li&gt; (List bullet close)<br />
&lt;li&gt; (List bullet open)<br />
Another text,link or image<br />
&lt;/li&gt; (List bullet close)<br />
&lt;/ul&gt; (List close)<br />
</asp:Panel>

<asp:Panel ID="Panel_ULc" CssClass="center" visible="false" runat="server">
Click Save button to add List close item
</asp:Panel>

<asp:Panel ID="Panel_LIo" CssClass="center" visible="false" runat="server">
Click Save button to add List bullet open item
</asp:Panel>

<asp:Panel ID="Panel_LIc" CssClass="center" visible="false" runat="server">
Click Save button to add List bullet close item
</asp:Panel>

<asp:Panel ID="Panel_VisitorIP" CssClass="center" visible="false" runat="server">
Click Save button to add visitor's IP
</asp:Panel>

<asp:Panel ID="Panel_VisitorCountry" CssClass="center" visible="false" runat="server">
Click Save button to add visitor's country
</asp:Panel>

<asp:Panel ID="Panel_Text" visible="false" runat="server">
<b>Enter your text:</b><br/>
    <asp:TextBox TextMode="MultiLine" CssClass="fullscreen" Rows="5" ID="TextText" runat="server"></asp:TextBox>
    <br />Max 500 chars<br />
</asp:Panel>

<asp:Panel ID="Panel_bText" visible="false" runat="server">
<b>Enter your text:</b><br/>
    <asp:TextBox TextMode="MultiLine" CssClass="fullscreen" Rows="5" ID="bTextText" runat="server"></asp:TextBox>
        <br />Max 500 chars<br />
</asp:Panel>


<asp:Panel ID="Panel_iTextLink" visible="false" runat="server">
    <b>Link title:</b> <asp:TextBox ID="iTextLinkTitle" runat="server"></asp:TextBox> Max 50 chars<br />
    <b>Internal link:</b> <asp:DropDownList ID="iTextLinkUrl" runat="server" 
        DataSourceID="ListPagesDataSource" DataTextField="title" 
        DataValueField="id"></asp:DropDownList>
        <br />To add extarnal link, select <b>Text link</b> from <b>Chose item to add</b> menu<br />
</asp:Panel>


<asp:Panel ID="Panel_TextLink" visible="false" runat="server">
  <b>Link title:</b> <asp:TextBox ID="TextLinkTitle" runat="server"></asp:TextBox> Max 50 chars<br/>
  <b>Target url:</b> <asp:TextBox ID="TextLinkUrl" runat="server" Text="http://"></asp:TextBox> Max 255 chars
  <br />To add internal link (link to pages created in here), select <b>PHN: Link</b> from <b>Chose item to add</b> menu<br />
</asp:Panel>


<asp:Panel ID="Panel_Image" visible="false" runat="server">
  <b>Image url:</b> <asp:TextBox ID="ImageImageUrl" runat="server" Text="http://"></asp:TextBox> Max 255 chars<br/>
  <a href="flickrSearch.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>&amp;prevPositiontagid=<%=Request.QueryString["prevPositiontagid"] %>">Use images from Flickr</a>
</asp:Panel>

<asp:Panel ID="Panel_ImageLink" visible="false" runat="server">
  <b>Image url:</b> <asp:TextBox ID="ImageLinkImageUrl" runat="server" Text="http://"></asp:TextBox> Max 255 chars<br/>
  <b>Target url:</b> <asp:TextBox ID="ImageLinkTargetUrl" runat="server" Text="http://"></asp:TextBox> Max 255 chars<br/>
    <a href="flickrSearch.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>&amp;prevPositiontagid=<%=Request.QueryString["prevPositiontagid"] %>">Use images from Flickr</a>

 </asp:Panel>

<asp:Panel ID="Panel_HtmlCode" visible="false" runat="server">
<script type="text/javascript">
//<![CDATA[


function addTagIn (code)
{

    var inputtoupdate = document.forms[0].<%=this.HtmlCodeText.ClientID.ToString() %>;
   
        if (code=="b")
	    {
        inputtoupdate.value= inputtoupdate.value + ' <b>Text here</b>';
	    }
	    else if (code=="i")
	    {
        inputtoupdate.value= inputtoupdate.value + ' <i>Text here</i>';
	    }
	    else if (code=="big")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' <big>Text here</big>';
	    }
	    else if (code=="href")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' <a href="http://LINK HERE">TITLE HERE</a>';
	    }
	    else if (code=="small")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' <small>Text here</small>';
	    }
	    else if (code=="br")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' \n\r<br/>';
	    }
	    else if (code=="hr")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' \n\r<hr/>';
	    }
	    else if (code=="p")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' \n\r<p>Text here</p>';
	    }
	    else if (code=="div")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' \n\r<div>Text here</div>';
	    }
	    else if (code=="span")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' \n\r<span>Text here</span>';
	    }
	    else if (code=="img")
	    {
	    inputtoupdate.value= inputtoupdate.value + ' <img src="Image link here" alt=""/>';
	    }
	    
}



//]]>
</script>
    <b>Enter html code:</b><br/>
    <a href="#" onclick="addTagIn('p')">p</a>
    <a href="#" onclick="addTagIn('div')">div</a>
    <a href="#" onclick="addTagIn('span')">span</a>
    <a href="#" onclick="addTagIn('br')">br</a>
    <a href="#" onclick="addTagIn('hr')">hr</a>
    <a href="#" onclick="addTagIn('href')">link</a> 
    <a href="#" onclick="addTagIn('img')">image</a> 
    <a href="#" onclick="addTagIn('b')"><b>b</b></a>
    <a href="#" onclick="addTagIn('i')"><i>i</i></a>
    <a href="#" onclick="addTagIn('big')"><big>big</big></a> 
    <a href="#" onclick="addTagIn('small')"><small>small</small></a> 
    <br />
    <asp:TextBox TextMode="MultiLine" CssClass="fullscreen" Rows="5" ID="HtmlCodeText" runat="server"></asp:TextBox>
    <br />Max 1000 chars<br />
    
    <div class="gap"></div>
     <b>Or import from file: </b><br/>
     <asp:Panel ID="UploadForm" runat="server">
            <input type="file" id="File1" name="File1" runat="server">
            <asp:button runat="server" id="CmdUpload" Text="Import" onClick="UploadFile" />
            <br />Only html or text files up to 1000 chars
            </asp:Panel>
           
</asp:Panel>

<asp:Panel ID="Panel_CodeSnippet" class="center" visible="false" runat="server">
      <asp:DropDownList ID="CodeSnippetDropDownList" runat="server" 
        DataSourceID="CodeSnippetDataSource" DataTextField="snippetName" 
        DataValueField="id"></asp:DropDownList><br />
              If you can't see anything in the list, make sure you have some data in your <a href="manageSiteSnippets.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">code snippet</a> tool
</asp:Panel>

<asp:Panel ID="Panel_LinkRotator" class="center" visible="false" runat="server">
       Make sure you have some links in your <a href="manageSiteLinks.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Link rotator</a> then click Save
</asp:Panel>

<asp:Panel ID="Panel_Advert" class="center" visible="false" runat="server">
Please select advert to add then click Save. If there are no adverts, use <a href="manageSiteAdverts.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Manage adverts</a> to add some<br />
        <asp:DropDownList ID="ListAdvertsDropDownList" runat="server" 
        DataSourceID="ListAdvertsDataSource" DataTextField="advertCompany" 
        DataValueField="id" AppendDataBoundItems="true">
        <asp:ListItem Selected="True" Text="Not configured" Value="0"></asp:ListItem>
        </asp:DropDownList>
</asp:Panel>

    <asp:PlaceHolder ID="AutoBrCheckBoxPlaceHolder" runat="server" Visible="false">
    <asp:CheckBox ID="AutoBrCheckBox" runat="server" Checked="true"/> Add new line after
    </asp:PlaceHolder>
    
<asp:Panel ID="SaveTagButtonPanel1" class="center" visible="false" runat="server">
 <asp:Button ID="SaveTagButton1" runat="server" Text="Save" onclick="SaveTagButton_Click" />
</asp:Panel>  
 <!-- Tags end -->   
 
 
<!-- Tag stylers start -->

<asp:Panel ID="styleOptionsLabel" visible="false" runat="server">
    <div class="title">Style with css</div>
    <div class="gap"></div>
</asp:Panel>

<asp:Panel ID="Panel_tagStyleClass" visible="false" runat="server">
    <ul><li>Css class: <asp:DropDownList 
                                        ID="extractedCss" runat="server" 
                                        DataSourceID="CssExtractDataSource" 
                                        DataTextField="Cssclass" 
                                        DataValueField="Cssclass"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
                                        </asp:DropDownList>  <a href="manageSiteStyleSheet.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>" class="faint">Edit</a>
</li></ul>
</asp:Panel>   

<asp:Panel ID="manualStyleOptionsLabel" visible="false" runat="server">
    <div class="title">Style manually</div>
    <div class="gap"></div>
</asp:Panel>

 <asp:Panel ID="Panel_tagAlignmentList" visible="false" runat="server">
<ul><li>Alignment: <asp:DropDownList ID="tagAlignmentList" runat="server">
<asp:ListItem Value="" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="center" Text="center"></asp:ListItem>
<asp:ListItem Value="right" Text="right"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

 <asp:Panel ID="Panel_tagFontSize" visible="false" runat="server">
<ul><li>Font size: <asp:DropDownList ID="fontSizeList" runat="server">
<asp:ListItem Value="" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="xx-small" Text="Xx-small"></asp:ListItem>
<asp:ListItem Value="x-small" Text="X-small"></asp:ListItem>
<asp:ListItem Value="small" Text="Small"></asp:ListItem>
<asp:ListItem Value="medium" Text="Medium"></asp:ListItem>
<asp:ListItem Value="large" Text="Large"></asp:ListItem>
<asp:ListItem Value="x-large" Text="X-large"></asp:ListItem>
<asp:ListItem Value="xx-large" Text="Xx-large"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

 <asp:Panel ID="Panel_tagFontColor" visible="false" runat="server">
<ul><li>Font color: <asp:DropDownList ID="FontColourList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="FontColourList_setStyle">
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>   

 <asp:Panel ID="Panel_tagFontDecoration" visible="false" runat="server">
<ul><li>Font decoration: <asp:DropDownList ID="fontDecorationList" runat="server">
<asp:ListItem Value="none" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="underline" Text="Underline"></asp:ListItem>
<asp:ListItem Value="blink" Text="Blink"></asp:ListItem>
<asp:ListItem Value="overline" Text="Overline"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
        </asp:Panel>  
 
 <asp:Panel ID="Panel_tagFontWeight" visible="false" runat="server">
<ul><li>Font weight: <asp:DropDownList ID="fontWeightList" runat="server">
<asp:ListItem Value="" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="bold" Text="Bold"></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<asp:Panel ID="Panel_tagBgColor" visible="false" runat="server">
<ul><li>Background color: <asp:DropDownList ID="tagBgColorList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="tagBgColorList_setStyle">
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<asp:Panel ID="Panel_tagBorderSize" visible="false" runat="server">
<ul><li>Border size: <asp:DropDownList ID="tagBorderSizeList" runat="server">
<asp:ListItem Value="0" Text="Not configured"></asp:ListItem>
<asp:ListItem Value="0" Text="0 px"></asp:ListItem>
<asp:ListItem Value="1" Text="1 px"></asp:ListItem>
<asp:ListItem Value="2" Text="2 px"></asp:ListItem>
<asp:ListItem Value="3" Text="3 px"></asp:ListItem>
<asp:ListItem Value="4" Text="4 px"></asp:ListItem>
<asp:ListItem Value="5" Text="5 px"></asp:ListItem>
</asp:DropDownList>
</li></ul></asp:Panel>

<asp:Panel ID="Panel_tagBorderColor" visible="false" runat="server">
<ul><li>Border color: <asp:DropDownList ID="tagBorderColourList" runat="server" 
AppendDataBoundItems="true"
        DataSourceID="FontColorDataSource" DataTextField="ColorName" 
        DataValueField="ColorHex" OnPreRender="tagBorderColourList_setStyle">
        <asp:ListItem Text="Not configured" Value=""></asp:ListItem>
     </asp:DropDownList>
</li></ul>
</asp:Panel>

<!-- Tag stylers end -->

<asp:Panel ID="SaveTagButtonPanel2" class="center" visible="false" runat="server">
 <asp:Button ID="SaveTagButton2" runat="server" Text="Save" onclick="SaveTagButton_Click" />
</asp:Panel>  
 
 
 <asp:Panel class="center" Visible="false" ID="SaveButtonResultPanel" runat="server">
     <asp:Label ID="SaveButtonResultLabel" runat="server" Text=""></asp:Label>
     <br />
     <a href="manageSiteEditPage.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>&amp;pageid=<%=Request.QueryString["pageid"] %>">Ok</a>
</asp:Panel>


</asp:Panel>
<asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>
</form>
</asp:Content>

