﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" ValidateRequest="false" AutoEventWireup="true" CodeFile="~/codeBehindFiles/uploadFile.aspx.cs" Inherits="manage_uploadFile" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="upForm" runat="server">
    
<asp:ObjectDataSource ID="ListfilesDataSource" runat="server" 
            SelectMethod="ListUploadedFiles" TypeName="Functions">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
                    </SelectParameters>
                </asp:ObjectDataSource>

<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> 
    Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> My File store</div>

<asp:ListView ID="FileList" runat="server" DataSourceID="ListfilesDataSource" 
        onItemDataBound="FileListEventHandler">
<LayoutTemplate>
    <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder>
    </ul>
    </LayoutTemplate>
    <ItemTemplate>
<li>

<asp:Label ID="imgLabel" runat="server" Text="Label" Visible="false"></asp:Label>

<b><a onclick="return confirm('DELETE this file? It will be deleted from all pages too!')" href="uploadFileDelete.aspx?id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Delete</a></b><br/>

<b>Link:</b> /FileHandler.ashx?id=<%#DataBinder.Eval(Container.DataItem, "id")%><br />
<b>Name:</b> <%#DataBinder.Eval(Container.DataItem, "fileName")%><br />
<b>Type:</b> <%#DataBinder.Eval(Container.DataItem, "fileType")%><br />
<b>Size:</b> <%#DataBinder.Eval(Container.DataItem, "fileSize")%> bytes<br/>
</li>
    
    </ItemTemplate>
    <EmptyDataTemplate>You don&#39;t have any files yet. File store is the place where you 
        upload your content to use in your pages. Use form below to uplaod files. Some 
        phones may not be capable of uploading files, so try with different phone if 
        upload fails. 
    </EmptyDataTemplate>
</asp:ListView>

<div class="center"> 
 <asp:DataPager ID="FileListPaging"  PageSize="10" runat="server" PagedControlID="FileList" 
    QueryStringField="p">                      
        <Fields>
           <asp:NumericPagerField ButtonType="Link" ButtonCount="10" NextPageText="Next" 
                PreviousPageText="Previous" RenderNonBreakingSpacesBetweenControls="true" />
        </Fields>
    </asp:DataPager>
<asp:Panel ID="uploadform" runat="server">      
        <ul>
        <li>

            <asp:FileUpload ID="File1" runat="server" /> <asp:Button ID="upbutton" 
                runat="server" Text="Upload" onclick="upbutton_Click" />
                <br /><asp:CheckBox ID="CheckBoxResize" runat="server" />  Resize (if image)<br />
            Resize to: <asp:TextBox style="-wap-input-format: '*N'" ID="TextBoxEn" Columns="3" runat="server"></asp:TextBox>
            x<asp:TextBox style="-wap-input-format: '*N'" Columns="3" ID="TextBoxBoy" runat="server"></asp:TextBox> <br />
            
                <br />
                Allowed files types are <%= Functions.ListFileExtensionsAllowed() %> <%= Functions.ListFileExtensionsAllowedForPaidUser(Functions.un)%>
                <br />
                <b>File size limit:</b> <asp:Literal ID="fsLimit" runat="server"></asp:Literal>
                <br />
                <b>Account size limit:</b> <asp:Literal ID="asLimit" runat="server"></asp:Literal>
                <br />
                <b>Current size of files:</b> <%Functions f = new Functions(); Response.Write(Convert.ToInt32(f.GetCurrentTotalUploads(Functions.un))/1024); %>KB
                <br />
                Go to <b>Edit page &gt; Add item &gt; PHN: File</b> to add a file to your pages

        </li>
        </ul>                
        </asp:Panel>
</div>
<asp:Panel ID="resultPanel" CssClass="center" visible="false" runat="server">
<asp:Label ID="resultLabel" runat="server" Text=""></asp:Label>
<br/>
<a href="uploadFile.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Ok</a></asp:Panel>



    </asp:Panel>
    
    
    
    
    
    
    
    
    
    
    
        <asp:Panel ID="notLoggedinDiv" visible="false" runat="server">
            <div class="error">You need to be logged in in order to use this page!</div>
        </asp:Panel>
        </form>
</asp:Content>

