﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/managePaidUsers.aspx.cs" Inherits="manage_managePaidUsers" Title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI.WebControls" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="form1" runat="server">
<asp:Panel ID="loggedinDiv" runat="server">
<div class="title"><a href="default.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>"><img src="/images/home.gif" alt=""/> 
    Home</a> <img src="/images/bluearrow.gif" alt="&gt;"/> Manage paid users
</div>
  
 
      <asp:Panel ID="subsPanel" Visible="false" runat="server">
          <asp:Repeater ID="subsRepeater" runat="server">
          <HeaderTemplate><ul></HeaderTemplate>
          <ItemTemplate>
          <li><a href="http://<%#Eval ("subdomain") %>.<%#Eval ("domain") %>">http://<%#Eval ("subdomain") %>.<%#Eval ("domain") %></a> (<%#Eval ("totalhits") %>)</li>
          </ItemTemplate>
          <FooterTemplate><li><a href="managePaidUsers.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Close</a></li></ul></FooterTemplate>
          </asp:Repeater>
        
       </asp:Panel>
        
         
     <asp:Panel ID="res" Visible="false" CssClass="center" runat="server">
       User status has been changed<br />
        <a href="managePaidUsers.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>">Ok</a>
        </asp:Panel>
        
    <asp:ListView ID="PaidUsersListView" runat="server" 
        >
        <LayoutTemplate>
        <ul>
        <asp:PlaceHolder ID="itemPlaceholder" runat="server"></asp:PlaceHolder> 
        </ul>
        </LayoutTemplate>
         <ItemTemplate>
         <li>
         <%#Eval ("un") %> (<a href="managePaidUsers.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;v=<%#Eval ("un") %>"><%#Eval("subdomainCount")%> sites</a>)
        </li>
    </ItemTemplate>
    <EmptyDataTemplate>
    <ul>
    <li>
    There are no paid users
    </li>
    </ul>
    </EmptyDataTemplate>
    </asp:ListView>


<asp:PlaceHolder ID="PlaceHolder1" runat="server">
<div class="title">Add/Remove new paid user</div>

    <asp:Literal ID="Literal1" runat="server">Username:</asp:Literal> 
    <asp:TextBox ID="paidUser" runat="server"></asp:TextBox> 
    <asp:Button ID="Button1" runat="server" Text="Add" onclick="Button1_Click" /> 
    <asp:Button onclick="Button2_Click" OnClientClick="return confirm('Are you sure?')" ID="Button2" runat="server" Text="Remove" />
</asp:PlaceHolder>

    
 </asp:Panel>   
 
    
    
    
    <asp:Panel Visible="false" CssClass="error" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
</form>

</asp:Content>

