﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteRSS.aspx.cs" Inherits="manage_manageSiteRSS" Title="Untitled Page" %>


<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">

<asp:Panel ID="LoggedInDiv" runat="server">
<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> RSS feeds
</div>

<%
    string job = Request.QueryString["job"];
        if (job=="add")
        {
             Functions f = new Functions();

                    int id = Functions.ReturnNumeric(Request["subdomainid"]);
                    int rsssId = Functions.ReturnNumeric(Request["id"]);
                    string subdomain = f.GetSubdomainNameById(id);
                    string title = Functions.LimitString(Request["txtTitle"], 100);
                    title = Server.HtmlEncode(title);
                    title = f.SqlProtect(title);
                    string url = Functions.LimitString(Request["txtUrl"], 255);
                    url = Server.HtmlEncode(url);
                    url = f.SqlProtect(url);

                    if (f.checkUrl(url) == true)
                    {
                        if (
                            f.AddRSSLink(Functions.un, subdomain, title, url) == true)
                        {
                            
                            %>
                                <div class="center">Feed has been added<br/>
                                <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
                                </div>
                                <%
                        
                        }
                        else
                        {
                                 %><div class="error">Unable to add the feed<br/>
                                            <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
                                </div><%
                        }
                    }
                    else
                    {
                        %><div class="error">Unable to add the feed. Are you sure you entered correct url?<br/>
                                            <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
                                </div><%
                    }
                    
                   
        }
        else if (job=="delete")
        {
            Functions f = new Functions();
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            int feedid = Functions.ReturnNumeric(Request["id"]);
            string subdomain = f.GetSubdomainNameById(id);
            if (
            f.DeleteRSSfeed(Functions.un, subdomain, feedid) == true)
            {%>
            <div class="center">Feed has been deleted<br/>
            <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
            </div>
            <%}
            else
            {%><div class="error">Unable to delete the feed<br/>
                        <a href="manageSiteRSS.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>">Ok</a>
</div><%
            }
        }
        else{ %>

<asp:Repeater ID="Repeater" runat="server">
<HeaderTemplate>
<ul>
</HeaderTemplate>
<ItemTemplate>
<li>
<b>Title:</b> <%#DataBinder.Eval(Container.DataItem, "title")%> <a onclick="return confirm('DELETE this feed? It will be deleted from pages (if in any)!')" href="manageSiteRSS.aspx?job=delete&amp;id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img
    src="../images/delete.png" alt="del"/></a>
    <br/>
    <b>Url:</b> <%#DataBinder.Eval(Container.DataItem, "url")%>
    </li>
</ItemTemplate>
<FooterTemplate> 
 </ul>
</FooterTemplate>

</asp:Repeater>

<ul>
<li>
<div class="title">Add new</div>
<form action="manageSiteRSS.aspx?job=add&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>" method="post">
<p>
    Title: Max 100 chars<br/>
    <input name="txtTitle" type="text" />
    <br/>
    
    Url: Max 255 chars<br/>
    <input name="txtUrl" value="http://" type="text" />
    <br/>
    <input id="Submit1" type="submit" value="Add" />
    </p>
  </form> 
 </li>
<li>
RSS feeds are easiest way to put content on your site. 
You can add up to <%=SiteConfiguration.GetConfig().DefaultItemLimitPerPage%> feeds per site
</li></ul><%
        } %>





</asp:Panel>



    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
</asp:Panel>

</asp:Content>

