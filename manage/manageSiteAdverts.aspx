﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/codeBehindFiles/manageSiteAdverts.aspx.cs" Inherits="manage_manageSiteAdverts" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    <form id="form1" runat="server">


<asp:ObjectDataSource ID="ListAdvertsDataSource" runat="server" SelectMethod="ListAdverts" TypeName="myPageBuilder">
<SelectParameters>
<asp:ControlParameter ControlID="subdomainName" Name="subdomain" PropertyName="Value" Type="String" />
    <asp:QueryStringParameter Name="un" QueryStringField="un" Type="String" />
</SelectParameters>
</asp:ObjectDataSource>

    
<asp:Panel ID="loggedinDiv" runat="server">
<asp:HiddenField ID="subdomainName" runat="server" />

<div class="title">
<a href="manageSite.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>"><asp:Label ID="fullSiteNameLabel" runat="server"></asp:Label></a> <img src="/images/bluearrow.gif" alt="&gt;"/> Manage adverts
</div>

<asp:Panel ID="existingAdsPanel" runat="server">

 <asp:Repeater ID="AdvertsRepeater" runat="server" DataSourceID="ListAdvertsDataSource">
<HeaderTemplate>
<ul>
</HeaderTemplate>
<ItemTemplate>
<li>
<b><%#DataBinder.Eval(Container.DataItem, "advertCompany")%>:</b> <%#DataBinder.Eval(Container.DataItem, "publisherId")%>  <a onclick="return confirm('DELETE this advert? It will be deleted from all pages too!')" href="manageSiteAdverts.aspx?job=delete&amp;id=<%# Eval("id") %>&amp;un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"]%>"><img
    src="../images/delete.png" alt="del"/></a>
</li>
</ItemTemplate>
<FooterTemplate> 
 </ul>
</FooterTemplate>
 </asp:Repeater>


<ul>
<li>
<div class="title">Add new</div>
<div class="gap"></div>

<table style="width: 100%">
                     <tr>
                         <td style="width: 110px">
            Select company:</td>
                         <td>
                                  <asp:DropDownList ID="advertCompany" runat="server">
                                    <asp:ListItem Value="Admob">Admob</asp:ListItem>
                                    <asp:ListItem Value="AdmodaBanner">Admoda Banner</asp:ListItem>
                                    <asp:ListItem Value="AdultmodaBanner">Adultmoda Banner</asp:ListItem>
                                    <asp:ListItem Value="AdmodaText">Admoda Text</asp:ListItem>
                                    <asp:ListItem Value="AdultmodaText">Adultmoda Text</asp:ListItem>
                                    <asp:ListItem Value="BuzzcityBanner">Buzzcity Banner</asp:ListItem> 
                                    <asp:ListItem Value="BuzzcityText">Buzzcity Text</asp:ListItem>                                     
                                    <asp:ListItem Value="Decktrade">Decktrade</asp:ListItem>
                                    <asp:ListItem Value="Google">Google</asp:ListItem>
                                    <asp:ListItem Value="Mkhoj">Mkhoj</asp:ListItem>
                                    <asp:ListItem Value="Mojiva">Mojiva</asp:ListItem>
                                    <asp:ListItem Value="ZestADZ">ZestADZ</asp:ListItem>                                 
                                    </asp:DropDownList>
                        </td>
                     </tr>
                                 <tr>
                         <td style="width: 110px; vertical-align: top;">
            Publisher id:</td>
                         <td>
                             <asp:TextBox ID="publisherId" runat="server"></asp:TextBox>
                             </td>
                     </tr>
                     <tr>
                         <td style="width: 110px; vertical-align: top;">
            Channel id:</td>
                         <td>
                             <asp:TextBox ID="channelId" runat="server"></asp:TextBox><br />
     Google only! Leave empty for other advertisers
     </td>
                     </tr>
                     
                     <tr>
                         <td style="width: 110px; vertical-align: top;">
            Zone id:</td>
                         <td>
                             <asp:TextBox ID="zoneId" runat="server"></asp:TextBox><br />
     Mojiva only! Leave empty for other advertisers
     </td>
                     </tr>
                     
                     <tr>
                         <td style="width: 110px; vertical-align: top;">
                             &nbsp;</td>
                         <td>
                         
            <asp:Button ID="Button1" runat="server" Text="Add" onclick="Button1_Click" /></td>
                     </tr>
                 </table>
                                          You can add up to <%=SiteConfiguration.GetConfig().advertiserLimit %> advertisers per site.

                 

    
</li>
</ul>

<div class="title">Advertiser info</div>

<ul>
<li>Admob http://admob.com</li>
<li>Admoda http://admoda.com</li>
<li>Adultmoda http://adultmoda.com</li>
<li>Buzzcity http://buzzcity.com</li>
<li>Decktrade http://decktrade.com</li>
<li>Google http://adsense.google.com</li>
<li>Mkhoj http://mkhoj.com</li>
<li>Mojiva http://mojiva.com</li>
<li>ZestADZ http://zestadz.com</li>
</ul>

</asp:Panel>



<asp:Panel ID="addAdResultPanel" CssClass="center" visible="false" runat="server">
    <asp:Label ID="addAdResultLabel" runat="server" Text=""></asp:Label>
        <br />
        <a href="manageSiteAdverts.aspx?un=<%=Functions.un %>&amp;pw=<%=Functions.pw %>&amp;subdomainid=<%=Request.QueryString["subdomainid"] %>">Ok</a>

</asp:Panel>


</asp:Panel>
   



    <asp:Panel Visible="false" ID="notLoggedInDiv" runat="server">
    <div class="error">You need to be logged in in order to use this page!</div>
    </asp:Panel>
    
    </form>
</asp:Content>

