﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;


// tags etc
//id,pageId,tagType,tagRelatedId,tagText,
//tagStyleClass,tagFrontColor,tagFontDecoration,tagFontWeight,tagFontSize,tagBgColor,
//tagBorderSize,tagBorderColor,tagHorizantalAlign,positionInPage


public partial class manage_manageSiteEditPageAddNewItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
         Functions f = new Functions();

         if (f.LoginCheck() == false)
         {

             //login wrong
             loggedinDiv.Visible = false;
             notLoggedInDiv.Visible = true;

          }
         else
         {
             subdomainName.Value = f.GetSubdomainNameById(Functions.ReturnNumeric(Request["subdomainid"]));
             if (Request.QueryString["used"] == "flickr")
             {
                 //HiddenField dynamicHiddenField = new HiddenField();
                 //dynamicHiddenField.ID = "dynamicdynamicTextBox";
                 //dynamicHiddenField.Value= ""l
                 //Page.FindControl("Form1").Controls.Add(dynamicTextBox);
                 ImageImageUrl.Text = Request.QueryString["MediumUrl"];
                 ImageLinkTargetUrl.Text = Request.QueryString["MediumUrl"];
                 ImageLinkImageUrl.Text = Request.QueryString["SquareThumbnailUrl"];
             }
         }


    }
    protected void addTagButton_Click (object sender, EventArgs e)
    {

        // show selected panel
        string selectedTag = tagList.SelectedValue.ToString();
        if (String.IsNullOrEmpty(selectedTag))
        {
            selectedTag= "Text";
        }
        Panel panelToShow = (Panel)loggedinDiv.FindControl("Panel_" + selectedTag);
        panelToShow.Visible = true;
        // show save buttons
        SaveTagButtonPanel1.Visible = true;
        AutoBrCheckBoxPlaceHolder.Visible = true;
        //tag spesific properties
        switch (selectedTag)
        {

            case "BR":
                tagType.Value = "BR";
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;
            case "OnlineList":
                tagType.Value = "OnlineList";
                break;

            case "ContactAdminForm":
                tagType.Value = "ContactAdminForm";
                break;

            case "Guestbook":
                tagType.Value = "Guestbook";
                 break;

            case "TotalHits":
                 tagType.Value = "TotalHits";
                 break;

            case "Chat":
                 tagType.Value = "Chat";
                 break;

            case "Poll":
                 tagType.Value = "Poll";
                 break;

            case "File":
                 tagType.Value = "File";
                 break;

            case "HR":
                tagType.Value = "HR";
                break;

            case "ULo":
                tagType.Value = "ULo";
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;

            case "ULc":
                tagType.Value = "ULc";
                break;

            case "LIo":
                tagType.Value = "LIo";
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;

            case "LIc":
                tagType.Value = "LIc";
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;

            case "DIVo":
                tagType.Value = "DIVo";
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagAlignmentList.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagBgColor.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;

            case "DIVc":
                tagType.Value = "DIVc";
                
                break;


            case "ParagraphO":
                tagType.Value = "ParagraphO";
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagAlignmentList.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagBgColor.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
                //AutoBrCheckBoxPlaceHolder.Visible = false;
                AutoBrCheckBox.Checked = false;
                break;

            case "ParagraphC":
                tagType.Value = "ParagraphC";
                break;

            case "SiteMap":
                tagType.Value = "SiteMap";
                break;

            case "OnlineCount":
                tagType.Value = "OnlineCount";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                SaveTagButtonPanel2.Visible = true;
                break;

            case "VisitorIP":
                tagType.Value = "VisitorIP";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                SaveTagButtonPanel2.Visible = true;
                break;


            case "VisitorCountry":
                tagType.Value = "VisitorCountry";

                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                SaveTagButtonPanel2.Visible = true;
                break;

            case "Text":
                tagType.Value = "Text";
                TextText.TextMode = TextBoxMode.SingleLine;
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagAlignmentList.Visible = true;
                Panel_tagBgColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "bText":
                tagType.Value = "bText";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagAlignmentList.Visible = true;
                Panel_tagBgColor.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "ServerTime":
                tagType.Value = "ServerTime";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagBgColor.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "ServerDate":
                tagType.Value = "ServerDate";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                Panel_tagBgColor.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "TextLink":
                tagType.Value = "TextLink";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "iTextLink":
                tagType.Value = "iTextLink";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagFontColor.Visible = true;
                Panel_tagFontDecoration.Visible = true;
                Panel_tagFontWeight.Visible = true;
                Panel_tagFontSize.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "Image":
                tagType.Value = "Image";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
            break;

            case "ImageLink":
                tagType.Value = "ImageLink";
                
                styleOptionsLabel.Visible = true;
                manualStyleOptionsLabel.Visible = true;
                Panel_tagStyleClass.Visible = true;
                Panel_tagBorderSize.Visible = true;
                Panel_tagBorderColor.Visible = true;
                SaveTagButtonPanel2.Visible = true;
             break;

            case "HtmlCode":
                tagType.Value = "HtmlCode";
                
            break;

            case "CodeSnippet":
                tagType.Value = "CodeSnippet";
            break;

            case "RssFeed":
                tagType.Value = "RssFeed";
            break;

            case "LinkRotator":
                tagType.Value = "LinkRotator";
                break;

            case "Advert":
                tagType.Value = "Advert";
            break;

            //default: other++; break; 
        }

    }
    protected void SaveTagButton_Click (object sender, EventArgs e)
    {

       Functions f = new Functions();
        myPageBuilder builder = new myPageBuilder();

             int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
             string SubdomainToSave = f.GetSubdomainNameById(subdomainid);
             int PageIdToSave = Functions.ReturnNumeric(Request["pageid"]);
             int prevPositiontagid = Functions.ReturnNumeric(Request["prevPositiontagid"]);
             int tagRelatedId = 0;


        //set refresh
        HtmlMeta meta = new HtmlMeta();
        meta.HttpEquiv = "refresh";
        meta.Content = "3;url=manageSiteEditPage.aspx?un="+Functions.un+"&pw="+Functions.pw+"&subdomainid="+subdomainid+"&pageid="+PageIdToSave;
        this.Header.Controls.Add(meta);






        SaveButtonResultPanel.Visible = true;

        string TagTypeToSave    = Functions.LimitString(tagType.Value.ToString(),20);
        TagTypeToSave = Server.HtmlEncode(TagTypeToSave);
        TagTypeToSave = f.SqlProtect(TagTypeToSave);

        string CssClassToSave = Functions.LimitString(extractedCss.SelectedValue.ToString(),20);
        CssClassToSave = Server.HtmlEncode(CssClassToSave);
        CssClassToSave = f.SqlProtect(CssClassToSave);

        string FontColourToSave = Functions.LimitString(FontColourList.SelectedValue.ToString(),20);
        FontColourToSave = Server.HtmlEncode(FontColourToSave);
        FontColourToSave = f.SqlProtect(FontColourToSave);

        string FontDecorationToSave = Functions.LimitString(fontDecorationList.SelectedValue.ToString(),20);
        FontDecorationToSave = Server.HtmlEncode(FontDecorationToSave);
        FontDecorationToSave = f.SqlProtect(FontDecorationToSave);

        string FontWeightToSave = Functions.LimitString(fontWeightList.SelectedValue.ToString(),20);
        FontWeightToSave = Server.HtmlEncode(FontWeightToSave);
        FontWeightToSave = f.SqlProtect(FontWeightToSave);

        string FontSizeToSave   = Functions.LimitString(fontSizeList.SelectedValue.ToString(),20);
        FontSizeToSave = Server.HtmlEncode(FontSizeToSave);
        FontSizeToSave = f.SqlProtect(FontSizeToSave);

        string AlignmentToSave = Functions.LimitString(tagAlignmentList.SelectedValue.ToString(), 20);
        AlignmentToSave = Server.HtmlEncode(AlignmentToSave);
        AlignmentToSave = f.SqlProtect(AlignmentToSave);

        string TagBgColorToSave = Functions.LimitString(tagBgColorList.SelectedValue.ToString(),20);
        TagBgColorToSave = Server.HtmlEncode(TagBgColorToSave);
        TagBgColorToSave = f.SqlProtect(TagBgColorToSave);

        int TagtagBorderSizeToSave = Functions.ReturnNumeric(tagBorderSizeList.SelectedValue.ToString());
        
        string TagBorderColourToSave = Functions.LimitString(tagBorderColourList.SelectedValue.ToString(), 20);
        TagBorderColourToSave = Server.HtmlEncode(TagBorderColourToSave);
        TagBorderColourToSave = f.SqlProtect(TagBorderColourToSave);


        int limit; // gets limits per page from web.config


      
        switch (TagTypeToSave)
        {
                


            // save item -------------------
            case "Text":
  
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().TextInputLimitPerPage);
                string TextToSave= Functions.LimitString(TextText.Text.ToString(),500);
                TextToSave = Server.HtmlEncode(TextToSave);
                TextToSave = f.SqlProtect(TextToSave);
                if (String.IsNullOrEmpty(TextToSave))
                {
                    TextToSave = "New text";
                }
                if (builder.AddTag(limit,Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty,string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
            else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ServerTime":
                string _ddlTimeZone = Functions.ReturnAlphaNumeric(ddlTimeZone.SelectedValue);

                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, _ddlTimeZone, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "SiteMap":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().SiteMapLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;


            // save item -------------------
            case "ServerDate":
                string __ddlTimeZone = Functions.ReturnAlphaNumeric(ddlTimeZone2.SelectedValue);

                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, __ddlTimeZone, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "DIVo":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "DIVc":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ParagraphO":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;


            // save item -------------------
            case "Guestbook":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "Chat":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ParagraphC":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ContactAdminForm":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                string submitButtonTitle = Functions.LimitString(submitButtonTextBox.Text.ToString(), 50);
                submitButtonTitle = Server.HtmlEncode(submitButtonTitle);
                submitButtonTitle = f.SqlProtect(submitButtonTitle);

                string messageBoxTitle = Functions.LimitString(messageBoxTextBox.Text.ToString(), 50);
                messageBoxTitle = Server.HtmlEncode(messageBoxTitle);
                messageBoxTitle = f.SqlProtect(messageBoxTitle);

                int returnPageLink = Functions.ReturnNumeric(ReturnPageDropDownList.SelectedValue.ToString());
                string __returnPageLink = "Page" + returnPageLink + ".aspx";

                if (String.IsNullOrEmpty(submitButtonTitle))
                {
                    TextToSave = "Send";
                }
                if (String.IsNullOrEmpty(messageBoxTitle))
                {
                    TextToSave = "Your message to admin";
                }

                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, submitButtonTitle, messageBoxTitle,__returnPageLink, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "bText":
                limit =Convert.ToInt32(SiteConfiguration.GetConfig().TextInputLimitPerPage);
                string bTextToSave = Functions.LimitString(bTextText.Text.ToString(), 500);
                TextToSave = Server.HtmlEncode(bTextToSave);
                TextToSave = f.SqlProtect(TextToSave);
                TextToSave = f.NewLineBreak(TextToSave);
                if (String.IsNullOrEmpty(TextToSave))
                {
                    TextToSave = "New text";
                }

                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "BR":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "<br/>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "VisitorIP":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "OnlineList":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;
            // save item -------------------
            case "VisitorCountry":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "OnlineCount":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "TotalHits":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "HR":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "<hr/>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ULo":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "<ul>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ULc":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "</ul>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;


            // save item -------------------
            case "LIo":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "<li>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "LIc":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                TextToSave = "</li>";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, TextToSave, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "iTextLink":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                string _iTextLinkTitle = Functions.LimitString(iTextLinkTitle.Text.ToString(), 50);
                _iTextLinkTitle = Server.HtmlEncode(_iTextLinkTitle);
                _iTextLinkTitle = f.SqlProtect(_iTextLinkTitle);
                int _iTextLinkUrl = Functions.ReturnNumeric(iTextLinkUrl.SelectedValue.ToString());
                string __iTextLinkUrl = "Page" + _iTextLinkUrl + ".aspx";
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, _iTextLinkTitle, __iTextLinkUrl, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "TextLink":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                string _TextLinkTitle = Functions.LimitString(TextLinkTitle.Text.ToString(), 50);
                _TextLinkTitle = Server.HtmlEncode(_TextLinkTitle);
                _TextLinkTitle = f.SqlProtect(_TextLinkTitle);
                string _TextLinkUrl = Functions.LimitString(TextLinkUrl.Text.ToString(), 250);
                _TextLinkUrl = Server.HtmlEncode(_TextLinkUrl);
                _TextLinkUrl = f.SqlProtect(_TextLinkUrl);


                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, _TextLinkTitle, _TextLinkUrl, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "Image":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                string _ImageUrl = Functions.LimitString(ImageImageUrl.Text.ToString(), 255);
                _ImageUrl = Server.HtmlEncode(_ImageUrl);
                _ImageUrl = f.SqlProtect(_ImageUrl);


                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, _ImageUrl, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "ImageLink":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                string _ImageLinkImageUrl = Functions.LimitString(ImageLinkImageUrl.Text.ToString(), 255);
                _ImageLinkImageUrl = Server.HtmlEncode(_ImageLinkImageUrl);
                _ImageLinkImageUrl = f.SqlProtect(_ImageLinkImageUrl);
                string _ImageLinkTargetUrl = Functions.LimitString(ImageLinkTargetUrl.Text.ToString(), 255);
                _ImageLinkTargetUrl = Server.HtmlEncode(_ImageLinkTargetUrl);
                _ImageLinkTargetUrl = f.SqlProtect(_ImageLinkTargetUrl);


                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, _ImageLinkImageUrl, _ImageLinkTargetUrl, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "HtmlCode":
                limit =  Convert.ToInt32(SiteConfiguration.GetConfig().TextInputLimitPerPage);
                string _HtmlCodeText = Functions.LimitString(HtmlCodeText.Text.ToString(),1000);
                _HtmlCodeText = Server.HtmlEncode(_HtmlCodeText);
                _HtmlCodeText = f.SqlProtect(_HtmlCodeText);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, _HtmlCodeText, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "CodeSnippet":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().TextInputLimitPerPage);
                tagRelatedId = Functions.ReturnNumeric(CodeSnippetDropDownList.SelectedValue.ToString());
                if (tagRelatedId <= 0)
                {
                    notSavedResult();
                }
                else
                {
                    if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                    {
                        savedResult();
                    }
                    else
                    {
                        notSavedResult();

                    }
                }
                break;

            // save item -------------------
            case "Poll":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().OnlyOneItemPerPage);
                tagRelatedId = Functions.ReturnNumeric(pollDropDownList.SelectedValue.ToString());
                if (tagRelatedId <= 0)
                {
                    notSavedResult();
                }
                else
                {
                    if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                    {
                        savedResult();
                    }
                    else
                    {
                        notSavedResult();

                    }
                }
                break;
            // save item -------------------
            case "RssFeed":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().RssFeedLimitPerPage);
                tagRelatedId = Functions.ReturnNumeric(RssFeedDropDownList.SelectedValue.ToString());
                int FeedPerPageC = Functions.ReturnNumeric(FeedPerPage.SelectedValue.ToString());

                if (tagRelatedId <= 0 || FeedPerPageC <= 0 || FeedPerPageC > 20)
                {
                    notSavedResult();
                }
                else
                {
                    if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, FeedPerPageC.ToString(), string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                    {
                        savedResult();
                    }
                    else
                    {
                        notSavedResult();

                    }
                }
                break;

            // save item -------------------
            case "File":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().DefaultItemLimitPerPage);
                tagRelatedId = Functions.ReturnNumeric(FileDropDownList.SelectedValue.ToString());
                string FileLinkType = Functions.ReturnAlphaNumeric(LinkStyleDropDownList.SelectedValue.ToString());
                string tagLinkTitle = Functions.LimitString(FileLinkTitle.Text.ToString(), 50);
                tagLinkTitle = Server.HtmlEncode(tagLinkTitle);
                tagLinkTitle = f.SqlProtect(tagLinkTitle);

                if (String.IsNullOrEmpty(tagLinkTitle) == true && FileLinkType=="href")
                tagLinkTitle = "New link";

                if (tagRelatedId > 0)
                {
                    if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, FileLinkType, tagLinkTitle, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                    {
                        savedResult();
                    }
                    else
                    {
                        notSavedResult();

                    }
                }
                else
                {
                    notSavedResult();
                }
                break;

            // save item -------------------
            case "LinkRotator":
              limit = Convert.ToInt32(SiteConfiguration.GetConfig().TextInputLimitPerPage);
                if (builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;

            // save item -------------------
            case "Advert":
                limit = Convert.ToInt32(SiteConfiguration.GetConfig().AdvertiserLimitPerPage);
                tagRelatedId = Functions.ReturnNumeric(ListAdvertsDropDownList.SelectedValue.ToString());

                if (tagRelatedId > 0 && builder.AddTag(limit, Functions.un, SubdomainToSave, PageIdToSave, TagTypeToSave, tagRelatedId, string.Empty, string.Empty, string.Empty, CssClassToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, FontSizeToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, prevPositiontagid, AlignmentToSave) == true)
                {
                    savedResult();
                }
                else
                {
                    notSavedResult();

                }
                break;


        }

        if (AutoBrCheckBox.Checked == true)// is new line checked?
        {
             builder.AddAutoBrTag(PageIdToSave, "BR", "<br/>");
        }
        

   
     }


public void savedResult()
    {
 
        SaveButtonResultLabel.Text = "Item has been added";
        tagListPanel.Visible = false;
    }
public void notSavedResult()
    {
    SaveButtonResultLabel.CssClass = "error";    
    SaveButtonResultLabel.Text = "There was an error! Make sure you don't try to add more items than set limits";
    }

    protected void FontColourList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < FontColourList.Items.Count; i++)
        {
            FontColourList.Items[i].Attributes.Add("style", "background-color: " + FontColourList.Items[i].Value);
        }
    }

    protected void tagBgColorList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < tagBgColorList.Items.Count; i++)
        {
            tagBgColorList.Items[i].Attributes.Add("style", "background-color: " + tagBgColorList.Items[i].Value);
        }
    }

    protected void tagBorderColourList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < tagBorderColourList.Items.Count; i++)
        {
            tagBorderColourList.Items[i].Attributes.Add("style", "background-color: " + tagBorderColourList.Items[i].Value);
        }
    }
    protected void UploadFile(object sender, EventArgs e)
    {

        if (File1.PostedFile != null && File1.PostedFile.ContentLength > 0 && (File1.PostedFile.ContentType == "text/html" || File1.PostedFile.ContentType == "text/plain"))
        {
            HttpPostedFile myFile = File1.PostedFile;
            int nFileLen = myFile.ContentLength;
            byte[] myData = new byte[nFileLen];
            myFile.InputStream.Read(myData, 0, nFileLen);
            string importedTxt = Encoding.UTF8.GetString(myData);
            importedTxt = Regex.Replace(importedTxt, @"</?(?i:body|head|title|link|html|!DOCTYPE|meta|\?xml)(.|\n)*?>", "");
            HtmlCodeText.Text = Functions.LimitString(importedTxt, 1000);
        }
        else
        {
            HtmlCodeText.Text = "Unable to import. Try with more advanced browser/phone!";
        }

        Panel_HtmlCode.Visible = true;
        tagType.Value = "HtmlCode";
        SaveTagButtonPanel1.Visible = true;

    }
}
