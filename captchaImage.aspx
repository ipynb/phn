﻿<%@ Page Language="C#" %>
<%@ Import Namespace="System.Drawing.Imaging" %>
<%@ Import Namespace="CaptchaDLL" %><%
                                        
        
        if (Cache["CaptchaImageText"] == null)
        {
            Cache.Insert(
            "CaptchaImageText",
           CaptchaImage.GenerateRandomCode(CaptchaType.AlphaNumeric, 4),
            null,
            DateTime.Now.AddHours(1), Cache.NoSlidingExpiration);
         }



                                        
                                        
                                        
                                        
        CaptchaImage ci = new CaptchaImage(Cache["CaptchaImageText"].ToString(), 90, 30);
            //YOU CAN USE THE OTHER OVERLOADED METHODS ALSO
            //CaptchaImage ci = new CaptchaImage(Session["CaptchaImageText"].ToString(), 200, 50, "Courier New");
            //CaptchaImage ci = new CaptchaImage(Session["CaptchaImageText"].ToString(), 200, 50, "Courier New" ,System.Drawing.Color.White, System.Drawing.Color.Red);

            // Change the response headers to output a JPEG image.
            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";

            // Write the image to the response stream in JPEG format.
            ci.Image.Save(this.Response.OutputStream, ImageFormat.Jpeg);

            // Dispose of the CAPTCHA image object.
            ci.Dispose();
%>