﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" %>
<script runat="server">
    protected void Page_Load(object sender, EventArgs e)
    {
        ((HtmlControl)this.Master.FindControl("footer")).Visible = false;
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
    Welcome to <%=SiteConfiguration.GetConfig().siteName%> the best mobile site builder around!
    <br />
    We provide a free, high-end mobile web design tool. With <%=SiteConfiguration.GetConfig().siteName%> you can create a professional mobile web site without any knowledge of html or a scripting language!
  <div class="gap"></div>
    <div class="center">
        <a href="manage/default.aspx"><img src="images/logon.gif" alt=""/> Logon</a>
        <br/>
        <a href="manage/register.aspx"><img src="images/register.gif" alt=""/> Register</a><br/>
        <a href="verify.aspx"><img src="images/verify.gif" alt=""/> Verify account</a>

    </div>
    <div class="gap"></div>
    <div class="title"><img src="images/features.gif" alt=""/> Features</div>
    <ul>
    <li>Totally FREE!</li>
    <li>Your own subdomain!<br /><%Functions f = new Functions(); f.listAvailableDoaminsFromWEbConfig2(); %></li>
	<li>Most advanced mobile web designer</li>
	<li>Earn money from adverts with the help of companies like Admob, Mojiva, Buzzcity etc.</li>
    <li>HTML allowed to design your site manually</li>
	<li>Use style sheets to design your site</li>
    <li>Code snippets</li>
    <li>Link rotator add-on</li>
    <li>Re-publish RSS feeds add-on</li>
    <li>Guestbook add-on</li>
    <li>Poll and Quize add-on</li>
    <li>Chat add-on</li>
    <li>Email to admin add-on</li>
    <li>Online visitor list add-on</li>
    <li>Show visitors' country and flag add-on</li>
    <li>Insert images from Flickr and videos from Youtube to your site</li>  
    <li>Forums to get help from fellow wapmasters</li>
    <li>And many more...</li>
    </ul>
     
     
<div class="gap"></div>
<div class="title"><img src="images/help.png" alt=""/> Help &amp; support</div>
<a href="/forums.aspx"><img src="images/forum.gif" alt=""/> Support forum</a>
<br />
<a href="/news.aspx"><img src="images/icon_sticky.gif" alt=""/> Announcements</a>
<br />
<a href="http://support.phn.me/page3069.aspx"><img src="images/email.gif" alt=""/> Contact</a>

<div class="gap"></div>
<div class="title"><img src="images/url.gif" alt=""/> Stats</div>
<img src="images/bluearrow.gif" alt=""/> <a href="sitesPopular.aspx">List of popular sites</a>
<br />
<img src="images/bluearrow.gif" alt=""/> <a href="sites.aspx">List of latest sites</a>
<br />
<img src="images/bluearrow.gif" alt=""/> <a href="onlines.aspx">Global online visitors</a>
<div class="gap"></div>
<div class="center"><a href="http://waptrack.net/welcome/24459"><img src="http://waptrack.net/waptrack.gif" alt="WapTrack Toplist" /></a>

</div>
</asp:Content>