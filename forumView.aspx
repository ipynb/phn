﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"%>
<asp:Content ID="Content1" ContentPlaceHolderID="body" Runat="Server">
<% 
    int id = Functions.ReturnNumeric(Request.QueryString["id"]);
    int pageNumber = Functions.ReturnNumeric(Request["p"]);
    int pageSize = 10;
    if (id != 0)
    {

        Functions f = new Functions();
        StringBuilder s = new StringBuilder();
        s.Append("<div class=\"title\"><a class=\"bold\" href=\"forums.aspx?un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Forum</a> <img src=\"/images/bluearrow.gif\" alt=\"&gt;\"/> ");
        s.Append("<b>");
        s.Append(f.GetPostTopic(id));
        s.Append("</b>");
        if (Session["IsGlobalAdmin"].ToString() == "yes")
        {
            s.Append(" <a onclick=\"return confirm('Are you sure?')\" href=\"forumManage.aspx?id=" + id + "&amp;del=topic&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "\"><img src=\"/images/delete.png\" alt=\"\" /></a>");
        }
        s.Append("</div>");
        Response.Write(s.ToString());

        f.ViewForumTopic(id, pageNumber, pageSize); %>
    
    <%//if username exist show forumpost 
    if (String.IsNullOrEmpty(Request.QueryString["un"]) == false)
    {%>
       
    <div class="title">Add reply</div>
    <form method="post" action="forumReplyUpdate.aspx">
     <p>
     <input name="un" value="<%=Functions.un %>" type="hidden" />
     <input name="pw" value="<%=Functions.pw %>" type="hidden" />
     <input name="id" value="<%= id %>" type="hidden" />
     Message:<br/>
    <textarea name="replyBody" rows="5" class="fullscreen"></textarea>
    <br />
    <input type="submit" value="Post" />
    </p>
    </form>
    
    <%}
    }%>

     

    </asp:Content>

