﻿using System;
using System.Configuration;
using System.Data;

using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class manage_manageSiteEditPageEditItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {
            int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
            string SubdomainToSave = f.GetSubdomainNameById(subdomainid);
            int PageIdToSave = Functions.ReturnNumeric(Request["pageid"]);
            int TagIdToSave = Functions.ReturnNumeric(Request["tagid"]);
            subdomainName.Value = SubdomainToSave;

            if (Page.IsPostBack)
            {
                UpdateTagById(SubdomainToSave, PageIdToSave, TagIdToSave, tagTypeText.Value.ToString());
            }
            else
            {
                GetTagById(SubdomainToSave, PageIdToSave, TagIdToSave);
            }

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }
    }

    protected void GetTagById(string subdomain, int pageid, int tagid)
    {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetTagById", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@pageid", SqlDbType.Int).Value = pageid;
                cmd.Parameters.Add("@tagid", SqlDbType.Int).Value = tagid;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string tagtype = reader["tagType"].ToString();
                        string tagText = reader["tagText"].ToString();
                        string tagStyleClass = reader["tagStyleClass"].ToString();
                        string tagFontColor = reader["tagFontColor"].ToString();
                        string tagFontDecoration = reader["tagFontDecoration"].ToString();
                        string tagFontWeight = reader["tagFontWeight"].ToString();
                        string tagFontSize = reader["tagFontSize"].ToString();
                        string tagBgColor = reader["tagBgColor"].ToString();
                        string tagBorderSize = reader["tagBorderSize"].ToString();
                        string tagBorderColor = reader["tagBorderColor"].ToString();
                        string tagAlignment = reader["tagAlignment"].ToString();
                        tagTypeText.Value = tagtype;

                        if (tagtype == "bText" || tagtype == "Text" || tagtype == "HtmlCode")
                        {
                            textEditPanel.Visible = true;

                            if (tagtype == "Text")
                            {
                                textEditTextBox.TextMode = TextBoxMode.SingleLine;
                            }
                            if (tagtype == "HtmlCode")
                            {
                                styleOptionsLabel.Visible = false;
                                manualStyleOptionsLabel.Visible = false;
                                Panel_tagStyleClass.Visible = false;
                                Panel_tagFontColor.Visible = false;
                                Panel_tagFontDecoration.Visible = false;
                                Panel_tagFontWeight.Visible = false;
                                Panel_tagFontSize.Visible = false;
                                Panel_tagBgColor.Visible = false;
                                Panel_tagBorderSize.Visible = false;
                                Panel_tagBorderColor.Visible = false;
                                updateButton2.Visible = false;
                            }

                            textEditTextBox.CssClass = "fullscreen";
                            textEditTextBox.Rows = 5;
                            textEditTextBox.Text = Server.HtmlDecode(tagText);


                            if (tagtype == "bText" || tagtype == "Text")
                            {
                                extractedCss.Items.FindByValue("-").Value = tagStyleClass;
                                fontSizeList.Items.FindByValue("-").Value = tagFontSize;
                                FontColourList.Items.FindByValue("-").Value = tagFontColor;
                                fontDecorationList.Items.FindByValue("-").Value = tagFontDecoration;
                                fontWeightList.Items.FindByValue("-").Value = tagFontWeight;
                                tagBgColorList.Items.FindByValue("-").Value = tagBgColor;
                                tagBorderSizeList.Items.FindByValue("-").Value = tagBorderSize;
                                tagBorderColourList.Items.FindByValue("-").Value = tagBorderColor;
                                tagAlignmentList.Items.FindByValue("-").Value = tagAlignment;

                            }
                        }
                        
                    }
                    else
                    {
                        UpdateButtonResultPanel.Visible = true;
                        UpdateButtonResultLabel.CssClass = "error";
                        UpdateButtonResultLabel.Text = "Unable to find item!";
                        styleOptionsLabel.Visible = false;
                        manualStyleOptionsLabel.Visible = false;
                        Panel_tagStyleClass.Visible = false;
                        Panel_tagFontColor.Visible = false;
                        Panel_tagFontDecoration.Visible = false;
                        Panel_tagFontWeight.Visible = false;
                        Panel_tagFontSize.Visible = false;
                        Panel_tagBgColor.Visible = false;
                        Panel_tagBorderSize.Visible = false;
                        Panel_tagAlignmentList.Visible = false;
                        tagAlignmentList.Visible = false;
                        updateButton2.Visible = false;
                        updateButton1.Visible = false;
                    }
                }
            }
        }

    }

    protected void FontColourList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < FontColourList.Items.Count; i++)
        {
            FontColourList.Items[i].Attributes.Add("style", "background-color: " + FontColourList.Items[i].Value);
        }
    }

    protected void tagBgColorList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < tagBgColorList.Items.Count; i++)
        {
            tagBgColorList.Items[i].Attributes.Add("style", "background-color: " + tagBgColorList.Items[i].Value);
        }
    }

    protected void tagBorderColourList_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < tagBorderColourList.Items.Count; i++)
        {
            tagBorderColourList.Items[i].Attributes.Add("style", "background-color: " + tagBorderColourList.Items[i].Value);
        }
    }
    protected void UpdateTagById(string subdomain, int pageid, int tagid, string tagtype)
    {
        Functions f = new Functions();
        UpdateButtonResultPanel.Visible = true;
        styleOptionsLabel.Visible = false;
        manualStyleOptionsLabel.Visible = false;
        Panel_tagStyleClass.Visible = false;
        Panel_tagFontColor.Visible = false;
        Panel_tagFontDecoration.Visible = false;
        Panel_tagFontWeight.Visible = false;
        Panel_tagFontSize.Visible = false;
        Panel_tagBgColor.Visible = false;
        Panel_tagBorderSize.Visible = false;
        Panel_tagBorderColor.Visible = false;
        Panel_tagAlignmentList.Visible = false;
        updateButton2.Visible = false;
        updateButton1.Visible = false;

        string TextToSave;

        if (tagtype=="HtmlCode")
        {
        TextToSave = Functions.LimitString(textEditTextBox.Text.ToString(), 1000);
        }
        else
        {
        TextToSave = Functions.LimitString(textEditTextBox.Text.ToString(), 500);
        }

        TextToSave = Server.HtmlEncode(TextToSave);
        TextToSave = f.SqlProtect(TextToSave);

        string CssClassToSave = Functions.LimitString(extractedCss.SelectedValue.ToString(), 20);
        CssClassToSave = Server.HtmlEncode(CssClassToSave);
        CssClassToSave = f.SqlProtect(CssClassToSave);
 

        string FontColourToSave = Functions.LimitString(FontColourList.SelectedValue.ToString(), 20);
        FontColourToSave = Server.HtmlEncode(FontColourToSave);
        FontColourToSave = f.SqlProtect(FontColourToSave);
      

        string FontDecorationToSave = Functions.LimitString(fontDecorationList.SelectedValue.ToString(), 20);
        FontDecorationToSave = Server.HtmlEncode(FontDecorationToSave);
        FontDecorationToSave = f.SqlProtect(FontDecorationToSave);

        string FontWeightToSave = Functions.LimitString(fontWeightList.SelectedValue.ToString(), 20);
        FontWeightToSave = Server.HtmlEncode(FontWeightToSave);
        FontWeightToSave = f.SqlProtect(FontWeightToSave);

        string FontSizeToSave = Functions.LimitString(fontSizeList.SelectedValue.ToString(), 20);
        FontSizeToSave = Server.HtmlEncode(FontSizeToSave);
        FontSizeToSave = f.SqlProtect(FontSizeToSave);

        string AlignmentToSave = Functions.LimitString(tagAlignmentList.SelectedValue.ToString(), 20);
        AlignmentToSave = Server.HtmlEncode(AlignmentToSave);
        AlignmentToSave = f.SqlProtect(AlignmentToSave);

        string TagBgColorToSave = Functions.LimitString(tagBgColorList.SelectedValue.ToString(), 20);
        TagBgColorToSave = Server.HtmlEncode(TagBgColorToSave);
        TagBgColorToSave = f.SqlProtect(TagBgColorToSave);

        int TagtagBorderSizeToSave = Functions.ReturnNumeric(tagBorderSizeList.SelectedValue.ToString());

        string TagBorderColourToSave = Functions.LimitString(tagBorderColourList.SelectedValue.ToString(), 20);
        TagBorderColourToSave = Server.HtmlEncode(TagBorderColourToSave);
        TagBorderColourToSave = f.SqlProtect(TagBorderColourToSave);
        myPageBuilder builder = new myPageBuilder();

        if (builder.UpdateTag(subdomain, pageid, tagid, TextToSave, CssClassToSave, FontSizeToSave, FontColourToSave, FontDecorationToSave, FontWeightToSave, TagBgColorToSave, TagtagBorderSizeToSave, TagBorderColourToSave, AlignmentToSave) == true)
        {
            UpdateButtonResultLabel.Text = "Item updated";
        }
        else
        {
          
            UpdateButtonResultLabel.CssClass = "error";
            UpdateButtonResultLabel.Text = "Unable to update item!";

        }

   }
}
