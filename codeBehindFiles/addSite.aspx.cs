﻿using System;
using System.Web;

public partial class manage_addSite : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        
        string msg;
               
        
        if (f.LoginCheck() == true)
        {
                                        string subdomain = Functions.LimitString(HttpContext.Current.Request["subdomain"], 50);
                                        string domain = Functions.LimitString(HttpContext.Current.Request["domain"], 50);
                                        int isR = Convert.ToInt32(Functions.LimitString(HttpContext.Current.Request["isR"], 1));
                                        //adult keyworded sites set to adult only
                                        if (f.isAdultKeyword(subdomain.ToLower()) == true || f.isAdultKeyword(domain.ToLower()) == true)
                                        {
                                            isR = 1;
                                        }

                                        bool isReserved;

                                        try
                                        {
                                            isReserved = f.isReservedSub(subdomain.ToLower());
                                        }
                                        catch
                                        {
                                            isReserved = true;
                                        }


                                        if (isReserved == true) //www reserved subdomain
                                        {
                                            changeResult.CssClass = "error";
                                            msg = "This is a reserved subdomain name and cannot be used!";
                                        }
                                        else
                                        {
                                            if (
                                                Functions.IsAlphaNumericForSubdomain(subdomain) == true
                                                && Functions.IsAlphaNumeric(domain) == true
                                                && f.isDomaininWebConfig(domain) > -1)
                                            {

                                                if (f.RegisterSubdomain(Functions.un, subdomain, domain, isR) == true)
                                                {
                                                    msg = "Your site has been created. You can start creating and designing it's pages. Just click link below to go to account manager and then select newly created site to access it's settings";

                                                }
                                                else
                                                {
                                                    changeResult.CssClass = "error";
                                                    msg = "Subdomain name is in use or you have reached your site limit";

                                                }

                                            }
                                            else
                                            {
                                                changeResult.CssClass = "error";
                                                msg = "Subdomain must be alpha-numeric (a-z-0-9)!";

                                            }
                                        }

        }




        else
        {
            changeResult.CssClass = "error";
                                    msg="You need to be logged in in order to use this page!";
            
        }

        changeResult.Text = msg + "<br/><a href=\"default.aspx?un=" + Functions.un + "&amp;pw=" + Functions.pw + "\">Ok</a>";
        
    }
 
}
