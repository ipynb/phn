﻿using System;
using System.Text;
using System.Data;



public partial class manage_manageSiteEditPage : System.Web.UI.Page
{
    public string listPages()
    {
      int pageid = Functions.ReturnNumeric(Request["pageid"]);
             int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);

    StringBuilder sb = new StringBuilder();    
    DataSet ds = new DataSet();
    myPageBuilder b = new myPageBuilder();
        Functions f = new Functions();

        sb.Append("<form id=\"frm\" method=\"get\" action=\"" + Request.FilePath + "\"><p><select name=\"pageid\" onchange=\"javascript:document.forms['frm'].submit();\">");

        ds = b.ListPages(f.GetSubdomainNameById(subdomainid), Functions.un);
        string selected;
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
            if (pageid == Functions.ReturnNumeric(dr["id"].ToString()))
            { selected = "selected=\"selected\""; }
            else
            {
                selected = "";
            }

            sb.AppendFormat("<option value=\"{0}\" {2}>{1}</option>", dr["id"], dr["title"], selected);
        }


        sb.Append("</select><input type=\"hidden\" name=\"un\" value=\"" + Functions.un + "\" /><input type=\"hidden\" name=\"pw\" value=\"" + Functions.pw + "\" /><input type=\"hidden\" name=\"subdomainid\" value=\"" + subdomainid + "\" /><input type=\"submit\" value=\"Go\" /></p></form>");
        return sb.ToString();
    }
    protected void Page_Load(object sender, EventArgs e)
    {
         Functions f = new Functions();

         if (f.LoginCheck() == true)
         {

             int pageid = Functions.ReturnNumeric(Request["pageid"]);
             int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
             //get full sitenamedomain
             string fullsitedomain = f.GetFullSubdomainNameById(subdomainid);
             //fullSiteNameLabel.Text = fullsitedomain;
             editPageLabel.Text = "page" + pageid +".aspx <a href=\"http://"+fullsitedomain+"/page"+pageid+".aspx\"><img src=\"../images/url.gif\" alt=\"Preview\"/></a>";


         }
         else
         {
             //login wrong
             loggedinDiv.Visible = false;
             notLoggedInDiv.Visible = true;
         }

    }



}
