﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;

using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;


public partial class pollVoteEngine : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        int limit = 30;//limit in SECONDS bitween messages;
        string subdomain = Functions.LimitString(Request["subdomain"], 50);
        string domain = Functions.LimitString(Request["domain"], 50);
        string Ip = Request.ServerVariables["REMOTE_ADDR"];
        string returnUrl = Request["returnUrl"];
        int pollid = Functions.ReturnNumeric(Request["pollid"]);
        int answerid = Functions.ReturnNumeric(Request["answer"]);

        string captcha = Functions.ReturnAlphaNumeric(Request["captcha"]).ToLower();
        string generatedCaptcha;
        // somehow cache fails to hold captcah text so at those times i don' do check 
        if (Cache["CaptchaImageText"] != null)
        {
            generatedCaptcha = Cache["CaptchaImageText"].ToString().ToLower();
        }
        else
        {
            generatedCaptcha = captcha;
        }
        //-------------------------------------------------------------------------------


        if (String.IsNullOrEmpty(subdomain) == true
            || Functions.IsAlphaNumericForSubdomain(subdomain) == false
               || String.IsNullOrEmpty(domain) == true
                  || String.IsNullOrEmpty(returnUrl) == true
                      || String.IsNullOrEmpty(captcha) == true
                           || captcha != generatedCaptcha)
        {
            // all fields are required;
            Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?err=1#err");
        }
        else
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("poll_vote_now", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pollid", SqlDbType.Int).Value = pollid;
                    cmd.Parameters.Add("@answerid", SqlDbType.Int).Value = answerid;
                    cmd.Parameters.Add("@posterIp", SqlDbType.VarChar).Value = Ip;
                    cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;

                    SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                    success.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(success);
                    cmd.ExecuteScalar();
                    bool result = Convert.ToBoolean(success.Value);

                    //int result = (int)cmd.ExecuteScalar();

                    if (result == true)
                    {
                        Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?v=yes");
                    }
                    else
                    {
                        //Response.Write(pollid);
                        //Response.Write("-");
                        //Response.Write(answerid);
                        //Response.Write("-");
                        //Response.Write(Ip);
                        //Response.Write("-");
                        //Response.Write(limit);
                        Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?err=2#res");
                    }

                }
            }
        }
    }
}
