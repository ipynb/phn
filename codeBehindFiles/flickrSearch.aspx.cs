﻿using System;
using FlickrNet;

public partial class manage_flickrSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        Functions f = new Functions();

        if (f.LoginCheck() == false)
        {

            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        BindMe(1);
        
        //PhotoCollection allPhotos = foundPhotos.PhotoCollection;
        //foreach (Photo photo in allPhotos)
        //{
         //   Response.Write("Photos title is " + photo.Title);
          //  photo.Farm;
           // photo.IconServer;
            //photo.MediumUrl;
            //photo.ThumbnailUrl;
            //photo.SquareThumbnailUrl;
        //}

    }

    protected void BindMe(int currentPage)
    {
        
        cPage.Visible = true;
        goToPanel.Visible = true;
        LabelPagesLabel.Visible = true;
        int perPage=5;
        if (currentPage > 1000)
            currentPage = 1000;
        int npage= currentPage+1;
        //do not allow more than 1000 page
        if (npage > 1000)
            npage = 1000;
    


        cPage.Text = npage.ToString();
        Flickr flickr = new Flickr("f46763da16ad4d86acb769f197be13f2");
        PhotoSearchOptions searchOptions = new PhotoSearchOptions();
        searchOptions.Tags = searchTextBox.Text;
        searchOptions.PerPage = perPage;
        searchOptions.AddLicense(1);
        searchOptions.AddLicense(2);
        searchOptions.AddLicense(3);
        searchOptions.AddLicense(4);
        searchOptions.AddLicense(5);
        searchOptions.AddLicense(6);
        searchOptions.AddLicense(7);
        searchOptions.Page = currentPage;
        Photos foundPhotos = flickr.PhotosSearch(searchOptions);
        long totalPages = foundPhotos.TotalPages;
        long totalPhotos = foundPhotos.TotalPhotos;
         LabelPagesLabel.Text = "Page " + currentPage.ToString()+ " of "+ totalPages;


         pageCountLabel.Text = "Total " + totalPhotos + " photo(s) found.<br/>Click <b>Use</b> link below any image to copy details of image and then go to <b>Image</b> or <b>Image link</b> on Add new item menu to use copied details.";

        if (totalPages > 1 && currentPage<totalPages)
            nextButton.Visible = true;
        if (currentPage> 1)
            prevButton.Visible= true;

  
        ListView1.DataSource = foundPhotos.PhotoCollection;
        ListView1.DataBind();
    }
    protected void nextButton_Click(object sender, EventArgs e)
    {

        BindMe(Functions.ReturnNumeric(cPage.Text));
    }
    protected void prevButton_Click(object sender, EventArgs e)
    {
        BindMe(Functions.ReturnNumeric(cPage.Text)-2);
    }
    protected void goToButton_Click(object sender, EventArgs e)
    {
        BindMe(Functions.ReturnNumeric(cPage.Text));
    }

    
}
