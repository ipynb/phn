﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class manage_manageSiteAddPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {


            string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;

            int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
            int pageid = Functions.ReturnNumeric(Request["id"]);
            //get full sitenamedomain
            //fullSiteNameLabel.Text = f.GetFullSubdomainNameById(subdomainid);

            if (!IsPostBack){

                string subdomain = f.GetSubdomainNameById(subdomainid);
                if (Functions.IsAlphaNumericForSubdomain(subdomain) == true)
                {

                    subdomainholder.Value = subdomain;


                    SqlConnection conn = new SqlConnection(connString);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("GetPageHeader", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                    cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                    cmd.Parameters.Add("@id", SqlDbType.VarChar).Value = pageid;

                    SqlDataReader reader = cmd.ExecuteReader();


                    if (reader.Read())
                    {

                        pageidholder.Value = Convert.ToString(pageid);
                        pageTitle.Text = Server.HtmlDecode(reader["title"].ToString());
                        pageKeywords.Text = Server.HtmlDecode(reader["keywords"].ToString());
                        pageDescription.Text = Server.HtmlDecode(reader["description"].ToString());
                        pageOtherMetaTags.Text = Server.HtmlDecode(reader["other_meta"].ToString());
                        pageFolder.Text = Server.HtmlDecode(reader["pageFolder"].ToString());

                        if (!String.IsNullOrEmpty(reader["fontSize"].ToString()))
                        {
                            fontSize.SelectedValue = reader["fontSize"].ToString();
                        }
                        if (!String.IsNullOrEmpty(reader["pageBgColor"].ToString()))
                        {
                            try
                            {
                                pageBgColor.SelectedValue = reader["pageBgColor"].ToString().ToUpper();
                            }
                            catch
                            {
                                pageBgColor.SelectedValue = "#FFFFFF";
                            }

                        }
                        if (!String.IsNullOrEmpty(reader["pageFontColor"].ToString()))
                        {
                            try
                            {
                                pageFontColor.SelectedValue = reader["pageFontColor"].ToString().ToUpper();
                            }
                            catch
                            {
                                pageFontColor.SelectedValue = "#000000";
                            }

                        }

                       
                    }
                }
                else
                {
                    loggedinDiv.Visible = false;
                    result.Visible = true;
                    result.CssClass = "error";
                    result.Text = "An error occured. Can't find subdomain! Do not edit the query string!"; 
                }


                }

       

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {

            string _pageTitle = pageTitle.Text;
            //_pageTitle = f.ReplaceLineBreakForTitles(_pageTitle);
            _pageTitle = Server.HtmlEncode(_pageTitle);
            _pageTitle = f.SqlProtect(_pageTitle);
            _pageTitle = Functions.LimitString(_pageTitle, 100);

            string _pageDescription = pageDescription.Text;
            //_pageDescription = f.ReplaceLineBreakForTitles(_pageDescription);
            _pageDescription = Server.HtmlEncode(_pageDescription);
            _pageDescription = f.SqlProtect(_pageDescription);
            _pageDescription = Functions.LimitString(_pageDescription, 100);

            string _pageKeywords = pageKeywords.Text;
            //_pageKeywords = f.ReplaceLineBreakForTitles(_pageKeywords);
            _pageKeywords = Server.HtmlEncode(_pageKeywords);
            _pageKeywords = f.SqlProtect(_pageKeywords);
            _pageKeywords = Functions.LimitString(_pageKeywords, 100);

            string _pageOtherMetaTags = pageOtherMetaTags.Text;
            //_pageOtherMetaTags = f.ReplaceLineBreakForTitles(_pageOtherMetaTags);
            _pageOtherMetaTags = Server.HtmlEncode(_pageOtherMetaTags);
            _pageOtherMetaTags = f.SqlProtect(_pageOtherMetaTags);
            _pageOtherMetaTags = Functions.LimitString(_pageOtherMetaTags, 500);

            string _fontSize = Functions.LimitString(fontSize.SelectedValue.ToString(), 50);
            //_fontSize = f.ReplaceLineBreakForTitles(_fontSize);
            _fontSize = Server.HtmlEncode(_fontSize);
            _fontSize = f.SqlProtect(_fontSize);

            string _pageBgColor = Functions.LimitString(pageBgColor.SelectedValue.ToString(), 50);
            //_pageBgColor = f.ReplaceLineBreakForTitles(_pageBgColor);
            _pageBgColor = Server.HtmlEncode(_pageBgColor);
            _pageBgColor = f.SqlProtect(_pageBgColor);

            string _pageFontColor = Functions.LimitString(pageFontColor.SelectedValue.ToString(), 50);
            //_pageFontColor = f.ReplaceLineBreakForTitles(_pageFontColor);
            _pageFontColor = Server.HtmlEncode(_pageFontColor);
            _pageFontColor = f.SqlProtect(_pageFontColor);

            string _pageFolder = pageFolder.Text.ToString();
            _pageFolder = f.ReplaceLineBreakForTitles(_pageFolder);
            _pageFolder = Functions.ReturnAlphaNumeric(_pageFolder);
            _pageFolder = Functions.LimitString(_pageFolder, 50);

            if (String.IsNullOrEmpty(_pageFolder) == true)
            {
                _pageFolder = "Root";
            }
            string _subdomain = Functions.LimitString(subdomainholder.Value,50);
            int _pageid = Functions.ReturnNumeric(Request["id"]);
           

            if (Functions.IsAlphaNumericForSubdomain(_subdomain) == true
                && String.IsNullOrEmpty(_pageTitle)==false
                && String.IsNullOrEmpty(_pageKeywords) == false
                && String.IsNullOrEmpty(_pageDescription) == false)
            {

                if (f.UpdateSitePage(Functions.un, _subdomain, _pageid, _pageTitle, _pageDescription, _pageKeywords, _fontSize, _pageBgColor, _pageFontColor, _pageFolder, _pageOtherMetaTags) == true)
                {
                result.Visible = true;
                result.Text = "Page headers has been updated";
                loggedinDiv.Visible = false;
                okLink.Visible = true;
                }
                else{
                loggedinDiv.Visible = false;
                result.Visible = true;
                result.CssClass = "error";
                result.Text = "An error occured. Can't update the page"; 
                
                }

            }
            else
            {
                result.Visible = true;
                result.CssClass = "error";
                result.Text = "All fields are required!";
                
            }

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;
        }

            
    }

    protected void BackgorundDataSource_setStyle(object sender, EventArgs e)
    {
        for (int i = 0; i < pageBgColor.Items.Count; i++)
        {
            pageBgColor.Items[i].Attributes.Add("style", "background-color: " + pageBgColor.Items[i].Value);
        }
    }
    protected void BackgorundDataSource_setStyleFont(object sender, EventArgs e)
    {
        for (int i = 0; i < pageFontColor.Items.Count; i++)
        {
            pageFontColor.Items[i].Attributes.Add("style", "background-color: " + pageBgColor.Items[i].Value);
        }
    }
}
