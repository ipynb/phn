﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.WebControls;


public partial class manage_managePages : System.Web.UI.Page
{
    string selectedFolder;

    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == true)
        {
           
           BindData(selectedFolder);

        }
        else
        {
            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }



    }

    protected void subDomainPageListEventHandler(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        Panel setindexpagediv = (Panel)e.Item.FindControl("setindexpagediv");
        DataRowView drv = (DataRowView)dataItem.DataItem;

        Label pointIndexPageLabel = (Label)e.Item.FindControl("pointIndexPageLabel");


        if (drv["isStartPage"] != DBNull.Value)
        {
            if (Convert.ToInt32(drv["isStartPage"]) != 0)
            {
                pointIndexPageLabel.Text = " <img src=\"../images/defaultpage.gif\" alt=\"*\"/>";
                setindexpagediv.Visible = false;
            }
            else
            {
                pointIndexPageLabel.Visible = false;
            }
        }
        else
        { pointIndexPageLabel.Visible = false; }



    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        selectedFolder = FolderDropDownList.SelectedValue.ToString();
        BindData(selectedFolder);
    }
    protected void pageList_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
    {
        this.pageListPaging.SetPageProperties(e.StartRowIndex, e.MaximumRows, false);

        BindData(selectedFolder);
    }

    protected void BindData(string selectedFolder)
    {
        if (String.IsNullOrEmpty(selectedFolder) == true)
            selectedFolder = string.Empty;

        Functions f = new Functions();
        string connString = ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString;
        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);

        if (String.IsNullOrEmpty(subdomain) == false)
        {
                subdomainName.Value = subdomain;
                //get full sitenamedomain
                string fullsubname = f.GetFullSubdomainNameById(id);
                fullSiteNameLabel.Text = fullsubname;

                //start page list
                SqlConnection conn = new SqlConnection(connString);
                conn.Open();
                SqlCommand cmd = new SqlCommand("listSubdomainPages", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@folder", SqlDbType.VarChar).Value = selectedFolder;

                SqlDataAdapter da = new SqlDataAdapter();
                da.SelectCommand = cmd;

                DataSet ds = new DataSet();
                da.Fill(ds);

                //subDomainInfoList.DataSource = ds.Tables[0];
                //subDomainInfoList.DataBind();
                pageList.DataSource = ds.Tables[1];
                pageList.DataBind();


                //paging setup
                //int currentPage = (pageListPaging.StartRowIndex / pageListPaging.PageSize) + 1;
                //int totalPages = pageListPaging.TotalRowCount / pageListPaging.PageSize;
        }
        else
        {
            fullSiteNameLabel.Text = "Error!";
        }

    }
}
