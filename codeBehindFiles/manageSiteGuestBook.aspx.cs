﻿using System;
using System.Data;


public partial class manageSiteGuestBook : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        


        if (f.LoginCheck() == true)
        {

            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            string subdomainname = f.GetSubdomainNameById(id);

            if (String.IsNullOrEmpty(subdomainname) == false)
            {

                fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);
                myPageBuilder build = new myPageBuilder();
                int perPage = 10;
                int page = Functions.ReturnNumeric(Request["p"]);
                DataTable PagingTable = new DataTable("paging");
                PagingTable = build.TagBuildGuestbook(subdomainname, page, perPage).Tables[0];
                int currentPage = Functions.ReturnNumeric(PagingTable.Rows[0]["CurrentPage"].ToString());
                int totalPages = Functions.ReturnNumeric(PagingTable.Rows[0]["TotalPages"].ToString());
                int totalRecords = Functions.ReturnNumeric(PagingTable.Rows[0]["TotalRows"].ToString());
                DataTable MessagesTable = new DataTable("messages");
                MessagesTable = build.TagBuildGuestbook(subdomainname, page, perPage).Tables[1];

                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append("<ul>");

                foreach (DataRow dr in MessagesTable.Rows)
                {
                    sb.Append("<li> <a onclick=\"return confirm('DELETE this message?')\" href=\"manageSiteGuestBook.aspx?job=delete&amp;id=" + dr["id"].ToString() + "&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\"><img src=\"../images/delete.png\" alt=\"del\"/></a> ");
                    sb.AppendFormat(" <b>{1}</b> " + Functions.GetCountryFlag(dr["posterIp"].ToString()) + " <small>{4:dd MMM yyyy}</small><br/>{5}</li>", dr["id"], dr["poster"], dr["posterIp"], dr["posterBrowser"], dr["postDate"], build.Smile(dr["post"].ToString()));
                }
                //pages
                if (totalPages > 1)
                {
                    sb.Append("<li><div class=\"center\">");
                    sb.AppendFormat("{0}/{1}<br/>", currentPage, totalPages);
                }

                //first page
                if (currentPage > 1)
                {
                    sb.Append("<a href=\"manageSiteGuestBook.aspx?p=1&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\"><img src=\"/images/rewind.png\" alt=\"First page\"/></a> ");
                }
                //next page
                if (currentPage < totalPages && totalPages > 1)
                {
                    sb.AppendFormat("<a href=\"manageSiteGuestBook.aspx?p={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\"><img src=\"/images/forward.png\" alt=\"Next page\"/></a> ", currentPage + 1);
                }
                //prev page
                if (currentPage > 1)
                {
                    sb.AppendFormat("<a href=\"manageSiteGuestBook.aspx?p={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\"><img src=\"/images/back.png\" alt=\"Previous page\"/></a> ", currentPage - 1);
                }
                //last page
                if (totalPages > 1 && currentPage < totalPages)
                {
                    sb.AppendFormat("<a href=\"manageSiteGuestBook.aspx?p={0}&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\"><img src=\"/images/fast_forward.png\" alt=\"Last page\"/></a> ", totalPages);
                }
                //finish pages
                if (totalPages > 1)
                {
                    sb.Append("</div></li>");
                }
                //wipe gb
                if (totalRecords > 0)
                {
                    sb.Append("<li><a onclick=\"return confirm('You are about to DELETE ALL MESSAGES from your guestbook! ARE YOU SURE?')\" href=\"manageSiteGuestBook.aspx?job=delete&amp;id=0&amp;un=" + Functions.un + "&amp;pw=" + Functions.pw + "&amp;subdomainid=" + Request.QueryString["subdomainid"] + "\">Delete all messages!</a></li>");
                }
                else
                {
                    sb.Append("<li>There are no messages in your guestbook. If you haven't added guestbook to your site yet, go to page designer and insert guestbook to any of your pages</li>");
                }
                sb.Append("</ul>");
                dumbGb.Text=sb.ToString();
  


                
            }

            else
            {
                notLoggedInDiv.Visible = true;
                LoggedInDiv.Visible = false;
            }
        }
        else
        {
            notLoggedInDiv.Visible = true;
            LoggedInDiv.Visible = false;
        }

    }
}
