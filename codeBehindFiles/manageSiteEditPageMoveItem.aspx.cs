﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class manage_manageSiteEditPageMoveItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();

        if (f.LoginCheck() == false)
        {

            //login wrong
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible = true;


        }
        else
        {
            myPageBuilder builder = new myPageBuilder();
            int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
            string  subdomainName = f.GetSubdomainNameById(subdomainid);
            string way = Request["way"];
            int PageId = Functions.ReturnNumeric(Request["pageid"]);
            int tagid = Functions.ReturnNumeric(Request["tagid"]);
            int cp = Functions.ReturnNumeric(Request["cp"]);

            ResultPanel.Visible = true;

            if (way == "up")
            {
                if (builder.MoveTag(Functions.un, subdomainName, PageId, tagid, cp, "up") == true)
                {

                    ResultLabel.Text = "Item moved up";
                }
                else
                {
                    ResultLabel.Text = "Item cannot be moved up";
                }
            }
            else
            {
                if (builder.MoveTag(Functions.un, subdomainName, PageId, tagid, cp, "down") == true)
                {
                    ResultLabel.Text = "Item moved down";
                }
                else
                {
                    ResultLabel.Text = "Item cannot be moved down";
                }
            }


        }


    }
}
