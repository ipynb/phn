﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class manage_managePaidUsers : System.Web.UI.Page
{
 protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
        {

            if (!Page.IsPostBack)
            {
                PaidUsersListView.DataSource = f.ListPaidUsers();
                PaidUsersListView.DataBind();
            }

            string uun = Functions.LimitString(Functions.ReturnAlphaNumeric(Request.QueryString["v"]), 50);
           
            if (uun.Length > 0)
            {
              
                subsPanel.Visible = true;
                subsRepeater.DataSource = f.listUsersSubdomains2(uun);
                subsRepeater.DataBind();
            }
        }
        else
        {
            loggedinDiv.Visible = false;
            notLoggedInDiv.Visible=true;
        }

    }
 protected void Button1_Click(object sender, EventArgs e)
 {
     Functions f = new Functions();
     string paidU = Functions.LimitString(Functions.ReturnAlphaNumeric(paidUser.Text),50);
     f.MakeUserPaid(paidU);
     PaidUsersListView.Visible = false;
     res.Visible = true;
     PlaceHolder1.Visible = false;
 }
 protected void Button2_Click(object sender, EventArgs e)
 {
     Functions f = new Functions();
     string paidU = Functions.LimitString(Functions.ReturnAlphaNumeric(paidUser.Text), 50);
     f.MakeUserUnPaid(paidU);
     PaidUsersListView.Visible = false;
     res.Visible = true;
     PlaceHolder1.Visible = false;
 }

}
