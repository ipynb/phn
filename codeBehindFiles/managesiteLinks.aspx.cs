﻿using System;
using System.Web.UI.WebControls;
using System.Data;

public partial class manage_managesiteLinks : System.Web.UI.Page
{



    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();


        if (f.LoginCheck() == true)
        {

            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            string _subdomain = f.GetSubdomainNameById(id);

            if (String.IsNullOrEmpty(_subdomain) == false)
            {

                fullSiteNameLabel.Text = f.GetFullSubdomainNameById(id);

                ObjectDataSource ds = new ObjectDataSource();
                ds.TypeName = "Functions";
                ds.SelectMethod = "ListRotatorLinks";
                ds.SelectParameters.Add("un", DbType.String, Functions.un);
                ds.SelectParameters.Add("subdomain", DbType.String, _subdomain);
                Repeater.DataSource = ds;
                Repeater.DataBind();
                if (Repeater.Items.Count == 0)
                {
                    Repeater.Visible = false;
                }
            }

            else
            {
                notLoggedInDiv.Visible = true;
                LoggedInDiv.Visible = false;
            }
        }
        else
        {
            notLoggedInDiv.Visible = true;
            LoggedInDiv.Visible = false;
        }

    }
}
