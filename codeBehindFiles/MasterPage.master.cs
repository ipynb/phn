﻿using System;
using System.Web;

public partial class MasterPage : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        
        Response.ContentEncoding = System.Text.Encoding.UTF8;
        //Response.ContentType = "application/xhtml+xml";
        Response.ContentType = "text/html";
        Response.Cache.SetExpires(DateTime.Now.AddMinutes(-10));
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.AddHeader("Cache-Control", "no-transform");

        //dont show any advert at the moment
        //Admob ad = new Admob();
        //phn_me_top_advert.Text = ad.ShowAd(false, DefaultAdverts.AdmobPublisherID());
    }
}
