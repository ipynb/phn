﻿using System;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

public partial class manage_manageSiteSnippetsEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();


        if (f.LoginCheck() == true)
        {
            if (!IsPostBack)
            {
                getCurrentDetails();
            }
            else
            {
                updateME();
            }
        }
        else
        {
            notLoggedInDiv.Visible = true;
            LoggedInDiv.Visible = false;
        }
    }


    protected void updateME()
    {
        Functions f = new Functions();
    
        if (f.LoginCheck() == true)
        {

           
            int id = Functions.ReturnNumeric(Request["subdomainid"]);
            string subdomain = f.GetSubdomainNameById(id);
            int _snipid = Functions.ReturnNumeric(Request["id"]);
            string _snippetName = Functions.LimitString(snippetName.Text, 50);
            //_snippetName = f.ReplaceLineBreakForTitles(_snippetName);
            _snippetName = Server.HtmlEncode(_snippetName);
            _snippetName = f.SqlProtect(_snippetName);
            string _snippetCode = Functions.LimitString(txtsnippetCode.Text, 1000);
            //_snippetCode = f.CleanHtml(_snippetCode);
            _snippetCode = Server.HtmlEncode(_snippetCode);
            _snippetCode = f.SqlProtect(_snippetCode);


            if (Functions.IsAlphaNumericForSubdomain(subdomain) == true
                && String.IsNullOrEmpty(_snippetName) == false
                && String.IsNullOrEmpty(_snippetCode) == false)
            {
                if (f.UpdateSnip(Functions.un, subdomain, _snippetName, _snippetCode, _snipid))
                {
                    
                    PanelGetDetails.Visible = false;
                    PanelUpdateDetails.Visible = true;


                }
                else
                {

                    PanelUpdateDetailsError.Visible = true;
                }
            }

            else
            {
                Panelallfields.Visible = true;
            }



        }
        else
        {
            notLoggedInDiv.Visible = true;
            LoggedInDiv.Visible = false;
        }
    }

    protected void getCurrentDetails()
    {
        Functions f = new Functions();
        int id = Functions.ReturnNumeric(Request["subdomainid"]);
        string subdomain = f.GetSubdomainNameById(id);
        int _snipid = Functions.ReturnNumeric(Request["id"]);

        if (String.IsNullOrEmpty(subdomain)==false)
            {
        using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString))
        {
            conn.Open();

            using (SqlCommand cmd = new SqlCommand("GetAsnip", conn))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@un", SqlDbType.VarChar).Value = Functions.un;
                cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                cmd.Parameters.Add("@id", SqlDbType.Int).Value = _snipid;
                using (SqlDataReader reader = cmd.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        
                            snippetName.Text = Server.HtmlDecode(reader["snippetName"].ToString());
                            txtsnippetCode.Text = Server.HtmlDecode(reader["snippetCode"].ToString());
                            subdomainholder.Value = subdomain;

                      
                    }
                    else
                    {
                        PanelGetDetails.Visible = false;
                        PanelUpdateDetailsError.Visible = true;
                    }
                }
            }
        }
        }
        else
        {
            PanelGetDetails.Visible = false;
                        PanelUpdateDetailsError.Visible = true;
        }
    }
    








}
