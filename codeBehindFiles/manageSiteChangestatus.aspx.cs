﻿using System;

public partial class manage_manageSiteStyleSheet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {
            int id = Functions.ReturnNumeric(Request["subdomainid"]);

            string subd = f.GetSubdomainNameById(id);
            if (String.IsNullOrEmpty(subd) == true)
            {
                changeResult.Text = "Wrong subdomain id! DO NOT play with querystring!";
            }
            else
            {

                int setmestatus = f.ChangeSiteStatus(Functions.un, subd);
                if (setmestatus == 1)
                {
                    changeResult.Text = "Site status changed to <b>online</b>";
                }
                else if (setmestatus == 0)
                {
                    changeResult.Text = "Site status changed to <b>offline</b>. No page will be served and all requests will be redirected to NOT FOUND error.";
                }

                else if (setmestatus == 2)
                {
                    changeResult.Text = "Can't apply changes. Are you sure you are doing the right thing?";
                }

            }

        }
            else
            {
                //login wrong
                loggedinDiv.Visible = false;
                notLoggedInDiv.Visible = true;
            }

    }

    
  }
