﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;

public partial class manage_manageSiteEditPageDeleteItem : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        myPageBuilder builder = new myPageBuilder();
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {

            int pid = Functions.ReturnNumeric(Request["pageid"]);
            int tid = Functions.ReturnNumeric(Request["tagid"]);
            int subdomainid = Functions.ReturnNumeric(Request["subdomainid"]);
            string subdomain = f.GetSubdomainNameById(subdomainid);
            bool res;

            string tagidcollection = Functions.LimitString(f.SqlProtect(Request["DeleteCheckbox"]),5000);
            int deletecollection=Functions.ReturnNumeric(Request["delcol"]);

            //set refresh
            HtmlMeta meta = new HtmlMeta();
            meta.HttpEquiv = "refresh";
            meta.Content = "3;url=manageSiteEditPage.aspx?un=" + Functions.un + "&pw=" + Functions.pw + "&subdomainid=" + subdomainid + "&pageid=" + pid;
            this.Header.Controls.Add(meta);



                if (String.IsNullOrEmpty(subdomain) == false)
                {
                    if (String.IsNullOrEmpty(tagidcollection) == false && deletecollection == 1)
                    {
                        res = builder.DeleteItems(Functions.un, subdomain, pid, tagidcollection); 
                    }
                    else
                    {
                        res = builder.DeleteItem(Functions.un, subdomain, pid, tid);
                    }

                    if ( res == true)
                    {
                        msg = "Item(s) has been deleted!";
                    }
                    else
                    {
                        deleteResult.CssClass = "error";
                        msg = "Can't delete item(s).";
                    }
                }
                else
                {
                    deleteResult.CssClass = "error";
                    msg = "Subdomain is required. Do not play with query string!";
                }


            

        }
        else
        {
            deleteResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        deleteResult.Text = msg;
    }
  
}
