﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.UI.WebControls;


public partial class manage_uploadFile : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        if (f.LoginCheck() == true)
        {
            if (!Page.IsPostBack)
            {
                uploadform.Visible = true;

                bool isUserPaid = f.isUserPaid(Functions.un);
                if (isUserPaid == true)
                {
                    fsLimit.Text = Convert.ToInt32(SiteConfiguration.GetConfig().uploadedfileSizeLimitPaidUserInBytes) / 1024 + "KB";
                    asLimit.Text = (Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitPaidUserInBytes) / 1024) / 1024 + "MB (" + Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitPaidUserInBytes) / 1024 + "KB)";

                }
                else
                {
                    fsLimit.Text = Convert.ToInt32(SiteConfiguration.GetConfig().uploadedfileSizeLimitInBytes) / 1024 + "KB";
                    asLimit.Text = (Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitInBytes) / 1024) / 1024 + "MB (" + Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitInBytes) / 1024 + "KB)";


                }
            }
        }
        else
        {
            loggedinDiv.Visible = false;
            notLoggedinDiv.Visible = true;
        }
    }

    protected void upbutton_Click(object sender, EventArgs e)
    {
        Functions f = new Functions();
        uploadform.Visible = false;
        resultPanel.Visible = true;
        FileList.Visible = false;
        FileListPaging.Visible = false;
        bool isUserPaid = f.isUserPaid(Functions.un);
        bool isFileallowed = false;

        //int sizeLimit = Convert.ToInt32(SiteConfiguration.GetConfig().uploadedfileSizeLimitInBytes);
        int sizeLimit = (isUserPaid == true) ? Convert.ToInt32(SiteConfiguration.GetConfig().uploadedfileSizeLimitPaidUserInBytes) : Convert.ToInt32(SiteConfiguration.GetConfig().uploadedfileSizeLimitInBytes);
        //int AccountsizeLimit = Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitInBytes);
        int AccountsizeLimit = (isUserPaid == true) ? Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitPaidUserInBytes) : Convert.ToInt32(SiteConfiguration.GetConfig().uploadedTotalfileSizeLimitInBytes);

        int currenttotalUploadsize = f.GetCurrentTotalUploads(Functions.un);

  
        string ct=string.Empty ;
        string ext=string.Empty ;
        if (File1.PostedFile != null && File1.PostedFile.ContentLength > 0 ) 
        ct = File1.PostedFile.ContentType.ToString();
        ext = Path.GetExtension(File1.PostedFile.FileName);
        //Response.Write(ext);


        if (isUserPaid == true)
        {
            if (f.IsFileExtensionsAllowed(ext) > -1 || f.IsFileExtensionsAllowedForPaidUser(ext) > - 1)
            {
                isFileallowed = true;
            }
            else
            {
                isFileallowed = false;
            }
        }
        else
        {
            if (f.IsFileExtensionsAllowed(ext) > -1)
            {
                isFileallowed = true;
            }
            else
            {
                isFileallowed = false;
            }
        }


        // mime type "image/pjpeg" created by ie!!
        if (
            File1.PostedFile != null &&
            File1.PostedFile.ContentLength > 0 &&
            File1.PostedFile.ContentLength < sizeLimit &&
            currenttotalUploadsize < AccountsizeLimit &&
            isFileallowed == true
            )
        {
            
            //fix ie problem
            if (ct == "image/pjpeg"){ct = "image/jpeg";}

            HttpPostedFile myFile = File1.PostedFile;
            string fileName=f.SqlProtect(myFile.FileName.ToString());
            fileName = Server.HtmlEncode(fileName);
            string fileNameWoEx = Path.GetFileNameWithoutExtension(fileName);
            fileNameWoEx = Functions.LimitString(fileNameWoEx, 40);
            string fileExt = Path.GetExtension(fileName);
            fileExt = Functions.ReturnAlphaNumeric(Functions.LimitString(fileExt, 5));
            fileName = fileNameWoEx +  fileExt;

            int nFileLen = myFile.ContentLength;
            byte[] myData = new byte[nFileLen];
            myFile.InputStream.Read(myData, 0, nFileLen);


            //resize
            if (Functions.IsMimeTypeImage(ct) && CheckBoxResize.Checked == true)
            {
                
                int newEn = Functions.ReturnNumeric(TextBoxEn.Text);
                int newBoy = Functions.ReturnNumeric(TextBoxBoy.Text);
                if ( newEn>0 &&newBoy>0 && newEn<255 && newBoy<255)
                {
                    System.Drawing.Image Resized = System.Drawing.Image.FromStream(myFile.InputStream, true);
                    myData = ResizeImage.Resize(Resized, newEn, newBoy);
                   
                }
            }
            //resize end




            if (f.UploadFile(Functions.un, fileName, nFileLen, ct, myData) == true)
            {
                resultLabel.Text = "File uploaded";
            }
            else 
            {
                resultLabel.CssClass="error";
                resultLabel.Text = "File already exists! Unable to upload!";
            }

        }
        else
        {
            resultLabel.CssClass = "error";
            resultLabel.Text = "Unable to upload! Make sure you didn't try to upload more then permitet size limits and file types";
        }
    }
    protected void FileListEventHandler(object sender, ListViewItemEventArgs e)
    {
        ListViewDataItem dataItem = (ListViewDataItem)e.Item;
        DataRowView drv = (DataRowView)dataItem.DataItem;

        Label imgLabel = (Label)e.Item.FindControl("imgLabel");

        if (Functions.IsFileImage(drv["fileName"].ToString()) == true)
        {
            imgLabel.Visible = true;
            imgLabel.Text = "<img src=\"/FileHandler.ashx?id=" + drv["id"].ToString() + "&amp;m=t\"/><br />";
            
        }

    }
}
