﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;


public partial class manage_uploadFileDelete : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        myPageBuilder builder = new myPageBuilder();
        Functions f = new Functions();
        string msg;
        if (f.LoginCheck() == true)
        {
            int tid = Functions.ReturnNumeric(Request["id"]);

            
                if (f.DeleteAFile(Functions.un, tid) == true)
                {
                    msg = "File has been deleted!";
                }
                else
                {
                    deleteResult.CssClass = "error";
                    msg = "Can't delete the file.";
                }
            


        }
        else
        {
            deleteResult.CssClass = "error";
            msg = "You need to be logged in in order to use this page!";
        }

        deleteResult.Text = msg;
    }
}
