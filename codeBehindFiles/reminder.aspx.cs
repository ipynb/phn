﻿using System;
using System.Web;

public partial class manage_reminder : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string email = HttpContext.Current.Request["email"];
        string remind = HttpContext.Current.Request["remind"];
        string captchaText = HttpContext.Current.Request["captcha"];

        if (
               Functions.IsAlphaNumeric(Functions.un) == true
               && Functions.IsValidEmail(email) == true
               && remind == "remind"
               && captchaText.ToLower() == Cache["CaptchaImageText"].ToString().ToLower()
               
            )
        {   Functions f = new Functions();
            string recoveredPass = f.RemindPassword(Functions.un,email);

            if (String.IsNullOrEmpty(recoveredPass) == true)
            {

                reminderResult.Text = "Username, e-mail or security image combination does not match!";
            }
            else
            {
                if (f.sendPasswordReminderMail(email, Functions.un, recoveredPass) == true)
                {
                    reminderResult.Text = "Your details have been sent to " + email;
                }
                else
                {
                    reminderResult.Text = "There was a problem when connecting to mail server. We couldn't send your password reminder email. Please contact us at "+SiteConfiguration.GetConfig().emailServerDefaultFromMail+" in order to solve this issue.";
                }
                
            }
        }
        else
        {
            reminderResult.Text = "All fields are required!";
        }
    }
}
