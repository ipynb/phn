﻿using System;
using System.Web;

public partial class manage_register : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        registerBoxResult.Visible = false;
        ErrorLabel.Visible = false;
        emailSenderError.Visible = false;

   }
    protected void regbuttonClick(object sender, EventArgs e)
    {
        Functions f = new Functions();
        string email = Functions.LimitString(HttpContext.Current.Request["email"],100);
        string pw2 = Functions.LimitString(HttpContext.Current.Request["pw2"],50);
        string ip = HttpContext.Current.Request.UserHostAddress;
        string verificationCode = f.GetUniqueKey(5);
        string captchaText = Functions.LimitString(HttpContext.Current.Request["captcha"],50);
        string generatedCaptcha;
        // somehow cache fails to hold captcah text so at those times i don' do check 
        if (Cache["CaptchaImageText"] != null)
        {
            generatedCaptcha = Cache["CaptchaImageText"].ToString().ToLower();
        }
        else
        {
            generatedCaptcha = captchaText.ToLower();
        }
      
        if (
               Functions.IsAlphaNumeric(Functions.un) == true
            && Functions.IsAlphaNumeric(Functions.pw) == true
            && Functions.IsValidEmail(email)          == true
            && Functions.IsAlphaNumeric(pw2)          == true
            && String.Compare(Functions.pw,pw2)       ==0
            && captchaText.ToLower()                  == generatedCaptcha
            )
        {
            if (f.RegisterUser(Functions.un, Functions.pw, email, ip, verificationCode) == true)
            {
                if (f.sendVerificationMail(email, verificationCode, Functions.un, Functions.pw)) // sendmail
                {
                    registerBox.Visible = false;
                    registerBoxResult.Visible = true;
                }
                else
                {
                    registerBox.Visible = false;
                    emailSenderError.Visible = true;
                }
                
            }
            else
            {
                ErrorLabel.Visible = true;
                ErrorLabel.Text = "Username and/or e-mail address exists. Try another username and/or e-mail!<br/>";
            }
        }
        else
        {
            ErrorLabel.Visible = true;
            ErrorLabel.Text = "All fields are required! Try again.<br/>";
        }

    }
    
}
