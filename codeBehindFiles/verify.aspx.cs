﻿using System;
using System.Web.UI;

public partial class verify : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Page.IsPostBack)
        {
            result.Visible = true;
            formpanel.Visible = false;
        }

    }
    protected void sbmt_Click(object sender, EventArgs e)
    {
        string email = Functions.LimitString(emailinput.Text, 100);
        string verficationcode = Functions.LimitString(verificationCodeInput.Text, 100);
     

        if (Functions.IsValidEmail(email) == true && Functions.IsAlphaNumeric(verficationcode))
        {
            Functions f = new Functions();
            if (f.ValidateAccount(email, verficationcode))
            {
                reslab.Text = "Your account has been verified. Please go to main page and logon";
            }
            else
            {
                reslab.Text = "E-mail and verification code does not match!";
            }
        }
        else
        {
                reslab.Text = "All fields are required!";
        }
    }
}
