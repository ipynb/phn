﻿using System;
using System.Configuration;
using System.Data;
using System.Text;
using System.Data.SqlClient;


public partial class postToGuestBook : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Functions f = new Functions();
        int limit = 1;//limit in minutes bitween messages;
        string subdomain = Functions.LimitString(Request["subdomain"], 50);
        string domain = Functions.LimitString(Request["domain"], 50);
        string Ip = Request.ServerVariables["REMOTE_ADDR"];

        string Browser = Request.ServerVariables["HTTP_USER_AGENT"];
        Browser = Server.HtmlEncode(Browser);
        Browser = f.SqlProtect(Browser);
        if (String.IsNullOrEmpty(Browser))
        {
            Browser = "Unknown browser";
        }

        if (Browser.ToLower().Contains("mini"))
        {
            string forwardedip = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (forwardedip != null)
            {
                string[] forwardedipArray = forwardedip.Split(new char[] { ',' });
                //last ip in the array is the real ip
                //http://dev.opera.com/articles/view/opera-mini-request-headers/
                string extIp = forwardedipArray[forwardedipArray.Length - 1].ToString().Trim();
                if (Functions.IsValidIP(extIp) == true && Functions.IsIpLocalIP(extIp) == false)
                {
                    Ip = extIp;
                }
            }

        }




        string nick = Request["nick"];
        nick = Server.HtmlEncode(nick);
        nick = f.ReplaceLineBreakForTitles(nick);
        nick = Functions.LimitString(nick, 50);
        nick = f.SqlProtect(nick);

        string returnUrl = Request["returnUrl"];

        string message = Request["message"];
        message = Server.HtmlEncode(message);
        message = f.ReplaceLineBreak(message);
        message = Functions.LimitString(message, 500);
        message = f.SqlProtect(message);

        string captcha = Functions.ReturnAlphaNumeric(Request["captcha"]).ToLower();
        string generatedCaptcha;
        // somehow cache fails to hold captcah text so at those times i don' do check 
        if (Cache["CaptchaImageText"] != null)
        {
            generatedCaptcha = Cache["CaptchaImageText"].ToString().ToLower();
        }
        else
        {
            generatedCaptcha = captcha;
        }
        //-------------------------------------------------------------------------------


        if (String.IsNullOrEmpty(subdomain) == true
            || Functions.IsAlphaNumericForSubdomain(subdomain) == false
               || String.IsNullOrEmpty(domain) == true
                || String.IsNullOrEmpty(returnUrl) == true
                    || String.IsNullOrEmpty(nick) == true
                        || String.IsNullOrEmpty(message) == true
                            || message.Length < 10
                                || String.IsNullOrEmpty(captcha) == true
                                    || captcha != generatedCaptcha)
        {
            // all fields are required;
            Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?err=1#err");
        }
        else
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["myConnectionString"].ConnectionString))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand("AddGuestBookMessage", conn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@subdomain", SqlDbType.VarChar).Value = subdomain;
                    cmd.Parameters.Add("@poster", SqlDbType.NVarChar).Value = nick;
                    cmd.Parameters.Add("@posterIp", SqlDbType.VarChar).Value = Ip;
                    cmd.Parameters.Add("@posterBrowser", SqlDbType.VarChar).Value = Browser;
                    cmd.Parameters.Add("@post", SqlDbType.NVarChar).Value = message;
                    cmd.Parameters.Add("@limit", SqlDbType.Int).Value = limit;

                    SqlParameter success = new SqlParameter("@RETURN_VALUE", SqlDbType.Bit);
                    success.Direction = ParameterDirection.ReturnValue;
                    cmd.Parameters.Add(success);
                    cmd.ExecuteScalar();
                    bool result = Convert.ToBoolean(success.Value);

                    //int result = (int)cmd.ExecuteScalar();

                    if (result == true)
                    {
                        Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?err=0#res");
                    }
                    else
                    {
                        Response.Redirect("http://" + subdomain + "." + domain + "/" + returnUrl + "?err=1#res");
                    }

                }
            }
        }
    }
}
