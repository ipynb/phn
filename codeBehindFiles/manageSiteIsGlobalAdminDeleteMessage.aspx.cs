﻿using System;
using System.Web.Caching;

public partial class manage_manageSiteDeleteMessage : System.Web.UI.Page
{

        protected void Page_Load(object sender, EventArgs e)
        {
            Functions f = new Functions();
            string msg;
            if (f.LoginCheck() == true && Session["IsGlobalAdmin"].ToString() == "yes")
            {
                int id = Functions.ReturnNumeric(Request["id"]);



                    if (f.DeleteAnounce (id) == true)
                    {
                        msg = "Message has been deleted!";
                        
                        Cache.Remove("anounces");
                        
                    }
                    else
                    {
                        deleteResult.CssClass = "error";
                        msg = "Can't delete the message!";
                    }
              

            
            }
            else
            {
                deleteResult.CssClass = "error";
                msg = "You need to be logged in in order to use this page!";
            }

            deleteResult.Text = msg;
        }

   

}
